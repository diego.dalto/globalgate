<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
date_default_timezone_set('America/Argentina/Buenos_Aires');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

session_start();

require __DIR__.'/../vendor/autoload.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
    ],
]);

$app->add(new \Models\Middleware\Session([
    'name' => 'globalgate_comercial',
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));

// Get container
$container = $app->getContainer();

$container['session'] = function () {
    return new \Models\Session\Helper;
};

$app->add(new \Models\MySQL\Security(
    $container['session']->get("user_id"), 
    $container['session']::id(),
    $container->router
));

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views/', [
        'cache' => false
    ]);
    
    $view->addExtension(new Slim\Views\TwigExtension(
        $container->router, 
        $container->request->getUri(),
        $container['session']->get('user_name'),
        $container['session']->get('user_lastname'),
        $container['session']->get('user_photo')
    ));

    return $view;
};

$container['db'] = function () {
    return new \Models\MySQL\MyPDO();
};

$container['log'] = function ($container) {
    return new \Models\PDO\Log($container->db, $container->session->user_id);
};

$container['user'] = function($container) {
    return new \Models\PDO\User($container['db']);
};

require __DIR__ . '/../v1/index.php';
require __DIR__ . '/../v1/dashboard.php';
require __DIR__ . '/../v1/calendario.php';
require __DIR__ . '/../v1/cliente.php';
require __DIR__ . '/../v1/oportunidad.php';
require __DIR__ . '/../v1/registro.php';
require __DIR__ . '/../v1/renovacion.php';
require __DIR__ . '/../v1/tarea.php';
require __DIR__ . '/../v1/usuario.php';
require __DIR__ . '/../v1/venta.php';
require __DIR__ . '/../v1/marca.php';
require __DIR__ . '/../v1/grupo_marca.php';
require __DIR__ . '/../v1/producto.php';
require __DIR__ . '/../v1/log.php';
require __DIR__ . '/../v1/configuracion.php';
