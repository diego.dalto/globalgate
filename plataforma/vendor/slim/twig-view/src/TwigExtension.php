<?php
/**
 * Slim Framework (http://slimframework.com)
 *
 * @link      https://github.com/slimphp/Twig-View
 * @copyright Copyright (c) 2011-2015 Josh Lockhart
 * @license   https://github.com/slimphp/Twig-View/blob/master/LICENSE.md (MIT License)
 */
namespace Slim\Views;

class TwigExtension extends \Twig_Extension
{
    /**
     * @var \Slim\Interfaces\RouterInterface
     */
    private $router;

    /**
     * @var string|\Slim\Http\Uri
     */
    private $uri;
    
    private $userName;
    private $userLastName;
    private $userPicture;

    public function __construct($router, $uri, $userName, $userLastName, $userPicture)
    {
        $this->router = $router;
        $this->uri = $uri;
        $this->userName = $userName;
        $this->userLastName = $userLastName;
        $this->userPicture = $userPicture;
    }

    public function getName()
    {
        return 'slim';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('path_for', array($this, 'pathFor')),
            new \Twig_SimpleFunction('base_url', array($this, 'baseUrl')),
            new \Twig_SimpleFunction('is_current_path', array($this, 'isCurrentPath')),
            new \Twig_SimpleFunction('user_name', array($this, 'getUserName')),
            new \Twig_SimpleFunction('user_lastname', array($this, 'getUserLastName')),
            new \Twig_SimpleFunction('user_picture', array($this, 'getUserPicture'))
        ];
    }

    public function pathFor($name, $data = [], $queryParams = [], $appName = 'default')
    {
        return $this->router->pathFor($name, $data, $queryParams);
    }

    public function baseUrl()
    {
        if (is_string($this->uri)) {
            return $this->uri;
        }
        if (method_exists($this->uri, 'getBaseUrl')) {
            return $this->uri->getBaseUrl();
        }
    }

    public function isCurrentPath($name, $data = [])
    {
        return $this->router->pathFor($name, $data) === sprintf("%s/%s", $this->uri->getBasePath(), $this->uri->getPath());
    }

    /**
     * Set the base url
     *
     * @param string|Slim\Http\Uri $baseUrl
     * @return void
     */
    public function setBaseUrl($baseUrl)
    {
        $this->uri = $baseUrl;
    }
    
    public function getUserName() {
        return $this->userName;
    }
    
    public function getUserLastName() {
        return $this->userLastName;
    }
    
    public function getUserPicture() {
        return $this->userPicture;
    }
}
