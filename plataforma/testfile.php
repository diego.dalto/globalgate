<?php
?>
<html>
<body>
<form enctype="multipart/form-data" action="testfile.php" method="POST">
<input type="file" name="testfile" id="testfile" />
<input type="submit"/>
</form>
<?php

function file_upload_error_message($error_code) {
    switch ($error_code) {
        case UPLOAD_ERR_INI_SIZE:
            return 'The uploaded file exceeds the upload_max_filesize directive
in php.ini';
        case UPLOAD_ERR_FORM_SIZE:
            return 'The uploaded file exceeds the MAX_FILE_SIZE directive that w
as specified in the HTML form';
        case UPLOAD_ERR_PARTIAL:
            return 'The uploaded file was only partially uploaded';
        case UPLOAD_ERR_NO_FILE:
            return 'No file was uploaded';
        case UPLOAD_ERR_NO_TMP_DIR:
            return 'Missing a temporary folder';
        case UPLOAD_ERR_CANT_WRITE:
            return 'Failed to write file to disk';
        case UPLOAD_ERR_EXTENSION:
            return 'File upload stopped by extension';
        default:
            return 'Unknown upload error';
    }
}
if (isset($_FILES))
{
  if (count($_FILES) == 0)
    return;
  echo "<br/>";
  echo 'Filename: ' . $_FILES['testfile']['name'];
  echo "<br/>";
  echo 'File temporal name: ' . $_FILES['testfile']['tmp_name'];
  echo "<br/>";

  $target_path = "tmp/";
  $target_path = $target_path . basename( $_FILES['testfile']['name']);
  echo 'Target path: ' . $target_path;
  echo "<br/>";
  echo 'Error code: ';
  if ($_FILES['testfile']['error'] != UPLOAD_ERR_OK)
    echo $_FILES['testfile']['error'];
  echo '<br/>';
  echo 'Error message: ';
  if ($_FILES['testfile']['error'] != UPLOAD_ERR_OK)
    echo file_upload_error_message($_FILES['testfile']['error']);
  echo '<br/>';
  if(move_uploaded_file($_FILES['testfile']['tmp_name'], $target_path)) {
      echo "File ".  basename( $_FILES['testfile']['name']).
      " uploaded";
    echo "<br/>";
  } else {
    echo "There was an error moving the file.";
    echo "<br/>";
 }
}
?>

</body>
</html>
<?php
?>


