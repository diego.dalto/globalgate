<?php 

$app->group('/configuracion', function () {

    $this->get('/origen-de-oportunidad', function ($request, $response, $args) {
        $params = [];

        /**
         * Lista de opportunity_origin
         */
        $tableopportunityOrigin = $this->db->prepare("
            SELECT opportunity_origin_id, opportunity_origin_name
                FROM opportunity_origin;
        ");
        $tableopportunityOrigin->execute();
        $params['opportunity_origin'] = $tableopportunityOrigin->fetchAll(PDO::FETCH_ASSOC);
        
        $this->log->info("Se ingresó al listado de origen de oportunidad", "SELECT", "", 0);

        return $this->view->render($response, 'setting.twig', $params);
        
    })->setName('opportunity-origin-list');

    $this->delete('/delete', function ($request, $response, $args) {
        $opportunity_origin_id = $request->getParsedBodyParam('opportunity_origin_id');
        $params = [
            'type' => -1,
            'message' => "Falta ingresar el id"
        ];

        if (isset($opportunity_origin_id) && !empty($opportunity_origin_id)) {
            $tableopportunityOrigin = $this->db->prepare("
                DELETE
                    FROM opportunity_origin
                WHERE opportunity_origin_id = :id;
            ");
            $tableopportunityOrigin->bindValue(':id', $opportunity_origin_id);
            $tableopportunityOrigin->execute();
            
            $params = [
                'type' => 2,
                'message' => "Se eliminó el origen de oportunidad"
            ];
        }

        $this->log->info("Se borró el origen de oportunidad", "DELETE", "opportunity_origin_id", $opportunity_origin_id);

        return $response->withJson($params);
    })->setName('opportunity-origin-delete');

    $this->post('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $existGroup = false;
        $brand_type = '';
        $params = [
            'type' => -1,
            'message' => "No se pudo guardar"
        ];

        if (isset($post)) {
            if (isset($post['opportunity_origin_name']) && !empty($post['opportunity_origin_name'])) {
                $tableopportunityOrigin = $this->db->prepare("
                    SELECT opportunity_origin_name 
                        FROM opportunity_origin 
                    WHERE opportunity_origin_name = :name;
                ");
                $tableopportunityOrigin->bindValue(':name', $post['opportunity_origin_name']);
                $tableopportunityOrigin->execute();

                $opportunityOrigin = $tableopportunityOrigin->fetchAll(PDO::FETCH_ASSOC);

                if (count($opportunityOrigin) > 0) {
                    $existGroup = true;
                    $params = [
                        'type' => 1,
                        'message' => "El origen de oprtunidad ya existe"
                    ];
                }
            }

            if (!$existGroup) {
                $tableopportunityOrigin = $this->db->prepare("
                    INSERT INTO opportunity_origin 
                    (opportunity_origin_name) VALUES (:name);
                ");
                $tableopportunityOrigin->bindValue(':name', $post['opportunity_origin_name']);
                $tableopportunityOrigin->execute();
                
                $params = [
                    'type' => 2,
                    'message' => "Guardado"
                ];
                $opportunity_origin_id = $this->db->lastInsertId();

                $this->log->info("Se creó el origen de oportunidad ".$post['opportunity_origin_name'], "INSERT", "opportunity_origin_id", $opportunity_origin_id);                
            }
        }

        return $response->withJson($params);
    })->setName('opportunity-origin-create');

    $this->put('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $opportunity_origin_id = $post['opportunity_origin_id'];
        $params = [
            'type' => -1,
            'message' => "No se pudo actualizar"
        ];

        if (isset($post) && (isset($opportunity_origin_id) && !empty($opportunity_origin_id))) {
            if (isset($post['opportunity_origin_name']) && !empty($post['opportunity_origin_name'])) {
                $tableOpportunityOrigin = $this->db->prepare("
                    UPDATE opportunity_origin 
                        SET opportunity_origin_name = :name
                    WHERE opportunity_origin_id = :id;
                ");
                $tableOpportunityOrigin->bindValue(':name', $post['opportunity_origin_name']);
                $tableOpportunityOrigin->bindValue(':id', $opportunity_origin_id);
                $tableOpportunityOrigin->execute();
                
                $this->log->info("Se modificó el origen de oportunidad ".$post['opportunity_origin_name'], "UPDATE", "opportunity_origin_id", $opportunity_origin_id);

                $params = [
                    'type' => 2,
                    'message' => "Actualizado"
                ];
            }
        }

        return $response->withJson($params);
    })->setName('opportunity-origin-update');
});