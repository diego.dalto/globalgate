<?php

namespace Models\Contracts;

/**
 *
 * @author turkee
 */
interface iMySQL {
    public function delete(array $where);
        
    public function insert(array $insert);
    
    public function update(array $set, array $where);
    
    public function save(array $insert, array $update);
    
    public function select(array $where, $class = "");

}
