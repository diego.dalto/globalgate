<?php

namespace Models\PDO;

/**
 * Description of Client
 *
 * @author turkee
 */
class Client {
    protected $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getByCuit($cuit) {
      $client = $this->db->prepare("
          SELECT
            `client_code`, `client_businessname`, `client_address`, `client_city`,
            `client_state`, `client_invoicetype`, `client_cp`, `client_email`, `client_cuit`
            FROM `client`
          WHERE REPLACE(client_cuit, '-', '') = :cuit
      ");
      $client->bindParam(':cuit', $cuit);
      $client->execute();

      return $client->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function set($datos) {
      /**
      * `CODIGO`, `RAZON_SOC`, `DIRECCION`, `LOCALIDAD`, `COD_PCIA`,
      * `TITULAR`, `CUIT`, `FACT_TIPO`, `TELEFONO0`, `TELEFONO1`,
      * `TELEFONO2`, `TELEFONO3`, `FAX`, `FECHA_FACT`, `PROX_FACT`,
      * `CODPOSTAL`, `FINGRESO`, `AGENTE`, `MAIL`, `WWW`, `STATUS`,
      * `LEYEN`, `RUBRO`, `TIPO`, `TC`, `FP`, `CUITPAIS`, `NPAIS`
      */
      $client = $this->db->prepare(
        "INSERT IGNORE INTO client
        (
              client_code, client_businessname, client_address, client_city, client_state,
              client_contact, client_cuit, client_invoicetype, client_phone0, client_phone1,
              client_phone2, client_phone3, client_fax, client_invoicedate, client_newtinvoicedate,
              client_cp, client_createdate, client_agent, client_email, client_www, client_status,
              client_leyen, client_entry, client_type, client_tc, client_fc, client_cuitpais, client_npais
        ) VALUES (
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?, ?, ?
        );
      ");
      $client->execute($datos);
    }

}
