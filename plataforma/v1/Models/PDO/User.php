<?php

namespace Models\PDO;
/**
 * Description of User
 *
 * @author turkee
 */
class User {
    protected $pdo;
    
    public function __construct($pdo) {
        $this->pdo = $pdo;
    }
        
    public function get(string $where, array $value = array()) {
        $products = $this->pdo->prepare("SELECT * "
            . "FROM users "
            . "WHERE $where");
        $products->execute($value);
        return $products->fetchAll(\PDO::FETCH_ASSOC);
    }
}
