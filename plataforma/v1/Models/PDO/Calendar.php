<?php

namespace Models\PDO;

/**
 * Description of Calendar
 *
 * @author turkee
 */
class Calendar {
    
    protected $db;
    
    public function __construct($db) {
        $this->db = $db;
    }

    public function get($user_id, $type = 'calendar') {
        //Obtengo calendario
        $milestone = $this->db->prepare("SELECT milestone_id, milestone_title, milestone_detail, "
                . "DATE_SUB(milestone_date, INTERVAL 1 MONTH) AS d, "
                . "milestone_start_time, milestone_end_time, milestone_type, "
                . "milestone_status, client_id "
                . "FROM milestone "
                . "WHERE milestone_type = :type AND user_id = :user_id");
        $milestone->bindParam(':user_id', $user_id);
        $milestone->bindParam(':type', $type);
        $milestone->execute();
        
        return $milestone->fetchAll(\PDO::FETCH_ASSOC);        
    }
    
    public function parseJsonCalendar($milestones) {
        $calendar = [];
        
        foreach ($milestones as $key => $mile) 
        {
            $date = str_replace("-", ", ", $mile['d']);
            $time_start = str_replace(":", ", ", $mile['milestone_start_time']);
            $time_end = str_replace(":", ", ", $mile['milestone_end_time']);
            $className = ($mile['milestone_status'] == 1) ? "palette-Green bg" : "";
            
            
            $calendar[$key] = 
                    "{"
                        . "'id':'". $mile['milestone_id']."', "
                        . "'title':'". $mile['milestone_title']."', "
                        . "'description':'". $mile['milestone_detail']."', "
                        . "'client_id':'". $mile['client_id']."', "
                        . "'start': new Date($date, $time_start), "
                        . "'end': new Date($date, $time_end), "
                        . "'time_start': '". $mile['milestone_start_time']."', "
                        . "'time_end': '". $mile['milestone_end_time']."', "
                        . "'allDay': false, "
                        . "'className': '$className'"
                    . "}";
        }
        return implode(", ", $calendar);
    }
}
