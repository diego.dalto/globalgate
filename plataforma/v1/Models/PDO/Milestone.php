<?php

namespace Models\PDO;

/**
 * Description of Milestone
 *
 * @author turkee
 */
class Milestone
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

	public function statusSerialSale($serials_contract_id, $sku)
    {
		$query = $this->pdo->prepare("SELECT od.opportunity_detail_father,sc.serials_contract_type,
		                                    sc.serials_contract_number,sc.serials_contract_enddate,
											sc.serials_contract_limitdate,sc.serials_contract_status,
											od.opportunity_detail_productid
		                             FROM serials_contract sc, opportunity_detail od
									 WHERE sc.serials_contract_id='".$serials_contract_id."' AND
									       sc.opportunity_detail_id=od.opportunity_detail_id");
		$query->execute();
		$serials_contract_father = $query->fetchAll(\PDO::FETCH_ASSOC);

		$opportunity_detail_father = $serials_contract_father[0]["opportunity_detail_father"] ?? $serials_contract_father[0]["opportunity_detail_father"];
		$serials_contract_type = $serials_contract_father[0]["serials_contract_type"] ?? $serials_contract_father[0]["serials_contract_type"];
		$serials_contract_number = $serials_contract_father[0]["serials_contract_number"] ?? $serials_contract_father[0]["serials_contract_number"];
		$serials_contract_enddate = $serials_contract_father[0]["serials_contract_enddate"] ?? $serials_contract_father[0]["serials_contract_enddate"];
		$serials_contract_limitdate = $serials_contract_father[0]["serials_contract_limitdate"] ?? $serials_contract_father[0]["serials_contract_limitdate"];
		$serials_contract_status = $serials_contract_father[0]["serials_contract_status"] ?? $serials_contract_father[0]["serials_contract_status"];
		$sku = $serials_contract_father[0]["opportunity_detail_productid"] ?? $serials_contract_father[0]["opportunity_detail_productid"];

		//////////////////

		$sql = "SELECT `product_rules_id`, `rules_key`, `sku` FROM `product_rules` WHERE sku=:skuproduct";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(':skuproduct', $sku);
		$query->execute();
		$rules_sku = $query->fetchAll(\PDO::FETCH_ASSOC);

		$estadoFinal = "PENDIENTE";

		$estadoSerial = true;
		$estadoContrato = true;
		$estadoAdmin = true;

		$infoReglas = "";

		foreach ($rules_sku as $row => $value) {
			if($value["rules_key"]=="REQUIRE_SERIAL_CONTRACT"){
				if($opportunity_detail_father=="" || $opportunity_detail_father=="0"){
					if($serials_contract_type=="Serial" && $serials_contract_number==""){
						$estadoSerial = false;
					}
				}else{
					if($serials_contract_type=="Contrato" && $serials_contract_number==""){
						$estadoSerial = false;
					}
				}
				$infoReglas .= "/RSC/";
			}

			if($value["rules_key"]=="REQUIRE_DEAD_LINE"){
				if($opportunity_detail_father=="" || $opportunity_detail_father=="0"){
					if($serials_contract_type=="Serial" && $serials_contract_enddate==""){
						$estadoSerial = false;
					}
				}else{
					if($serials_contract_type=="Contrato" && $opportunity_detail_father==""){
						$estadoSerial = false;
					}
				}
				$infoReglas .= "/RFV/";
			}

			if($value["rules_key"]=="REQUIRE_REGISTER_LIMIT"){
				if($opportunity_detail_father=="" || $opportunity_detail_father=="0"){
					if($serials_contract_type=="Serial" && $serials_contract_limitdate==""){
						$estadoSerial = false;
					}
				}else{
					if($serials_contract_type=="Contrato" && $serials_contract_limitdate==""){
						$estadoSerial = false;
					}
				}
				$infoReglas .= "/RFL/";
			}

			if($value["rules_key"]=="REQUIRE_CONTTRACT_ADITIONAL"){
				if($serials_contract_type=="Serial"){
					$estadoContrato = false;

					$query = $this->pdo->prepare("SELECT COUNT(*) AS total FROM serials_contract WHERE serials_contract_type='Contrato' AND serials_contract_number!='' AND serials_contract_parent=".$serials_contract_id."");
					$query->execute();
					$serials_contract_count = $query->fetchAll(\PDO::FETCH_ASSOC);

					$totalContratos = $serials_contract_count[0]["total"];

					if($totalContratos>0){
						$estadoContrato = true;
					}
				}

				$infoReglas .= "/RCAD/";
			}

			if($value["rules_key"]=="REQUIRE_ADMIN_CLOSE"){
				if($serials_contract_status=="0"){
					$estadoAdmin = false;
				}

				$infoReglas .= "/RCADM/";
			}
		}

		if($estadoSerial && $estadoContrato && $estadoAdmin){
			$estadoFinal = "COMPLETO";

			$query = $this->pdo->prepare("UPDATE serials_contract
											SET serials_contract_status = 1
										 WHERE serials_contract_id = :serial_id ");
      $query->bindParam(':serial_id', $serials_contract_id);
			$query->execute();
		}

		return $estadoFinal."|".$infoReglas;
		//return "test";
	}


	///////////////////////////////

    public function getTask(string $user_id, string $date, string $when = "NOW", string $limit = "5")
    {
        $d = "=";
        $sql_last="";

        if ($when == "LAST") {
            $d = "<";

            $sql_last="
      				UNION
      				SELECT '' AS milestone_title, '' AS milestone_detail,
      					'' AS  d, '' AS milestone_date,
      					'' AS milestone_start_time, '' AS milestone_end_time, '' AS milestone_type,
      					'' AS milestone_status, client_businessname, opportunity_id, opportunity_stage, opportunity_total, o.client_id, 'last' AS permiso, CURDATE() AS orden
      				FROM opportunity o
      					INNER JOIN client c ON c.client_id = o.client_id
      				WHERE
      					o.user_id = :user_id  AND o.opportunity_status = 'A'
      					AND (SELECT COUNT(m.milestone_id) FROM milestone m WHERE m.opportunity_id=o.opportunity_id)=0
              UNION
      				SELECT '' AS milestone_title, '' AS milestone_detail,
      					'' AS  d, '' AS milestone_date,
      					'' AS milestone_start_time, '' AS milestone_end_time, '' AS milestone_type,
      					'' AS milestone_status, client_businessname, opportunity_id, opportunity_stage, opportunity_total, o.client_id, 'last' AS permiso, CURDATE() AS orden
      				FROM opportunity o
      					INNER JOIN client c ON c.client_id = o.client_id
      				WHERE
      					o.user_id = :user_id  AND o.opportunity_status = 'A'
      					AND (SELECT COUNT(m.milestone_id) FROM milestone m WHERE m.opportunity_id=o.opportunity_id AND m.milestone_date > NOW())=0
						AND (SELECT COUNT(m.milestone_id) FROM milestone m WHERE m.opportunity_id=o.opportunity_id AND m.milestone_date < NOW() AND m.milestone_type NOT IN ('note','notice') AND  m.milestone_status = '0')=0";
        }

        if ($when == "TOMORROW") {
            $d = ">";
        }
        $sql_aditional = "UNION
        SELECT milestone_title, milestone_detail,
        					DATE_FORMAT(milestone_date, '%d-%m-%Y') AS d, milestone_date,
        					milestone_start_time, milestone_end_time, milestone_type,
        					milestone_status, client_businessname, m.opportunity_id, o.opportunity_stage, o.opportunity_total, o.client_id, 'adicional' AS permiso, milestone_date AS orden
        			 FROM milestone AS m
        				  INNER JOIN opportunity o ON o.opportunity_id = m.opportunity_id
						  INNER JOIN opportunity_user ou ON ou.opportunity_id = m.opportunity_id
        				  INNER JOIN client c ON c.client_id = m.client_id
        			 WHERE milestone_type NOT IN ('note','notice')
        				   AND ou.user_id = '$user_id' AND milestone_status = '0' AND o.opportunity_status = 'A'
        				   AND milestone_date $d $date";

        $sql="SELECT milestone_title, milestone_detail,
		        d, milestone_date,
				milestone_start_time, milestone_end_time, milestone_type,
				milestone_status, client_businessname, opportunity_id, opportunity_stage,
				opportunity_total, client_id, permiso, orden
		FROM (
			 SELECT milestone_title, milestone_detail,
					DATE_FORMAT(milestone_date, '%d-%m-%Y') AS d, milestone_date,
					milestone_start_time, milestone_end_time, milestone_type,
					milestone_status, client_businessname, m.opportunity_id, o.opportunity_stage, o.opportunity_total, o.client_id, 'titular' AS permiso, milestone_date AS orden
			 FROM milestone AS m
				  INNER JOIN opportunity o ON o.opportunity_id = m.opportunity_id
				  INNER JOIN client c ON c.client_id = m.client_id
			 WHERE milestone_type NOT IN ('note','notice')
				   AND o.user_id = :user_id AND milestone_status = '0' AND o.opportunity_status = 'A'
				   AND milestone_date $d :date
			 $sql_last
       $sql_aditional
			) tareas
		 WHERE 1 ORDER BY orden DESC, opportunity_total DESC LIMIT $limit";
     //WHERE 1 ORDER BY orden DESC, opportunity_total DESC ";
		//";

		//var_dump($sql);
        $milestone = $this->pdo->prepare($sql);

        $milestone->bindValue(':user_id', $user_id);
        $milestone->bindValue(':date', $date);
        $milestone->execute();
        return $milestone->fetchAll(\PDO::FETCH_ASSOC);
    }


    ///////////////////////////////

    public function getTaskCount(string $user_id, string $date, string $when = "NOW")
    {
        $d = "=";

        $sql_last = "";

        if ($when == "LAST") {
            $d = "<";

            $sql_last="
				UNION
				SELECT COUNT(*) AS n, '' AS milestone_date
				FROM opportunity o
					INNER JOIN client c ON c.client_id = o.client_id
				WHERE
					o.user_id = $user_id  AND o.opportunity_status = 'A'
					AND (SELECT COUNT(m.milestone_id) FROM milestone m WHERE m.opportunity_id=o.opportunity_id)=0
        UNION
  				SELECT COUNT(*) AS n, '' AS milestone_date
  				FROM opportunity o
  					INNER JOIN client c ON c.client_id = o.client_id
  				WHERE
  					o.user_id = $user_id  AND o.opportunity_status = 'A'
            AND (SELECT COUNT(m.milestone_id) FROM milestone m WHERE m.opportunity_id=o.opportunity_id AND m.milestone_date > NOW())=0
			";

        }

        if ($when == "TOMORROW") {
            $d = ">";
        }

        $sql_aditional = "
        UNION
        SELECT COUNT(*) AS n, milestone_date
                FROM milestone AS m
                INNER JOIN opportunity o ON o.opportunity_id = m.opportunity_id
                INNER JOIN opportunity_user ou ON ou.opportunity_id = m.opportunity_id
                INNER JOIN client c ON c.client_id = m.client_id
                WHERE milestone_type NOT IN ('note','notice')
                AND ou.user_id = $user_id AND milestone_status = '0' AND o.opportunity_status = 'A'
                AND milestone_date $d $date";

        $milestone = $this->pdo->prepare("SELECT SUM(n) AS number, milestone_date
          FROM (
            SELECT COUNT(*) AS n, milestone_date
                FROM milestone AS m
                INNER JOIN opportunity o ON o.opportunity_id = m.opportunity_id
                INNER JOIN client c ON c.client_id = m.client_id
                WHERE milestone_type NOT IN ('note','notice')
                AND m.user_id = :user_id AND milestone_status = '0' AND o.opportunity_status = 'A'
                AND milestone_date $d :date
                $sql_aditional
                $sql_last
              ) tareas WHERE 1");
        $milestone->bindValue(':user_id', $user_id);
        $milestone->bindValue(':date', $date);
        $milestone->execute();
        return $milestone->fetchAll(\PDO::FETCH_ASSOC);
        //return $milestone->debugDumpParams();
    }
}
