<?php

namespace Models\PDO;

/**
 * Description of ClientRawMigration
 *
 * @author turkee
 */
class ClientRawMigration {
    protected $db;

    public function __construct($db) {
        $this->db = $db;
    }


    public function getByCuit($cuit) {
      $clientRaw = $this->db->prepare("
          SELECT
          `CODIGO`, `RAZON_SOC`, `CUIT`, `TITULAR`, `MAIL`
            FROM `client_raw`
          WHERE CUIT = :cuit
      ");
      $clientRaw->bindParam(':cuit', $cuit);
      $clientRaw->execute();

      return $clientRaw->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAll() {
        $clientRaw = $this->db->prepare(
          "SELECT
            `CODIGO`, `RAZON_SOC`, `DIRECCION`, `LOCALIDAD`, `COD_PCIA`,
            `TITULAR`, `CUIT`, `FACT_TIPO`, `TELEFONO0`, `TELEFONO1`,
            `TELEFONO2`, `TELEFONO3`, `FAX`, `FECHA_FACT`, `PROX_FACT`,
            `CODPOSTAL`, `FINGRESO`, `AGENTE`, `MAIL`, `WWW`, `STATUS`,
            `LEYEN`, `RUBRO`, `TIPO`, `TC`, `FP`, `CUITPAIS`, `NPAIS`
          FROM client_raw ORDER BY CUIT ASC;"
        );
        $clientRaw->execute();

        return $clientRaw->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function set($datos) {
      $clientRaw = $this->db->prepare(
        "INSERT IGNORE INTO client_raw
          (
              `CODIGO`, `RAZON_SOC`, `DIRECCION`, `LOCALIDAD`, `COD_PCIA`,
              `TITULAR`, `CUIT`, `FACT_TIPO`, `TELEFONO0`, `TELEFONO1`,
              `TELEFONO2`, `TELEFONO3`, `FAX`, `FECHA_FACT`, `PROX_FACT`,
              `CODPOSTAL`, `FINGRESO`, `AGENTE`, `MAIL`, `WWW`, `STATUS`,
              `LEYEN`, `RUBRO`, `TIPO`, `TC`, `FP`, `CUITPAIS`, `NPAIS`
          )  VALUES (
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?, ?, ?
          );
      ");
      $clientRaw->execute($datos);
    }

    public function truncate(){
      $clientRaw = $this->db->prepare("TRUNCATE `client_raw`;");
      $clientRaw->execute();
    }
}
