<?php

namespace Models\PDO;

/**
 * Description of Milestone
 *
 * @author turkee
 */
class Opportunity {
    protected $pdo;
    private $status = "A";
    private $type;

    public function __construct($pdo, string $status = "A", string $type = "") {
        $this->pdo = $pdo;
        $this->status = $status;
        $this->type = $type;
    }

    public function setStatus($status) {
        $this->status = $status;
    }
    
    public function setType($type) {
        $this->type = $type;
    }
    
    public function getCountAvailable() {
        $opportunity_type = "";
        
        if(!empty($this->type)) {
            $opportunity_type = "AND opportunity_type = :type";
        }
        
        $milestone = $this->pdo->prepare("SELECT COUNT(*) AS count "
                . "FROM opportunity "
                . "WHERE opportunity_status = :status "
                . "$opportunity_type LIMIT 1;");
        $milestone->bindValue(':status', $this->status);
        if(!empty($this->type)) {        
            $milestone->bindValue(':type', $this->type);
        }
        $milestone->execute();
        return $milestone->fetch(\PDO::FETCH_ASSOC)['count'];
    }

    public function getCountCustomer() {
        $opportunity_type = "";
        
        if(!empty($this->type)) {
            $opportunity_type = "AND opportunity_type = :type";
        }
        
        $milestone = $this->pdo->prepare("SELECT COUNT(DISTINCT client_id) AS count "
                . "FROM opportunity "
                . "WHERE opportunity_status = :status "
                . "$opportunity_type LIMIT 1;");
        $milestone->bindValue(':status', $this->status);
        if(!empty($this->type)) {        
            $milestone->bindValue(':type', $this->type);
        }
        $milestone->execute();
        return $milestone->fetch(\PDO::FETCH_ASSOC)['count'];                
    }
}