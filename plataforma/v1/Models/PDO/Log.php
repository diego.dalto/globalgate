<?php

namespace Models\PDO;

/**
 * Description of Log
 *
 * @author turkee
 */
class Log {
    private $db;
    private $user_id;

    public function __construct($db, $user_id) {
        $this->db = $db;
        $this->user_id = $user_id;
    }

    public function info($detail, $action, $key, $value) {
        $this->save('INFO', $detail, $action, $key, $value);
    }

    public function warn($detail, $action, $key, $value) {
        $this->save('WARN', $detail, $action, $key, $value);
    }

    public function debug($detail, $action, $key, $value) {
        $this->save('DEBUG', $detail, $action, $key, $value);
    }

    public function error($detail, $action, $key, $value) {
        $this->save('ERROR', $detail, $action, $key, $value);
    }

    public function changeClient($detail, $action, $key = NULL, $value = NULL, $value_old = NULL) {
        $this->save('CHANGE_CLIENT', $detail, $action, $key, $value, $value_old);
    }

    private function save($type, $detail, $action, $key, $value, $value_old = NULL) {
        $tableLog = $this->db->prepare("
            INSERT INTO log
            (`log_type`, `log_detail`, `log_action`, `log_key`, `log_value`, `log_value_old`, `user_id`)
                VALUES
            (:log_type, :log_detail, :log_action, :log_key, :log_value, :log_value_old, :user_id);
        ");
        $tableLog->bindValue(":log_type", $type);
        $tableLog->bindParam(":log_detail", $detail);
        $tableLog->bindParam(":log_action", $action);
        $tableLog->bindParam(":log_key", $key);
        $tableLog->bindParam(":log_value", $value);
        $tableLog->bindParam(":log_value_old", $value_old);
        $tableLog->bindParam(":user_id", $this->user_id);
        $tableLog->execute();
    }

    public function getChangeClient($key = "", $detail = "", $action = "", $limit = "LIMIT 50")
    {
      $where = " `log_type` = 'CHANGE_CLIENT' ";
      $where .= empty($key) ? "" : " AND `log_key` = '$key' ";
      $where .= empty($detail) ? "" : " AND `log_detail` = '$detail' ";
      $where .= empty($action) ? "" : " AND `log_action` = '$action' ";

      //return $where;
      $tableLog = $this->db->prepare("
        SELECT
             `log_id`, `log_detail`,
             `log_action`, `log_key`, `log_value`, `log_value_old`
        FROM log WHERE $where $limit;
      ");
      $tableLog->execute();

      //return $tableLog->debugDumpParams();
      return $tableLog->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getDistinctChangeClient($key = "", $detail = "", $action = "", $limit = "LIMIT 500") {
      $where = " AND `log_type` = 'CHANGE_CLIENT' ";
      $where .= empty($key) ? "" : " AND log_key = '$key'";
      $where .= empty($detail) ? "" : " AND log_detail = '$detail'";
      $where .= empty($action) ? "" : " AND log_action = '$action'";
      $where .= $limit;
      //return $where; 
      return $this->getDistinct('log_detail', $where);
    }

    public function getDistinctKey($type) {
      return $this->getDistinct('log_key', " AND `log_type` = '$type' ");
    }

    public function getDistinctAction($type) {
      return $this->getDistinct('log_action', " AND `log_type` = '$type' ");
    }

    public function getDistinct($column, $where = '') {
      $tableLogKey = $this->db->prepare(
        "SELECT
          DISTINCT `$column`
        FROM log
        WHERE 1=1 $where"
      );
      $tableLogKey->execute();
      return $tableLogKey->fetchAll(\PDO::FETCH_ASSOC);
    }
}
