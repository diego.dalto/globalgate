<?php

namespace Models\Conseptual;
/**
 *
 * @author turkee
 */
abstract class aMySQL {
    
    abstract protected function delete(...$args);
        
    abstract protected function insert(...$args);
    
    abstract protected function update(...$args);
    
    abstract protected function save(...$args);
    
    abstract protected static function select(...$args);
    
}
