<?php

namespace Models\MySQL;

use PDO;
use Models\MySQL\Table as Table;

/**
 * Description of DataPDO
 *
 * @author turkee
 */
class MyPDO extends PDO
{
    private $host = "mysql:host=localhost;dbname=plataforma";
    private $username = "mild";
    private $password = "b3d0y4gg79";

    public function __construct() {
        parent::__construct($this->host, $this->username, $this->password);
        parent::setAttribute(PDO::ATTR_PERSISTENT, true);
        parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function table($name) {
        return new Table($this, $name);
    }
}
