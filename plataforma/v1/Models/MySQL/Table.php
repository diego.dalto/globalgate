<?php

namespace Models\MySQL;

/**
 * Description of Table
 *
 * @author turkee
 */
class Table implements \Models\Contracts\iMySQL {
    protected $db;
    protected $table_name;
    
    public function __construct($db, $table_name) {
        $this->db = $db;
        $this->table_name = $table_name;
    }
    
    /*
     * @Param array $where
     * $where = array(
     *   paper_id => 1, paperwork_id => 2
     * );
     */
    public function delete(array $where) {
        $w = $this->parseString(" AND ", array_keys($where));
        
        $sentencia = $this->db->prepare("DELETE FROM  ".$this->table_name
                ." WHERE $w;");
        
        $values = array_values($where);

        foreach ($values as $key => $val) {
            $sentencia->bindValue($key+1, $val);
        }
        $sentencia->execute();
    }
    
    /*
     * @Param array insert
     * $insert = array(
     *   0 => array(paper_id => 1, paper_su => 2, paper_ns => 3),
     *   1 => array(paper_id => 5, paper_su => 4, paper_ns => 7)
     * );
     */
    public function insert(array $insert) 
    {
        $key = implode(", ", array_keys($insert[0]));
        $val = implode(", ", array_fill(0, count($insert[0]), "?"));
        
        $sentencia = $this->db->prepare(
            "INSERT INTO ".$this->table_name
                ." ($key) VALUES ($val);");
        
        $values = array_values($insert);
        
        foreach ($values as $key => $row) {
            $sentencia->execute( array_values($row) );
        }
        
        return $sentencia;
    }

    /*
     * @Param array insert
     * $insert = array(
     *   0 => array(paper_id => 1, paper_su => 2, paper_ns => 3),
     *   1 => array(paper_id => 5, paper_su => 4, paper_ns => 7)
     * );
     */
    public function save(array $insert, array $update) 
    {
        $key = implode(", ", array_keys($insert[0]));
        $val = implode(", ", array_fill(0, count($insert[0]), "?"));
        
        $duplicate = $this->parseString(", ", array_keys($update[0]));
        $table = $this->table_name;
        $sentencia = $this->db->prepare("INSERT INTO $table "
                . "($key) VALUES ($val)
                    ON DUPLICATE KEY UPDATE ".$table."_id = LAST_INSERT_ID(".$table."_id), $duplicate;");

        
        foreach ($insert as $key => $in) {
            $u = array_values($update[$key]);
            $row = array_merge(array_values($in), $u);
            $sentencia->execute( array_values($row) );
        }
        
        return $sentencia;
    }

    /*
     * @Param array $set
     * $set = array(
     *   paper_id => 1, paper_su => 2, paper_ns => 3
     * );
     * 
     * @Param array $where
     * $where = array(
     *   paper_id => 1, paperwork_id => 2
     * );
     */
    public function update(array $set, array $where) {
        
        $columns = $this->parseString(", ", array_keys($set));
        $w = $this->parseString(" AND ", array_keys($where));
        
        $sentencia = $this->db->prepare(
            "UPDATE ".$this->table_name
            . " SET $columns WHERE $w;");
        
        $values = array_merge(array_values($set), array_values($where));
        
        foreach ($values as $key => $val) {
            $sentencia->bindValue($key+1, $val);
        }
        $sentencia->execute();
        
        return $sentencia->rowCount();
    }
    
    /*
     * @Param array $where
     * $where = array(
     *   paper_id => 1, paperwork_id => 2
     * );
     * 
     * @Param string $class
     * Name class
     */
    public function select(array $where, $class = "") {
        $w = $this->parseString(" AND ", array_keys($where));
        
        $sentencia = $this->db->prepare("SELECT * "
                . "FROM ".$this->table_name
                . " WHERE $w;");

        $values = array_values($where);

        foreach ($values as $key => $val) {
            $sentencia->bindValue($key+1, $val);
        }
        $sentencia->execute();
        
        if(empty($class)):
            return $sentencia->fetchAll(\PDO::FETCH_ASSOC);
        else:
            return $sentencia->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__."\\Tables\\$class");
        endif;
    }
    
    private function concat($param) {
        return "$param = ?";
    }
    
    private function parseString($glue, $array) {
        return implode($glue, array_map(array($this, "concat"), $array));
    }
}