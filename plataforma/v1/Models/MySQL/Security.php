<?php

namespace Models\MySQL;

use Models\MySQL\MyPDO;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Security {
    private $userId;
    private $sessionId;
    private $key_app = "GLOBALGATE-N-17";
    private $router;
    const ERROR = "ERROR";
    const OK = "OK";

    public function __construct($userId, $sessionId, $router) {
        if(empty($this->sessionId) || empty($this->userId))
        {
            new \Exception("Invalid arguments. Use USER_ID and SESSION_ID");
        }
        
        $this->userId = $userId;
        $this->sessionId = $sessionId;
        $this->router = $router;
    }
    
    public function __invoke(Request $request, Response $response, callable $next) {
        $path = $request->getUri()->getPath();
        $outside = ["index", "login", "index/empty", "index/unmatch"];

        $res = $next($request, $response);
        
        try {
            $pdo = new MyPDO();
            $table = $pdo->table("user_session");

            $session_info = $table->select([
                "session_id" => $this->sessionId, 
                "user_id" => $this->userId
            ]);
            $session_count = count($session_info);            

            if($session_count > 0 && in_array($path, $outside)) 
            {
                return $res->withRedirect($this->router->pathFor('dashboard'), 303);
            } 
            elseif($session_count < 1 && !in_array($path, $outside))
            {
                return $res->withRedirect($this->router->pathFor('index'), 403);
            }
        }
        catch(\PDOException $e)
        {
            if(!in_array($path, $outside)) {
                return $res->withRedirect($this->router->pathFor('index'), 403);
            }
        }        
        return $res;
    }
    
    public function getSHA1() {
        try {
            $pdo = new MyPDO();
            $table = $pdo->table("user_session");

            $session_info = $table->select([
                "session_id" => $this->sessionId, 
                "user_id" => $this->userId
            ]);

            if(count($session_info) > 0)
            {
                $session_info = $session_info[0];

                $keyid = $session_info['key_id'];

                return sha1("".$this->sessionId."".$keyid."".$this->key_app."");
            }
            else
                return self::ERROR;
        }
        catch(\PDOException $e)
        {
            return self::ERROR;
        }
    }

    public function getUserId() {
        return $this->userId;
    }
    
    public function getSessionId() {
        return $this->sessionId;
    }
    
    public function validateSHA1($sha1) {
        $sha_valida = $this->getSHA1();

        if(!strstr($sha_valida, self::ERROR)):
            return ($sha_valida == $sha1) ? self::OK : self::ERROR;
        else:
            return $sha_valida;
        endif;
    }
}