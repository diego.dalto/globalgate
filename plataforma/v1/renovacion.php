<?php

$app->group('/renovacion', function () {

    $this->get('/crear', function ($request, $response, $args) {
        
    });

    $this->get('/editar/{id:[0-9]+}', function ($request, $response, $args) {

        $twig_params = [
            'head' => array(),
            'path' => '../template/',
            'action' => 'ListadoRenovaciones'
        ];

        ////////   START OPORTUNITY DETAIL   /////////
        $queryOpportunity = $this->db->prepare(
            "SELECT serials_contract_enddate, serials_contract_number,
                serials_contract_status, 
                opportunity_detail_price, opportunity_detail_productid, 
                opportunity_detail_description, opportunity_detail_moneda, 
                opportunity_salenumber, client.client_id, client_businessname, 
                brand_name, opportunity.opportunity_id 
            FROM opportunity
                INNER JOIN opportunity_detail ON opportunity_detail.oportunity_id = opportunity.opportunity_id
                INNER JOIN serials_contract ON serials_contract.opportunity_detail_id = opportunity_detail.opportunity_detail_id
                INNER JOIN client ON client.client_id = opportunity_detail.opportunity_detail_id
                INNER JOIN product_brand ON product_brand.brand_id = opportunity.brand_id
            WHERE serials_contract_type = 'Serial' AND 
                (serials_contract_number IS NOT NULL AND serials_contract_number!='') 
                AND client.client_id = :client_id 
            ORDER BY serials_contract_enddate ASC, brand_name ASC"
        );
        $queryOpportunity->bindParam(':client_id', $args["id"]);
        $queryOpportunity->execute();
        $twig_params['opportunities'] = $queryOpportunity->fetchAll(PDO::FETCH_ASSOC);
        ////////// END OPPORTUNITY DETAIL ////////////
        
        ////////   START CLIENT   /////////
        $queryClient = $this->db->prepare(
            "SELECT SUM(opportunity_total) AS total_sail, client_businessname, 
                client_agent, client.client_id
             FROM opportunity
             INNER JOIN client ON client.client_id = opportunity.client_id
             WHERE opportunity_status = 'V' AND client.client_id = :id;"
        );
        $queryClient->bindValue(':id', $args["id"]);
        $queryClient->execute();
        $twig_params['client'] = $queryClient->fetch(PDO::FETCH_ASSOC);            
        ////////// END CLIENT ////////////        
        
        ////////   START CONTACTS   /////////
        $queryContact = $this->db->prepare(
            "SELECT a.contact_id, contat_createdate, contact_name, 
                contact_lastname, contact_email, contact_phone, 
                contact_celphone, contact_status, contact_type, contact_area, 
                client_id, user_id, b.opportunity_contact_id
             FROM contact_client a, opportunity_contacts b
             WHERE a.contact_id=b.contact_id  AND client_id = :id;"
        );
        $queryContact->bindValue(':id', $args["id"]);
        $queryContact->execute();
        $twig_params['contacts'] = $queryContact->fetchAll(PDO::FETCH_ASSOC);            
        ////////// END CONTACTS ////////////

        $this->log->info("Se ingresó a editar renovación", "SELECT", "", 0);

        return $this->view->render($response, 'renewal_edit.twig', $twig_params);
    });

    $this->get('/listado', function ($request, $response, $args) {

        $data = $request->getParsedBody();
        $where = '';

        $query = $this->db->prepare(
            "SELECT brand_id,brand_name
                FROM product_brand c
            WHERE 1=1 ORDER BY brand_name ASC"
        );
        $query->execute();
        $marcas = $query->fetchAll(PDO::FETCH_ASSOC);

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['client']) && $data['client'] != "") {
                $texto = $data['client'];
                $where .= "AND client_businessname like '%" . $texto . "%' ";
            }
        }

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['serial']) && $data['serial'] != "") {
                $texto = $data['serial'];
                $where .= "AND serials_contract_number like '%" . $texto . "%' ";
            }
        }
        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['marca']) && $data['marca'] != "") {
                $texto = $data['marca'];
                $where .= "AND product_brand.brand_id = '" . $texto . "' ";
            }
        }
        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['estado']) && $data['estado'] != "") {
                $texto = $data['estado'];
                $where .= "AND serials_contract_status = '" . $texto . "' ";
            }
        }
        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['producto']) && $data['producto'] != "") {
                $texto = $data['producto'];
                $where .= "AND (opportunity_detail_description like '%" . $texto . "%' OR opportunity_detail_productid like '%" . $texto . "%')";
            }
        }
        if (isset($data['fechadesde']) && $data['fechadesde'] != "") {
            $fecha = explode('/', $data['fechadesde']);
            $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
            $where .= 'AND s.serials_contract_date >= "' . $date . '" ';
        }
        if (isset($data['fechahasta']) && $data['fechahasta'] != "") {

            $fecha = explode('/', $data['fechahasta']);
            $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
            $where .= 'AND serials_contract_enddate <= "' . $date . '" ';
        }

        $sql = "SELECT serials_contract_enddate, serials_contract_number,
            serials_contract_status, 
            opportunity_detail_price, opportunity_detail_productid, 
            opportunity_detail_description, opportunity_detail_moneda, 
            opportunity_salenumber, client.client_id, client_businessname, brand_name
        FROM opportunity
            INNER JOIN opportunity_detail ON opportunity_detail.oportunity_id = opportunity.opportunity_id
            INNER JOIN serials_contract ON serials_contract.opportunity_detail_id = opportunity_detail.opportunity_detail_id
            INNER JOIN client ON client.client_id = opportunity.client_id
            INNER JOIN product_brand ON product_brand.brand_id = opportunity.brand_id
        WHERE serials_contract_type = 'Serial' AND (serials_contract_number IS NOT NULL AND serials_contract_number!='' ) $where 
        ORDER BY serials_contract_enddate ASC";

        $query = $this->db->prepare($sql);
        $query->execute();
        $renewals = $query->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se ingresó al listado de renovación", "SELECT", "", 0);

        return $this->view->render($response, 'renewal_list.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'action' => 'ListadoRenovaciones',
                    'renewals' => $renewals,
                    'brands' => $marcas,
                    'params' => $data
        ]);
    })->setName('renewal-list');

    $this->put('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        
    });

    $this->post('/crear/{id:[0-9]+}', function ($request, $response, $args) {
        
    });
});
