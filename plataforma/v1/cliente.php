<?php

$app->group('/cliente', function () {
    $this->post('/crear', function ($request, $response, $args) {
        $query = $this->db->prepare("INSERT INTO client (client_status) VALUES ('N')");
        $query->execute();
        $user = $this->db->lastInsertId();

        $this->log->info("Se creó cliente", "INSERT", "client_id", $user);

        return $response->withJson($user);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        $id = $request->getAttribute('id');

        $query = $this->db->prepare("SELECT client_id, client_origin, client_code, client_businessname, client_fantasyname, client_address, client_city,
                                        client_state, client_contact, client_cuit, client_invoicetype, client_phone0, client_phone1,
                                        client_phone2, client_phone3, client_fax, client_invoicedate, client_newtinvoicedate,
                                        client_cp, client_createdate, client_notes, client_agent, client_email, client_www,
                                        client_status, client_leyen, client_entry, client_type, client_tc, client_fc, client_cuitpais,
                                        client_npais, client_activity, client_identificationtype
                                    FROM client
                                    WHERE client_id=" . $id . "");

        $query->execute();
        $cliente = $query->fetchAll(PDO::FETCH_ASSOC);

        if (count($cliente) > 0) {
            foreach ($cliente as $key => &$c) {
                $q = $this->db->prepare(
                    "
                        SELECT provincia
                            FROM provincias
                        WHERE code = :code LIMIT 1;"
                );
                $q->bindParam(':code', $c['client_state']);
                $q->execute();
                $c['provincia'] = $q->fetch(PDO::FETCH_ASSOC);
            }
        }

        /////////////////////////////////////////////////////////
        $query = $this->db->prepare("SELECT MAX(client_cuit) AS number
                                        FROM client
                                        WHERE client_type = 'Temporal';");

        $query->execute();
        $temporal = $query->fetch(PDO::FETCH_ASSOC);

        if (count($temporal) > 0) {
            $temporal['number'] ++;
        } else {
            $temporal['number'] = "99-00000000-1";
        }

        /////////////////////////////////////////////////////////

        $query = $this->db->prepare("SELECT client_id, client_origin, client_code, client_businessname, client_fantasyname, client_address, client_city,
                                                client_state, client_contact, client_cuit, client_invoicetype, client_phone0, client_phone1,
                                                client_phone2, client_phone3, client_fax, client_invoicedate, client_newtinvoicedate,
                                                client_cp, client_createdate, client_notes, client_agent, client_email, client_www,
                                                client_status, client_leyen, client_entry, client_type, client_tc, client_fc, client_cuitpais,
                                                client_npais,client_activity
                                            FROM client
                                            WHERE client_type='Integrador' ");

        $query->execute();
        $integradores = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////////

        /* $query = $this->db->prepare("SELECT CONCAT(a.nombre, ' - ', b.nombre, ' - ', c.nombre ) AS ciudades
          FROM map_provincias  a, map_departamentos b, map_localidades c
          WHERE b.provincia_id=a.id AND c.departamento_id=b.id ORDER BY a.nombre, b.nombre, c.nombre ASC"); */

        $query = $this->db->prepare("
            SELECT  UPPER(provincia) AS provincia, UPPER(localidad) AS nombre, code
            FROM `localidades`
                INNER JOIN provincias ON provincias.id = localidades.id_privincia
            ORDER BY provincia ASC, localidad ASC
        ");
        $query->execute();
        $ciudades = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////////

        $query = $this->db->prepare("SELECT code,UPPER(provincia) AS nombre
                                        FROM provincias a
                                    WHERE 1=1 ");
        $query->execute();
        $provincias = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////////

        $actividades = ",Agropecuaria,Automotriz,Bancos y Financieras,Comercial,Construcción,Corporativo,Educación,Electrónica,Entretenimiento,Gas y Minería,Gobierno,Hogar,Industria,Informática y Telecomunicaciones,Legal,Organizaciones sin fines de lucro,Pesquera,Petrolera,Pyme,Salud,Seguros,Siderúrgica,Transporte,Turismo,Otra";

        $actividades = explode(",", $actividades);

        //////////////////////////

        $query = $this->db->prepare("
            SELECT contact_id, contat_createdate, contact_name, contact_lastname, contact_email, contact_phone,
                contact_celphone, contact_status, contact_type, contact_area, client_id, a.user_id, b.user_name, b.user_lastname
            FROM contact_client a, users b
            WHERE client_id=" . $id . " AND a.user_id=b.user_id ORDER BY contact_id DESC");

        $query->execute();
        $contacts_client = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////////

        $this->log->info("Se ingresó a la edición de cliente", "SELECT", "client_id", $id);

        return $this->view->render($response, 'customer_edit.twig', [
                    'head' => array(),
                    'path' => '../../template/',
                    'action' => 'EditarRegistro',
                    'cliente_info' => $cliente[0],
                    'ciudades' => $ciudades,
                    'provincias' => $provincias,
                    'actividades' => $actividades,
                    'integradores' => $integradores,
                    'cliente_contactos' => $contacts_client,
                    'temporal' => $temporal['number']
        ]);
    })->setName('client-edit');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/listado', function ($request, $response, $args) {
        $query = $this->db->prepare("
                DELETE FROM client
                    WHERE client_status IN ('N')
                AND client_createdate < DATE_SUB(NOW(), INTERVAL 45 MINUTE);
        ");
        $query->execute();

        //////////////////////////////////////

        $data = $request->getParsedBody();
        $where = '';
        $join = '';
        $search = '';
        $search_type = '';
        $search_activity = '';

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['search']) && $data['search'] != "") {
                $search = $data['search'];
                $texto = trim($data['search']);
                $join = ' LEFT JOIN contact_client cc ON cc.client_id = c.client_id ';

                $where .= "AND (client_code like '%" . $texto . "%' OR "
                        . "client_cuit like '%" . $texto . "%' OR "
                        . "client_businessname like '%" . $texto . "%' OR "
                        . "client_city like '%" . $texto . "%' OR "
                        . "client_state like '%" . $texto . "%' OR "
                        . "client_fantasyname LIKE '%" . $texto . "%' OR "
                        . "client_contact LIKE '%" . $texto . "%' OR "
                        . "cc.contact_name LIKE '%" . $texto . "%' OR "
                        . "cc.contact_lastname LIKE '%" . $texto . "%') ";
            }

            if (isset($data['search_type']) && $data['search_type'] != "") {
                $search_type = $data['search_type'];
                $texto = trim($data['search_type']);
                $where .= "AND client_type='" . $texto . "' ";
            }

            if (isset($data['search_activity']) && $data['search_activity'] != "") {
                $search_activity = $data['search_activity'];
                $texto = trim($data['search_activity']);
                $where .= "AND client_activity='" . $texto . "' ";
            }

            if (isset($data['search_status']) && $data['search_status'] != "") {
                $search_activity = $data['search_status'];
                $texto = trim($data['search_status']);
                $where .= "AND client_status='" . $texto . "' ";
            }
        }

        //////////////////////////////////////

        $query = $this->db->prepare("SELECT c.client_id, client_origin, client_code, client_businessname, client_fantasyname, client_address, client_city,
                                        client_state, client_contact, client_cuit, client_invoicetype, client_phone0, client_phone1,
                                        client_phone2, client_phone3, client_fax, client_invoicedate, client_newtinvoicedate,
                                        client_cp, client_createdate, client_notes, client_agent, client_email, client_www,
                                        client_status, client_leyen, client_entry, client_type, client_tc, client_fc, client_cuitpais,
                                        client_npais,client_activity,provincia
                                     FROM client AS c
                                     LEFT JOIN provincias ON provincias.code = c.client_state
                                     $join
                                     WHERE client_status IN ('A','I') " . $where . "");

        $query->execute();
        $cliente = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////////////////////

        $actividades = ",Agropecuaria,Automotriz,Bancos y Financieras,Comercial,Construcción,Corporativo,Educación,Electrónica,Entretenimiento,Gas y Minería,Gobierno,Hogar,Industria,Informática y Telecomunicaciones,Legal,Organizaciones sin fines de lucro,Pesquera,Petrolera,Pyme,Salud,Seguros,Siderúrgica,Transporte,Turismo,Otra";

        $actividades = explode(",", $actividades);

        //////////////////////////////////////

        $this->log->info("Se ingresó al listado de cliente", "SELECT", "", 0);

        return $this->view->render($response, 'customer_list.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'action' => 'ListadoUsuarios',
                    'cliente_info' => $cliente,
                    'actividades' => $actividades,
                    'search' => $search,
                    'search_type' => $search_type,
                    'search_activity' => $search_activity
        ]);
    })->setName('client-list');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/name/{name}', function ($request, $response, $args) {
        $name = "%" . $args['name'] . "%";

        $clientTable = $this->db->prepare("SELECT client_id, client_businessname "
                . "FROM client "
                . "WHERE client_businessname LIKE :name LIMIT 1;");
        $clientTable->bindParam(':name', $name);
        $clientTable->execute();
        $client = $clientTable->fetchAll(PDO::FETCH_ASSOC);

        return $response->withJson($client);
    });

    $this->put('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        $id = $args["id"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;
        $client = [];
        $params = [
            "msg" => "Algo va mal",
            "type" => 0
        ];
        if ($milestone["client_type"] == "Temporal" && $milestone["client_cuit"] == "99-99999999-9") {
            $query = $this->db->prepare("SELECT client_cuit FROM client WHERE client_cuit LIKE '99%' ORDER BY client_id DESC LIMIT 0,1");
            $query->execute();
            $max = $query->fetchAll(PDO::FETCH_ASSOC);

            if (count($max) == 0) {
                $maximo = "99-00000000-0";
            } else {
                $maximo = $max[0]["client_cuit"];
            }

            $maximo = str_replace("A", "", $maximo);
            $maximo = str_replace("-", "", $maximo);
            $maximo = $maximo + 1;
            $milestone["client_cuit"] = "" . substr($maximo, 0, 2) . "-" . substr($maximo, 2, 8) . "-" . substr($maximo, -1, 1);
        } elseif ($milestone["client_type"] != "Temporal" && $milestone["client_cuit"] != "99-99999999-9") {
            $cuit = "%" . $milestone["client_cuit"] . "%";

            $tableClient = $this->db->prepare("
                SELECT client_cuit
                    FROM client
                WHERE client_cuit LIKE :cuit AND client_id!=:id
            ");

            $tableClient->bindParam(':cuit', $cuit);
            $tableClient->bindParam(':id', $id);
            $tableClient->execute();

            $client = $tableClient->fetchAll(PDO::FETCH_ASSOC);
        }
        if (count($client) > 0) {
            $params = [
                "msg" => "El cliente ya existe",
                "type" => 1
            ];
        } else {
            $query = $this->db->prepare("
                UPDATE client SET
                    client_businessname=:cclient_businessname, client_fantasyname=:cclient_fantasy_name, client_cuit=:cclient_cuit, client_origin=:cclient_origin, client_code=:cclient_code,
                    client_activity=:cclient_activity, client_email=:cclient_email, client_agent=:cclient_agent, client_www=:cclient_www, client_address=:cclient_address, client_city=:cclient_city,
                    client_phone0=:cclient_phone0, client_fax=:cclient_fax, client_cp=:cclient_cp, client_state=:cclient_state, client_phone1=:cclient_phone1,
                    client_status=:cclient_status, client_type=:cclient_type, client_notes=:cclient_notes
                 WHERE client_id=:id
            ");
            $query->bindParam(':cclient_businessname', $milestone["client_businessname"]);
            $query->bindParam(':cclient_fantasy_name', $milestone["client_fantasyname"]);
            $query->bindParam(':cclient_cuit', $milestone["client_cuit"]);
            $query->bindParam(':cclient_origin', $milestone["client_origin"]);
            $query->bindParam(':cclient_code', $milestone["client_code"]);
            $query->bindParam(':cclient_activity', $milestone["client_activity"]);
            $query->bindParam(':cclient_email', $milestone["client_email"]);
            $query->bindParam(':cclient_agent', $milestone["client_agent"]);
            $query->bindParam(':cclient_www', $milestone["client_www"]);
            $query->bindParam(':cclient_address', $milestone["client_address"]);
            $query->bindParam(':cclient_city', $milestone["client_city"]);
            $query->bindParam(':cclient_phone0', $milestone["client_phone0"]);
            $query->bindParam(':cclient_fax', $milestone["client_fax"]);
            $query->bindParam(':cclient_cp', $milestone["client_cp"]);
            $query->bindParam(':cclient_state', $milestone["client_state"]);
            $query->bindParam(':cclient_phone1', $milestone["client_phone1"]);
            $query->bindParam(':cclient_status', $milestone["client_status"]);
            $query->bindParam(':cclient_type', $milestone["client_type"]);
            $query->bindParam(':cclient_notes', $milestone["client_notes"]);
            $query->bindParam(':id', $id);
            $query->execute();
            $params = [
                "msg" => "Cliente guardado!",
                "type" => 2
            ];

            if ($query->rowCount() > 0) {
                $this->log->info("Se modificó el cliente", "UPDATE", "client_id", $id);
            }
        }
        return $response->withJson($params);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->post('/crearContactos/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $params = [
            "msg" => "Algo va mal",
            "type" => 0
        ];

        $tableContact = $this->db->prepare("
            SELECT *
            FROM contact_client
            WHERE contact_email = :email AND client_id = :id
        ");
        $tableContact->bindParam(':email', $milestone["contact_email"]);
        $tableContact->bindParam(':id', $milestone["contact_cliente_id"]);
        $tableContact->execute();

        $contact = $tableContact->fetchAll(PDO::FETCH_ASSOC);

        if (count($contact) > 0) {
            $params = [
                "msg" => "El contacto ya existe",
                "type" => 1
            ];
        } else {
            $query = $this->db->prepare("
                INSERT IGNORE INTO contact_client (
                    contact_name, contact_lastname, contact_email,
                     contact_phone, contact_celphone, contact_status, contact_type,
                    client_id, user_id, contact_area
                ) VALUES (
                    :cname,:clast,:cemail,:cphone,:ccphone,:cstatus,
                    :ctype,:ccid,:userid,:carea
                );
            ");

            $query->bindParam(':cname', $milestone["contact_name"]);
            $query->bindParam(':clast', $milestone["contact_lastname"]);
            $query->bindParam(':cemail', $milestone["contact_email"]);
            $query->bindParam(':cphone', $milestone["contact_phone"]);
            $query->bindParam(':ccphone', $milestone["contact_celphone"]);
            $query->bindParam(':cstatus', $milestone["contact_status"]);
            $query->bindParam(':ctype', $milestone["contact_type"]);
            $query->bindParam(':ccid', $milestone["contact_cliente_id"]);
            $query->bindParam(':userid', $userId);
            $query->bindParam(':carea', $milestone["contact_area"]);
            $query->execute();

            $params = [
                "msg" => "Contacto guardado!",
                "type" => 2
            ];

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se creó contacto en cliente", "INSERT", "contact_id", $last_id);
            }
        }
        return $response->withJson($params);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/eliminar/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        try {
            $query = $this->db->prepare("DELETE FROM contact_client WHERE contact_id=:cid ");
            $query->bindParam(':cid', $milestone["contact_id"]);
            $query->execute();

            $this->log->info("Se eliminó contacto de cliente", "DELETE", "contact_id", $milestone["contact_id"]);

            return $response->withJson("OK");
        } catch (\PDOException $e) {
            return $response->withJson("FOREIGN");
        }
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/eliminarCliente/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("UPDATE client SET client_status='I' WHERE client_id=:cid ");
        $query->bindParam(':cid', $milestone["client_id"]);
        $query->execute();

        $this->log->info("Se eliminó cliente", "UPDATE", "client_id", $milestone["client_id"]);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/editarContactos/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $params = [
            "msg" => "Algo va mal",
            "type" => 0
        ];

        $tableContact = $this->db->prepare("
            SELECT *
            FROM contact_client
            WHERE contact_email = :email AND client_id = :id AND contact_id!=:cid
        ");
        $tableContact->bindParam(':email', $milestone["contact_email"]);
        $tableContact->bindParam(':cid', $milestone["contact_id"]);
        $tableContact->bindParam(':id', $milestone["contact_cliente_id"]);
        $tableContact->execute();

        $contact = $tableContact->fetchAll(PDO::FETCH_ASSOC);

        if (count($contact) > 0) {
            $params = [
                "msg" => "El contacto ya existe",
                "type" => 1
            ];
        } else {
            $query = $this->db->prepare("
                UPDATE contact_client SET
                    contact_name=:cname, contact_lastname=:clast,
                    contact_email=:cemail, contact_phone=:cphone,
                    contact_celphone=:ccphone, contact_status=:cstatus,
                    contact_type=:ctype, user_id=:userid, contact_area=:carea
                WHERE contact_id=:cid
            ");
            $query->bindParam(':cname', $milestone["contact_name"]);
            $query->bindParam(':clast', $milestone["contact_lastname"]);
            $query->bindParam(':cemail', $milestone["contact_email"]);
            $query->bindParam(':cphone', $milestone["contact_phone"]);
            $query->bindParam(':ccphone', $milestone["contact_celphone"]);
            $query->bindParam(':cstatus', $milestone["contact_status"]);
            $query->bindParam(':ctype', $milestone["contact_type"]);
            $query->bindParam(':userid', $userId);
            $query->bindParam(':carea', $milestone["contact_area"]);
            $query->bindParam(':cid', $milestone["contact_id"]);
            $query->execute();

            $params = [
                "msg" => "Contacto actualizado!",
                "type" => 2
            ];

            $this->log->info("Se editó cliente", "UPDATE", "contact_id", $milestone["contact_id"]);
        }
        return $response->withJson($params);
    });

    $this->group('/import', function () {
        $this->get('/csv', function ($request, $response, $args) {
            $fileName = __DIR__."/../load_files/20180804.txt";
            if (($gestor = fopen($fileName, "r")) !== false) {
                $fila = 1;

                $clientRawMigration = new \Models\PDO\ClientRawMigration($this->db);
                $clientRawMigration->truncate();

                $tableLog = $this->db->prepare("DELETE FROM `log` WHERE log_type = 'CHANGE_CLIENT';");
                $tableLog->execute();

                while (!feof($gestor)) {
                    $text = fgets($gestor);
                    $s = ['","', '",', ',"'];
                    $r = ['~,~', '~,', ',~'];
                    $viri = str_replace($s, $r, $text);
                    $viri = trim($viri);
                    $viri = trim($viri, '"');
                    $viri = "~$viri~";

                    $datos = str_getcsv($viri, ",", "~");
                    $countColumns = count($datos);

                    //Descarto primer linea siempre y cuando
                    //sean los nombres de columna
                    if ($fila == 1 && in_array("razon_soc", $datos)) {
                        $this->log->info("Se omitio registro de primer fila (encabezado de los nombres de columnas)", "INSERT", "", "");
                    } elseif ($countColumns == 28) {
                        //var_dump($datos);
                        $clientRawMigration->set($datos);
                    } else {
                        $this->log->error("Cantindad de columnas ($countColumns) en fila: ".$fila++, "INSERT", "", "");
                    }
                }

                fclose($gestor);
                //unlink($fileName);
                #*/5 * * * * wget http://{ip}/cliente/import/csv

                /***************************************************/
                /********** MODULO DE CAMBIO DE CLIENTE ************/
                /***************************************************/

                $clients_raw = $clientRawMigration->getAll();

                if (count($clients_raw) > 0) {
                    $clientDB = new \Models\PDO\Client($this->db);
                    $clientVariant = new \Models\PDO\ClientVariant($this->db);

                    foreach ($clients_raw as $key => $client_raw) {
                        //Clear string only number
                        $cuit = preg_replace('/[^0-9]/', '', $client_raw['CUIT']);
                        $clients = [];

                        if (!empty($cuit)) {
                            $clients = $clientDB->getByCuit($cuit);

                            if (count($clients) > 0) {
                                foreach ($clients as $key => $client) {
                                    if ($client_raw['CODIGO'] == $client['client_code']) {
                                        //Client
                                        if (trim(strtolower($client_raw['RAZON_SOC'])) != trim(strtolower($client['client_businessname']))) {
                                            $this->log->changeClient($client['client_cuit'], "UPDATE", "client_businessname", $client_raw['RAZON_SOC'], $client['client_businessname']);
                                        }

                                        if (trim(strtolower($client_raw['DIRECCION'])) != trim(strtolower($client['client_address']))) {
                                            $this->log->changeClient($client['client_cuit'], "UPDATE", "client_address", $client_raw['DIRECCION'], $client['client_address']);
                                        }

                                        if (trim(strtolower($client_raw['LOCALIDAD'])) != trim(strtolower($client['client_city']))) {
                                            $this->log->changeClient($client['client_cuit'], "UPDATE", "client_city", $client_raw['LOCALIDAD'], $client['client_city']);
                                        }

                                        if (trim(strtolower($client_raw['COD_PCIA'])) != trim(strtolower($client['client_state']))) {
                                            $this->log->changeClient($client['client_cuit'], "UPDATE", "client_state", $client_raw['COD_PCIA'], $client['client_state']);
                                        }

                                        if (trim(strtolower($client_raw['FACT_TIPO'])) != trim(strtolower($client['client_invoicetype']))) {
                                            $this->log->changeClient($client['client_cuit'], "UPDATE", "client_invoicetype", $client_raw['FACT_TIPO'], $client['client_invoicetype']);
                                        }

                                        if (trim(strtolower($client_raw['CODPOSTAL'])) != trim(strtolower($client['client_cp']))) {
                                            $this->log->changeClient($client['client_cuit'], "UPDATE", "client_cp", $client_raw['CODPOSTAL'], $client['client_cp']);
                                        }

                                        if (trim(strtolower($client_raw['MAIL'])) != trim(strtolower($client['client_email']))) {
                                            $this->log->changeClient($client['client_cuit'], "UPDATE", "client_email", $client_raw['MAIL'], $client['client_email']);
                                        }
                                    } else {
                                        //ClientVariant
                                        if(!empty($client_raw['cuit'])){
                                          $clientVariant->set($client_raw);
                                        }
                                    }
                                }
                            } else {
                                //Nuevo cliente
                                $this->log->changeClient($client_raw['CUIT'], "INSERT");
                            }
                        }
                    }
                } else {
                    $this->log->changeClient("TABLA client_raw SIN CAMBIOS", "EMPTY");
                }


                /***************************************************/
            } else {
                $this->log->error("No se encontro el archivo", "READ", "", "");
            }
        });

        $this->get('/check', function ($request, $response, $args) {
            // Traemos la lista de clientes nueva
            $clientRawMigration = new \Models\PDO\ClientRawMigration($this->db);
            $clients_raw = $clientRawMigration->getAll();

            if (count($clients_raw) > 0) {
                $clientDB = new \Models\PDO\Client($this->db);

                $clientVariant = new \Models\PDO\ClientVariant($this->db);

                foreach ($clients_raw as $key => $client_raw) {
                    //Clear string only number
                    $cuit = preg_replace('/[^0-9]/', '', $client_raw['CUIT']);
                    $clients = [];

                    if (!empty($cuit)) {
                        $clients = $clientDB->getByCuit($cuit);

                        if (count($clients) > 0) {
                            foreach ($clients as $key => $client) {
                                if ($client_raw['CODIGO'] == $client['client_code']) {
                                    //Client
                                    if (strtolower($client_raw['RAZON_SOC']) != strtolower($client['client_businessname'])) {
                                        $this->log->changeClient($client['client_cuit'], "UPDATE", "client_businessname", $client_raw['RAZON_SOC'], $client['client_businessname']);
                                    }

                                    if (strtolower($client_raw['DIRECCION']) != strtolower($client['client_address'])) {
                                        $this->log->changeClient($client['client_cuit'], "UPDATE", "client_address", $client_raw['DIRECCION'], $client['client_address']);
                                    }

                                    if (strtolower($client_raw['LOCALIDAD']) != strtolower($client['client_city'])) {
                                        $this->log->changeClient($client['client_cuit'], "UPDATE", "client_city", $client_raw['LOCALIDAD'], $client['client_city']);
                                    }

                                    if (strtolower($client_raw['COD_PCIA']) != strtolower($client['client_state'])) {
                                        $this->log->changeClient($client['client_cuit'], "UPDATE", "client_state", $client_raw['COD_PCIA'], $client['client_state']);
                                    }

                                    if (strtolower($client_raw['FACT_TIPO']) != strtolower($client['client_invoicetype'])) {
                                        $this->log->changeClient($client['client_cuit'], "UPDATE", "client_invoicetype", $client_raw['FACT_TIPO'], $client['client_invoicetype']);
                                    }

                                    if (strtolower($client_raw['CODPOSTAL']) != strtolower($client['client_cp'])) {
                                        $this->log->changeClient($client['client_cuit'], "UPDATE", "client_cp", $client_raw['CODPOSTAL'], $client['client_cp']);
                                    }

                                    if (strtolower($client_raw['MAIL']) != strtolower($client['client_email'])) {
                                        $this->log->changeClient($client['client_cuit'], "UPDATE", "client_email", $client_raw['MAIL'], $client['client_email']);
                                    }
                                } else {
                                    //ClientVariant
                                    $clientVariant->set($client_raw);
                                }
                            }
                        } else {
                            //Nuevo cliente
                            //$this->log->changeClient($client_raw['CUIT'], "INSERT");
                            $client->set($client_raw);
                        }
                    }
                }
            } else {
                $this->log->changeClient("TABLA BORRADOR SIN CAMBIOS", "EMPTY");
            }
        });

        $this->get('/client_raw', function ($request, $response, $args) {
            $key = $request->getParam('log_key');
            $detail = $request->getParam('log_detail');
            //$action = $request->getParam('log_action');

            $updateCuits = $this->log->getDistinctChangeClient($key, $detail, "UPDATE"); 

            $clientDB = new \Models\PDO\Client($this->db);
            $changeClients = [];
            foreach ($updateCuits as $k => $cuit) {
              $cuitClear = preg_replace('/[^0-9]/', '', $cuit['log_detail']);

              if (!empty($cuitClear)) {
                $client = $clientDB->getByCuit($cuitClear);

                if (count($client) > 0) {
                  $changeClients[] = [
                    'client' => $client[0],
                    'changes' => $this->log->getChangeClient($key, $cuit['log_detail'], "UPDATE")
                  ];
                }
              }
            }

            $insertCuits = $this->log->getDistinctChangeClient($key, $detail, "INSERT");

            $clientRawDB = new \Models\PDO\ClientRawMigration($this->db);
            $newClients = [];

            foreach ($insertCuits as $k => $cuit) {
              //$cuitClear = preg_replace('/[^0-9]/', '', $cuit['log_detail']);

              if (!empty($cuit['log_detail'])) {
                $client = $clientRawDB->getByCuit($cuit['log_detail']);

                if (count($client) > 0) {
                  //array_push($newClients, $client);
                  $newClients[] = $client;
                }
              }
            }

          $value = [
            'head' => [],
            'path' => '../template/',
            'selected' => $request->getParams(),
            'changeClients' => $changeClients,
            'newClients' => $newClients,
            'log_key' => $this->log->getDistinctKey("CHANGE_CLIENT")
          ];
          //return $response->withJson($value);
          return $this->view->render($response, 'customer_check_raw.twig', $value);

        })->setName('client-raw');
    });

    $this->get('/importSQLClient', function ($request, $response, $args) {
        $tableClientFull = $this->db->prepare("
            SELECT
                'ADM' AS client_origin, 'b' AS b, `CODIGO`, `RAZON_SOC`,  `DIRECCION`,
                `LOCALIDAD`, `COD_PCIA`, `TITULAR`, `CUIT`, `FACT_TIPO`,
                `TELEFONO0`, `FECHA_FACT`, `PROX_FACT`, `CODPOSTAL`, `FINGRESO`,
                `NOTAS`, `AGENTE`, `MAIL WWW STATUS`, `RUBRO`, 'c' AS client_identificationtype
                FROM client_full
            WHERE CUIT IS NOT NULL OR CUIT != ''
        ");
        $tableClientFull->execute();
        $client_full = $tableClientFull->fetchAll(PDO::FETCH_ASSOC);

        $tableClient = $this->db->prepare("
            INSERT IGNORE INTO client
            (
                client_origin, client_status, client_code, client_businessname, client_address,
                client_city, client_state, client_contact, client_cuit, client_invoicetype,
                client_phone0, client_invoicedate, client_newtinvoicedate, client_cp, client_createdate,
                client_notes, client_agent, client_email, client_www, client_identificationtype
            )  VALUES (
                ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?
            );
        ");

        $total = 0;
        $totalCuit = 0;
        $totalDNI = 0;
        $totalExterior = 0;
        if (count($client_full) > 0) {
            echo "\n\r";
            foreach ($client_full as $key => &$client) {
                if (substr($client['CUIT'], 0, 1) === 'E') {
                    $client['client_identificationtype'] = 'Exterior';
                    $totalExterior++;
                } elseif (substr($client['CUIT'], 0, 1) === 'A' && strlen($client['CUIT']) < 11) {
                    $client['client_identificationtype'] = 'DNI';
                    $totalDNI++;
                } else {
                    $client['client_identificationtype'] = 'CUIT';
                    $totalCuit++;
                }

                echo "#$key " . $client['client_identificationtype'] . " = " . $client['CUIT'];
                echo "\n\r";
                if ($key == 1) {
                    break;
                }
                $tableClient->execute(array_values($client));
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se importó cliente", "INSERT", "client_id", $last_id);

                $total++;
            }
            echo "\n\r";
            echo "------------------------------------- " . "\n\r";
            echo "------------------------------------- " . "\n\r";
            echo "---- total: $total ------------------ " . "\n\r";
            echo "---- totalCuit: $totalCuit ---------- " . "\n\r";
            echo "---- totalDNI: $totalDNI ------------ " . "\n\r";
            echo "---- totalExterior: $totalExterior -- " . "\n\r";
            echo "------------------------------------- " . "\n\r";
            echo "------------------------------------- " . "\n\r";
        }
        echo "proceso terminado";
        return $response->withJson("OK");
    });
});
