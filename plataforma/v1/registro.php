<?php

$app->group('/registro', function () {

  $this->get('/listado', function ($request, $response, $args) {
        $data = $request->getParsedBody();
        $where = '';
		$join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['clientes']) && $data['clientes'] != "") {
                $cliente = $data['clientes'];
                $where .= 'AND c.client_businessname like "%' . $cliente . '%" ';
            }            
			if (isset($data['serialcontrato']) && $data['serialcontrato'] != "") {
                $serial = $data['serialcontrato'];
                $where .= 'AND s.serials_contract_number like "%' .$serial. '%" ';
            }
			if (isset($data['numeroventa']) && $data['numeroventa'] != "") {
                $marca = $data['numeroventa'];
                $where .= "AND o.opportunity_salenumber LIKE '%". $marca ."%'";
            }
			if (isset($data['estado']) && $data['estado'] != "") {
                $estado = $data['estado'];
                $where .= "AND s.serials_contract_register='". $estado ."'";
            }
			if (isset($data['fechadesde']) && $data['fechadesde'] != "") {
				if (isset($data['fechahasta']) && $data['fechahasta'] != "") {
					$fecha = explode('/',$data['fechadesde']);
					$date = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
					$fecha2 = explode('/',$data['fechahasta']);
					$date2 = $fecha2[2].'-'.$fecha2[1].'-'.$fecha2[0];
					$where .= 'AND s.serials_contract_date BETWEEN "'.$date.'" AND "'.$date2.'" ';
				}else{
					$fecha = explode('/',$data['fechadesde']);
					$date = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                	$where .= 'AND s.serials_contract_date >= "'.$date.'" ';	
				}
            }
			if (isset($data['fechahasta']) && $data['fechahasta'] != "") {
				if (isset($data['fechadesde']) && $data['fechadesde'] != "") {
					$fecha = explode('/',$data['fechadesde']);
					$date = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
					$fecha2 = explode('/',$data['fechahasta']);
					$date2 = $fecha2[2].'-'.$fecha2[1].'-'.$fecha2[0];
					$where .= 'AND s.serials_contract_date BETWEEN "'.$date.'" AND "'.$date2.'" ';
				}else{
					$fecha = explode('/',$data['fechahasta']);
					$date = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
					$where .= 'AND s.serials_contract_date <= "'.$date.'" ';	
				}
            }
			
			//////
			
			if (isset($data['serialescontratos']) && $data['serialescontratos'] != "") {
                $sc = $data['serialescontratos'];
				/*switch($sc){
					case 'cs':
						$join = 'RIGHT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
						$where .= 'AND s.serials_contract_type = "Serial" ';	
						break;
					case 'cc':
						$join = 'RIGHT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
						$where .= 'AND s.serials_contract_type = "Contrato" ';	
						break;
					case 'csc':
						$join = 'RIGHT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
						$where .= 'AND (s.serials_contract_type = "Contrato" OR s.serials_contract_type = "Serial") ';	
						break;
					case 'ss':
						$join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
						$where .= 'AND s.serials_contract_type = "Contrato" ';	
						break;
					case 'sc':
						$join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
						$where .= 'AND s.serials_contract_type = "Serial" ';	
						break;
					case 'ssc':
						$join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
						$where .= 'AND s.serials_contract_type <> "Contrato" AND s.serials_contract_type <> "Serial" ';	
						break;
				}*/
            }
			
			//////
			
			if (isset($data['usuarios']) && $data['usuarios'] != "") {
                $usuario = $data['usuarios'];
				$where .= " HAVING user_namefull like '%".$usuario."%' ";
            }
        }
		
		$sql="SELECT c.client_businessname,o.opportunity_id, o.opportunity_createdate, o.opportunity_total,
					u.user_name, u.user_lastname, od.opportunity_detail_id, od.opportunity_detail_description, 
					od.opportunity_detail_qty, od.opportunity_detail_moneda, od.opportunity_detail_price, s.serials_contract_id,
					od.opportunity_detail_createdate, s.serials_contract_type,od.opportunity_detail_father,
					s.serials_contract_number, s.serials_contract_date, s.serials_contract_status, o.opportunity_id, s.serials_contract_enddate,
					s.serials_contract_limitdate, CONCAT(u.user_name, ' ', u.user_lastname) AS user_namefull,
					o.opportunity_salenumber, br.brand_name, s.serials_contract_parent, s.serials_contract_register,od.opportunity_detail_productid
			FROM opportunity o
					INNER JOIN client c ON c.client_id = o.client_id
					INNER JOIN product_brand br ON o.brand_id = br.brand_id
					INNER JOIN users u ON u.user_id = o.user_id
					INNER JOIN opportunity_detail od ON od.oportunity_id = o.opportunity_id
					" . $join . "
			WHERE o.opportunity_status='V' AND o.brand_id=1 AND s.serials_contract_number!='' " . $where . "
			      AND (SELECT COUNT(zz.opportunity_detail_id) FROM serials_contract zz WHERE zz.opportunity_detail_id IN (SELECT yy.opportunity_detail_id FROM opportunity_detail yy WHERE yy.oportunity_id=o.opportunity_id) AND  zz.serials_contract_status=1)=(SELECT COUNT(zz.opportunity_detail_id) FROM serials_contract zz WHERE zz.opportunity_detail_id IN (SELECT yy.opportunity_detail_id FROM opportunity_detail yy WHERE yy.oportunity_id=o.opportunity_id))
			ORDER BY 
				o.opportunity_id DESC,
				CAST(
					CONCAT(	(CASE WHEN (s.serials_contract_parent='0') THEN s.serials_contract_id ELSE s.serials_contract_parent END), 
							s.serials_contract_id
					) 
				AS UNSIGNED ) 
			ASC";
		$query = $this->db->prepare($sql);
        $query->execute();
        $sales = $query->fetchAll(PDO::FETCH_ASSOC);
		
		//Saco listado de marcas
        $query = $this->db->prepare("SELECT brand_id, brand_name
									 FROM product_brand");
        $query->execute();
        $brands = $query->fetchAll(PDO::FETCH_ASSOC);
		
		/////////////
		
		$order_sales = array();
        $j = 1;
        $d = 0;
        foreach ($sales as $s => $v) {
            $id_o = $v['opportunity_id'];
            $id_d = $v['opportunity_detail_id'];
            $id_s = $v['serials_contract_id'];

            $order_sales[$id_o]['opportunity_id'] = $v['opportunity_id'];
            $order_sales[$id_o]['opportunity_client'] = $v['client_businessname'];
            $order_sales[$id_o]['opportunity_user'] = $v['user_name'] . ' ' . $v['user_lastname'];
            $order_sales[$id_o]['opportunity_date'] = substr($v['opportunity_createdate'], 0, 10);
            $order_sales[$id_o]['opportunity_total'] = $v['opportunity_total'];
			$order_sales[$id_o]['brand_name'] = $v['brand_name'];
			$order_sales[$id_o]['opportunity_salenumber'] = $v['opportunity_salenumber'];
           
		    $order_sales[$id_o]['opportunity_details'][$id_d] = array(
                'id_detail' => $v['opportunity_detail_id'],
                'currency' => $v['opportunity_detail_moneda'],
                'product' => $v['opportunity_detail_description'],
                'qty' => $v['opportunity_detail_qty'],
                'price' => $v['opportunity_detail_price'],
				'odf' => $v['opportunity_detail_father'],
				'sku' => $v['opportunity_detail_productid'],
                'date' => substr($v['opportunity_detail_createdate'], 0, 10)
            );
			
            if ($v['serials_contract_number'] != '') {
                if($v['serials_contract_date']!=""){
					$date = explode("-",$v['serials_contract_date']);
					$v['serials_contract_date'] = $date[2]."/".$date[1]."/".$date[0];
				}	
				
				if($v['serials_contract_enddate']!=""){
					$date = explode("-",$v['serials_contract_enddate']);
					$v['serials_contract_enddate'] = $date[2]."/".$date[1]."/".$date[0];
				}
				
				if($v['serials_contract_limitdate']!=""){
					$date = explode("-",$v['serials_contract_limitdate']);
					$v['serials_contract_limitdate'] = $date[2]."/".$date[1]."/".$date[0];
				}
				
				$order_sales[$id_o]['details_serials'][$id_d][$id_s] = array(
                    'sc_id' => $v['serials_contract_id'],
					'type' => $v['serials_contract_type'],
                    'number' => $v['serials_contract_number'],
                    'date' => $v['serials_contract_date'],
					'enddate' => $v['serials_contract_enddate'],
					'limitdate' => $v['serials_contract_limitdate'],
                    'status' => $v['serials_contract_status'],
					'odf' => $v['opportunity_detail_father'],
					'scfid' => $v['serials_contract_parent'],
					'register' => $v['serials_contract_register'],
                );
                $j++;
            }
        }

        $this->log->info("Se ingresó al listado de registro", "SELECT", "", 0);

        return $this->view->render($response, 'register_list.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'body' => array(),
					'sales' => $order_sales,
					'brands' => $brands,
					'params' => $data
        ]);
    })->setName('register-list');
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$this->post('/crearRegistro', function ($request, $response, $args) {
        $data = $request->getParsedBody();
		
		$fecha = explode("/",$data["fecha"]);
		$data["fecha"] = $fecha[2]."-".$fecha[1]."-".$fecha[0];
		
		if($data["endfecha"]!=""){
			$endfecha = explode("/",$data["endfecha"]);
			$data["endfecha"] = $endfecha[2]."-".$endfecha[1]."-".$endfecha[0];
		}
		
        $query = $this->db->prepare("UPDATE serials_contract "
                . "SET serials_contract_register=:status, serials_contract_date=:fecha, serials_contract_enddate=:endfecha "
                . "WHERE serials_contract_id=:id ");
        
		$query->bindParam(':id', $data["serial_id"]);
        $query->bindParam(':status', $data["estado"]);
		$query->bindParam(':fecha', $data["fecha"]);
		$query->bindParam(':endfecha', $data["endfecha"]);
        $query->execute();

        $this->log->info("Se creó el serial de contrato", "UPDATE", "serials_contract_id", $data["serial_id"]);

        return $response->withJson("OK");
    });
});