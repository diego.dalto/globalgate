<?php

$app->get('/index[/{error}]', function ($request, $response, $args) {
    $msg = '';
    
    if(array_key_exists('error', $args)) {
        $msg = ($args['error'] == 'empty') ? 
                'Completá los campos' : 'El mail o la contraseña no coinciden';
    }
    
    return $this->view->render($response, 'login.twig', [
                'head' => array(),
                'path' => 'template/',
                'action' => 'Crear',
                'body' => array(),
                'msg' => $msg
    ]);
    
})->setName('index');

$app->get('/logout', function($request, $response, $args) {

    foreach ($_SESSION as $key => $value) {
        $this->session->delete($key);
    }
    $this->session::destroy();

    return $response->withRedirect($this->router->pathFor('index'), 301);
    
})->setName('logout');

$app->get('/login', function($request, $response, $args) {
    $params = $request->getQueryParams();
    $redirect = $this->router->pathFor('index', ['error' => 'empty']);
    
    $session = $this->session;
    
    $mail = $params['mail'];
    $pass = $params['pass'];
    
    if(!empty($mail) || !empty($pass))
    {
        $table = $this->db->table("users");
        $user = $table->select(["user_email" => $mail, "user_password" => $pass]);

        if (count($user) > 0) {
            $user = $user[0];
            foreach ($user as $key => $val) {
                if (isset($val)) {
                    $session->set($key, $val);
                }
            }

            $key = "GLOBALGATE_COMERCIAL" . strtotime("now");

            $table_session = $this->db->table("user_session");

            $table_session->insert(array([
                "session_id" => $session::id(),
                "name" => $user["user_name"],
                "email" => $mail,
                "user_id" => $user["user_id"],
                "key_id" => $key
            ]));
            
            $redirect = $this->router->pathFor('dashboard');
        } else {
            $redirect = $this->router->pathFor('index', ['error' => 'unmatch']);
        }
    }
    
    return $response->withRedirect($redirect, 307);
});
