<?php 

$app->group('/marcas', function () {

    $this->get('', function ($request, $response, $args) {
        $where = "";
        $params = [ ];
        $brand_type = $request->getParam('brand_type');
        $brand_group = $request->getParam('brand_group');

        /////////
        if (isset($brand_type) && !empty($brand_type)) {
            $params['selected']['brand_type'] = $brand_type;
            $where .= "AND brand_type='$brand_type' ";
        }
        /////////
        //
        /////////
        if (isset($brand_group) && !empty($brand_group)) {
            $params['selected']['brand_group'] = $brand_group;
            $where .= "AND pgd.group_brand_id='$brand_group' ";
        }
        /////////

        /**
         * Lista de marcas
         */
        $tableBrand = $this->db->prepare("
            SELECT pb.brand_id, brand_name, brand_type, gb.group_brand_id, gb.group_brand_name 
                FROM product_brand AS pb
                LEFT OUTER JOIN product_brand_group pgd ON pgd.brand_id = pb.brand_id 
                LEFT OUTER JOIN group_brand gb ON gb.group_brand_id = pgd.group_brand_id 
            WHERE 1=1 $where ORDER BY brand_id ASC;
        ");
        $tableBrand->execute();
        $params['brands'] = $tableBrand->fetchAll(PDO::FETCH_ASSOC);

        /**
         * Lista de tipos de marca
         */
        $tableBrandType = $this->db->prepare("
                SELECT brand_type 
                    FROM product_brand 
                WHERE 1=1 GROUP BY brand_type ASC;
        ");
        $tableBrandType->execute();
        $params['brand_types'] = $tableBrandType->fetchAll(PDO::FETCH_ASSOC);

        /**
         * Lista de grupo de marcas
         */
        $tableBrandGroup = $this->db->prepare("
                SELECT group_brand_id, group_brand_name  
                    FROM group_brand;
        ");
        $tableBrandGroup->execute();
        $params['brand_groups'] = $tableBrandGroup->fetchAll(PDO::FETCH_ASSOC);

        /**
         * Lista de grupo de usuarios
         */
        $tableUsers = $this->db->prepare("
                SELECT CONCAT(user_name, ' ', user_lastname) AS user_fullname,
                    user_id
                FROM users ORDER BY user_fullname ASC;
        ");
        $tableUsers->execute();
        $params['users'] = $tableUsers->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se ingresó al listado de marca", "SELECT", "", 0);
        
        return $this->view->render($response, 'brand.twig', $params);
    })->setName('brand-list');

    $this->get('/users/{id}', function ($request, $response, $args) {
        $id = $args["id"];

        $params = [];
        
        $tableUsers = $this->db->prepare("
                SELECT user_id
                    FROM brand_user
                WHERE brand_id = :brand_id;
        ");
        $tableUsers->bindParam(':brand_id', $id);
        $tableUsers->execute();
        $params['users'] = $tableUsers->fetchAll(PDO::FETCH_ASSOC);

        return $response->withJSON($params);
    })->setName('brand-users');

    $this->delete('/delete', function ($request, $response, $args) {
        $brand_id = $request->getParsedBodyParam('brand_id');
        $params = [
            'type' => -1,
            'message' => "Falta ingresar el id"
        ];

        if (isset($brand_id) && !empty($brand_id)) {
            $tableBrand = $this->db->prepare("
                DELETE
                    FROM product_brand
                WHERE brand_id = :id;
            ");
            $tableBrand->bindValue(':id', $brand_id);
            $tableBrand->execute();
            $tableBrandGroup = $this->db->prepare("
                DELETE
                    FROM product_brand_group
                WHERE brand_id = :id;
            ");
            $tableBrandGroup->bindValue(':id', $brand_id);
            $tableBrandGroup->execute();
            $tableBrandUser = $this->db->prepare("
                DELETE
                    FROM brand_user
                WHERE brand_id = :id;
            ");
            $tableBrandUser->bindValue(':id', $brand_id);
            $tableBrandUser->execute();
            
            $params = [
                'type' => 2,
                'message' => "Se eliminó la marca"
            ];
            
            $this->log->info("Se borró al listado de marca", "DELETE", "brand_id", $brand_id);
        }

        return $response->withJson($params);
    })->setName('brand-delete');

    $this->post('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $existBrand = false;
        $brand_type = '';
        $params = [
            'type' => -1,
            'message' => "No se pudo guardar"
        ];

        if (isset($post)) {
            if (isset($post['brand_name']) && !empty($post['brand_name'])) {
                $getTableBrand = $this->db->prepare("
                    SELECT brand_name 
                        FROM product_brand 
                    WHERE brand_name = :name;
                ");
                $getTableBrand->bindValue(':name', $post['brand_name']);
                $getTableBrand->execute();

                $brandName = $getTableBrand->fetchAll(PDO::FETCH_ASSOC);

                if (count($brandName) > 0) {
                    $existBrand = true;
                    $params = [
                        'type' => 1,
                        'message' => "La marca ya existe"
                    ];
                }
            }

            if (isset($post['brand_type']) && !empty($post['brand_type'])) {
                if ($post['brand_type'] == 'otra') {
                    if ((isset($post['brand_type_name']) && !empty($post['brand_type_name']))) {
                        $brand_type = $post['brand_type_name'];
                    }
                } else {
                    $brand_type = $post['brand_type'];
                }
            }

            if (!$existBrand) {
                $tableBrand = $this->db->prepare("
                    INSERT INTO product_brand 
                    (brand_name, brand_type) VALUES (:name, :type);
                ");
                $tableBrand->bindValue(':name', $post['brand_name']);
                $tableBrand->bindValue(':type', $brand_type);
                $tableBrand->execute();

                $params = [
                    'type' => 2,
                    'message' => "Guardado"
                ];
                $brand_id = $this->db->lastInsertId();
                
                $this->log->info("Se creó la marca ".$post['brand_name'], "INSERT", "brand_id", $brand_id);

                if (isset($post['brand_group']) && !empty($post['brand_group'])) {
                    $tableProductBrandGroup = $this->db->prepare("
                        INSERT 
                            INTO product_brand_group 
                        (brand_id, group_brand_id) VALUES (:brand_id, :group_brand_id);
                    ");
                    $tableProductBrandGroup->bindValue(':brand_id', $brand_id);
                    $tableProductBrandGroup->bindValue(':group_brand_id', $post['brand_group']);
                    $tableProductBrandGroup->execute();
                    
                    $this->log->info("Se creó la asociación grupo - marca ".$post['brand_name'], "INSERT", "group_brand_id", $post['brand_group']);
                }

                if (isset($post['brand_users']) && !empty($post['brand_users'])) {
                    $tableBrandUser = $this->db->prepare("
                        INSERT 
                            INTO brand_user 
                        (brand_id, user_id) VALUES (:brand_id, :user_id);
                    ");
                    $tableBrandUser->bindValue(':brand_id', $brand_id);
                    
                    foreach ($post['brand_users'] as $user_id) {
                        $tableBrandUser->bindValue(':user_id', $user_id);
                        $tableBrandUser->execute();
                        
                        $this->log->info("Se creó la asociación usuario - marca ".$post['brand_name'], "INSERT", "user_id", $user_id);
                    }
                }
            }
        }

        return $response->withJson($params);
    })->setName('brand-create');

    $this->put('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $brand_type = '';
        $brand_id = $post['brand_id'];
        $params = [
            'type' => -1,
            'message' => "No se pudo actualizar"
        ];

        if (isset($post) && (isset($brand_id) && !empty($brand_id))) {
            if (isset($post['brand_name']) && !empty($post['brand_name'])) {
                if (isset($post['brand_type']) && !empty($post['brand_type'])) {
                    if ($post['brand_type'] == 'otra') {
                        if ((isset($post['brand_type_name']) && !empty($post['brand_type_name']))) {
                            $brand_type = $post['brand_type_name'];
                        }
                    } else {
                        $brand_type = $post['brand_type'];
                    }
                    $tableBrand = $this->db->prepare("
                            UPDATE product_brand 
                                SET brand_name = :name, brand_type = :type
                            WHERE brand_id = :id;
                        ");
                    $tableBrand->bindValue(':name', $post['brand_name']);
                    $tableBrand->bindValue(':type', $brand_type);
                    $tableBrand->bindValue(':id', $brand_id);
                    $tableBrand->execute();
                    
                    $this->log->info("Se modificó la marca ".$post['brand_name'], "INSERT", "brand_id", $brand_id);

                    $params = [
                        'type' => 2,
                        'message' => "Actualizado"
                    ];
                }
                $tableProductBrandGroup = $this->db->prepare("
                    DELETE 
                        FROM product_brand_group 
                    WHERE brand_id = :id;
                ");
                $tableProductBrandGroup->bindValue(':id', $brand_id);
                $tableProductBrandGroup->execute();
                
                $this->log->info("Se borró la asociación grupo - marca ".$post['brand_name'], "DELETE", "brand_id", $brand_id);
                
                $tableBrandUser = $this->db->prepare("
                    DELETE 
                        FROM brand_user 
                    WHERE brand_id = :brand_id;
                ");
                $tableBrandUser->bindValue(':brand_id', $brand_id);
                $tableBrandUser->execute();
                
                $this->log->info("Se borró la asociación usuario - marca ".$post['brand_name'], "DELETE", "brand_id", $brand_id);
            }

            if (isset($post['brand_group']) && !empty($post['brand_group']) && !empty($brand_id)) {

                ///////

                $tableProductBrandGroup = $this->db->prepare("
                    INSERT 
                        INTO product_brand_group 
                    (brand_id, group_brand_id) VALUES (:brand_id, :group_brand_id);
                ");
                $tableProductBrandGroup->bindValue(':brand_id', $brand_id);
                $tableProductBrandGroup->bindValue(':group_brand_id', $post['brand_group']);
                $tableProductBrandGroup->execute();
                
                $this->log->info("Se creó la asociación grupo - marca ".$post['brand_name'], "INSERT", "brand_id", $brand_id);
            }

            if (isset($post['brand_users']) && !empty($post['brand_users'])) {
                ///////
                
                $tableBrandUser = $this->db->prepare("
                        INSERT 
                            INTO brand_user 
                        (brand_id, user_id) VALUES (:brand_id, :user_id);
                    ");
                $tableBrandUser->bindValue(':brand_id', $brand_id);

                foreach ($post['brand_users'] as $user_id) {
                    $tableBrandUser->bindValue(':user_id', $user_id);
                    $tableBrandUser->execute();
                    
                    $this->log->info("Se creó la asociación usuario - marca ".$post['brand_name'], "INSERT", "user_id", $user_id);
                }
            }
        }

        return $response->withJson($params);
    })->setName('brand-update');
});
