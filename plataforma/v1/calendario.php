<?php

$app->group('/calendario', function () {

    $this->get('/', function ($request, $response, $args) {

        $calendar = [];
        $me = $this->session->user_id;

        $calendar = new \Models\PDO\Calendar($this->db);
        $calendarJSON = $calendar->get($me);
        $calendarParse = $calendar->parseJsonCalendar($calendarJSON);
        
        $users = $this->db->table("users");
        
        //Obtengo usuarios
        $tableUsers = $this->db->prepare("
            SELECT user_id, user_name, user_lastname
                FROM users 
            WHERE user_status=1 AND user_id != :user_id
                ORDER BY user_name ASC
        ");
        $tableUsers->bindValue(':user_id', $me);
        $tableUsers->execute();
        $users_select = $tableUsers->fetchAll(PDO::FETCH_ASSOC);

        //Obtengo cliente
        $client = $this->db->table("client");

        //Obtengo Contacto
        $tableContactClient = $this->db->prepare("
            SELECT contact_id, contact_name, contact_lastname
                FROM contact_client 
            WHERE contact_status=1 
                ORDER BY contact_name ASC
        ");
        $tableContactClient->execute();
        $contacts = $tableContactClient->fetchAll(PDO::FETCH_ASSOC);


        $this->log->info("Se ingresó al calendario", "SELECT", "user_id", $me);

        return $this->view->render($response, 'calendar.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'action' => 'Listado',
                    'me' => $me,
                    'calendar' => $calendarParse,
                    'users' => $users->select(['user_status' => 1]),
                    'users_select' => $users_select,
                    'contacts' => $contacts,
                    'customers' => $client->select([1 => 1])
        ]);
    })->setName('calendario');

    $this->get('/{id:[0-9]+}', function ($request, $response, $args) {
        $me = $args['id'];

        //Obtengo calendario
        $calendar = new \Models\PDO\Calendar($this->db);
        $calendarJSON = $calendar->get($me);

        $this->log->info("Se ingresó al calendario", "SELECT", "user_id", $me);

        return $response->withJson($calendarJSON);
    })->setName('calendar-json');
    
    $this->get('/parent/{id}', function ($request, $response, $args) {
        $milestone_id = $args['id'];

        $event = [];
        
        //Obtengo milestone asociados
        $milestone = $this->db->prepare("
                SELECT user_id
                    FROM milestone 
                WHERE milestone_parent = :milestone_parent;
        ");
        $milestone->bindParam(':milestone_parent', $milestone_id);
        $milestone->execute();
        $event["milestones"] = $milestone->fetchAll(PDO::FETCH_ASSOC);
        
        return $response->withJson($event);
    })->setName('milestone-parent-user');

    $this->get('/contact/{id}', function ($request, $response, $args) {
        $milestone_id = $args['id'];

        $event = [];
        
        $milestoneContact = $this->db->table("milestone_contact");
        
        $event["milestone_contacts"] = $milestoneContact->select(['milestone_id' => $milestone_id]);;
        
        return $response->withJson($event);
    })->setName('milestone-contact-user');

    
    $this->get('/listado', function ($request, $response, $args) {
        
    });

    $this->put('/editar', function ($request, $response, $args) {

        $milestone = $request->getParsedBody();

        if (empty($milestone['user_id'])) {
            $me = $this->session->user_id;
        } else {
            $me = $milestone['user_id'];
        }

        $date = DateTime::createFromFormat('d/m/Y', $milestone['date']);

        $today = new DateTime('now');
        $interval = $today->diff($date);

        if (in_array($date->format('N'), [6, 7])) {
            return $response->withJson(["error" => 'La fecha no puede ser un fin de semana', "status" => 202], 202);
        }

        if ($interval->format('%R%a') < 0) {
            return $response->withJson(["error" => 'La fecha no puede ser menor al día de hoy', "status" => 202], 202);
        }

        if (empty($milestone['title']) || empty($milestone['client_id'])) {
            return $response->withJson(["error" => 'Completar campos', "status" => 202], 202);
        }

        $table = $this->db->table("milestone");
        $event = $table->select(['milestone_id' => $milestone['milestone_id']]);
        
        if(count($event) > 0 && !empty($event[0]['milestone_parent'])) {
            return $response->withJson(["error" => 'No tenés permisos para editar este evento', "status" => 202], 202);
        }
        
        $val = [
            "milestone_title" => $milestone['title'],
            "milestone_detail" => $milestone['detail'],
            "milestone_date" => $date->format('Y/m/d'),
            "milestone_start_time" => $milestone['start_time'],
            "milestone_end_time" => $milestone['end_time'],
            "milestone_type" => 'calendar',
            //"milestone_notification" => $milestone['notification'],
            "milestone_status" => 0,
            //"milestone_link" => $milestone['link'],
            //"opportunity_id" => $milestone['opportunity_id'],
            "user_id" => $me,
            "client_id" => $milestone['client_id']
        ];

        $table->update($val, ['milestone_id' => $milestone['milestone_id']]);
        $table->delete(['milestone_parent' => $milestone['milestone_id']]);
                
        $val_clone = $val;
        $val['status'] = 201;
        $val['id'] = $milestone['milestone_id'];
        
        if(isset($milestone['add_user']) && !empty($milestone['add_user'])) {
            
            $val_clones = [];
                        
            foreach ($milestone['add_user'] as $key => $user_id) {
                
                $val_clone['user_id'] = $user_id;
                $val_clone['milestone_parent'] = $val['id'];
                $val_clones[$key] = $val_clone;
                //$this->log->info("Se agrego el usuario al calendario ".$val['id'], "INSERT", "user_id", $user_id);
            }
            
            //return $response->withJson(["clones" => $val_clones, "status" => "202"], 202);
            $table->insert($val_clones);
        }
        
        $milestoneContact = $this->db->table("milestone_contact");
        $milestoneContact->delete(['milestone_id' => $val['id']]);
        
        if(isset($milestone['add_contact']) && !empty($milestone['add_contact'])) {

            $contact = []; $contacts = [];
            
            foreach ($milestone['add_contact'] as $key => $contact_id) {                
                $contact['contact_id'] = $contact_id;
                $contact['milestone_id'] = $val['id'];
                $contacts[$key] = $contact;
            }
            $milestoneContact->insert($contacts);
        }

        //$this->log->info("Se modificó el calendario", "UPDATE", "milestone_id", $val['id']);

        return $response->withJson($val, 201);
    })->setName('calendar-edit');

    $this->post('/crear', function ($request, $response, $args) {

        $milestone = $request->getParsedBody();

        if (empty($milestone['user_id'])) {
            $me = $this->session->user_id;
        } else {
            $me = $milestone['user_id'];
        }

        $date = DateTime::createFromFormat('d/m/Y', $milestone['date']);

        $today = new DateTime('now');
        $interval = $today->diff($date);

        if (in_array($date->format('N'), [6, 7])) {
            return $response->withJson(["error" => 'La fecha no puede ser un fin de semana', "status" => 202], 202);
        }

        if ($interval->format('%R%a') < 0) {
            return $response->withJson(["error" => 'La fecha no puede ser menor al día de hoy', "status" => 202], 202);
        }

        if (empty($milestone['title']) || empty($milestone['client_id'])) {
            return $response->withJson(["error" => 'Completar campos', "status" => 202], 202);
        }

        $val = [
            "milestone_title" => $milestone['title'],
            "milestone_detail" => $milestone['detail'],
            "milestone_date" => $date->format('Y/m/d'),
            "milestone_start_time" => $milestone['start_time'] . ":00",
            "milestone_end_time" => $milestone['end_time'] . ":00",
            "milestone_type" => 'calendar',
            //"milestone_notification" => $milestone['notification'],
            "milestone_status" => 0,
            //"milestone_link" => $milestone['link'],
            //"opportunity_id" => $milestone['opportunity_id'],
            "user_id" => $me,
            "client_id" => $milestone['client_id']
        ];

        $table = $this->db->table("milestone");
        $table->insert(array($val));
        
        $val_clone = $val;
        $val['status'] = 201;
        $val['id'] = $this->db->lastInsertId();

        //$this->log->info("Se creó el calendario", "INSERT", "milestone_id", $val['id']);

        if(isset($milestone['add_user']) && !empty($milestone['add_user'])) {

            $val_clones = [];
            
            foreach ($milestone['add_user'] as $key => $user_id) {
                
                $val_clone['user_id'] = $user_id;
                $val_clone['milestone_parent'] = $val['id'];
                $val_clones[$key] = $val_clone;
                //$this->log->info("Se agrego el usuario al calendario ".$val['id'], "INSERT", "user_id", $user_id);
            }
            $table->insert($val_clones);
        }
        
        if(isset($milestone['add_contact']) && !empty($milestone['add_contact'])) {

            $contact = []; $contacts = [];
            
            foreach ($milestone['add_contact'] as $key => $contact_id) {                
                $contact['contact_id'] = $contact_id;
                $contact['milestone_id'] = $val['id'];
                $contacts[$key] = $contact;
            }
            $milestoneContact = $this->db->table("milestone_contact");
            $milestoneContact->insert($contacts);
        }

        return $response->withJson($val, 201);
    })->setName('new-calendar');
});
