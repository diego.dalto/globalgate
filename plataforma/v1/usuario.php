<?php

$app->group('/usuario', function () {

    $this->get('/editar/{id}', function ($request, $response, $args) {        
        $id = $request->getAttribute('id');
        
        if($id == "me") {
            $id = $this->session->user_id;
        }
        
        $query = $this->db->prepare("SELECT u.user_id, user_dniciut, user_name, user_statuscompany,
            user_lastname, user_photo, user_email, user_phone, area_name, u.area_id, user_status, group_name, area_name, 
			ug.group_id, user_personalemail, user_address, user_birthday, user_datein, user_movilephone, user_cp
            FROM users u
            LEFT JOIN company_area c ON c.area_id = u.area_id
            LEFT JOIN user_group ug ON ug.user_id = u.user_id
            LEFT JOIN groups g ON g.group_id = ug.group_id
            WHERE u.user_id=" . $id);
        $query->execute();
        $users = $query->fetchAll(PDO::FETCH_ASSOC);

        //Saco listado de marcas
        $query = $this->db->prepare("SELECT brand_id, brand_name, brand_type
                                            FROM product_brand");
        $query->execute();
        $brands = $query->fetchAll(PDO::FETCH_ASSOC);

        //Saco listado de grupos
        $query = $this->db->prepare("SELECT group_id, group_name
                                            FROM groups");
        $query->execute();
        $groups = $query->fetchAll(PDO::FETCH_ASSOC);

        //Saco listado de Areas
        $query = $this->db->prepare("SELECT area_id, area_name
                                            FROM company_area");
        $query->execute();
        $areas = $query->fetchAll(PDO::FETCH_ASSOC);

        //Saco listado de marcas por usuario
        $query = $this->db->prepare("SELECT brand_id
                                            FROM brand_user WHERE user_id=" . $id);
        $query->execute();
        $brands_user = $query->fetchAll(PDO::FETCH_ASSOC);
        $b_u = array();

        foreach ($brands_user as $k => $v) {
            $b_u[] = $v['brand_id'];
        }
        
        $this->log->info("Se ingresó a editar usuario", "SELECT", "user_id", $id);

        return $this->view->render($response, 'user_edit_create.twig', [
                    'head' => array(),
                    'path' => '../../template/',
                    'action' => 'Editar',
                    'user' => $users[0],
                    'brands' => $brands,
                    'groups' => $groups,
                    'areas' => $areas,
                    'brands_user' => $b_u
                        //'body' => array()
        ]);
    })->setName('edit-user');

    $this->get('/listado', function ($request, $response, $args) {
        $query = $this->db->prepare("DELETE FROM users WHERE  user_status = '-1';");
        $query->execute();

        $data = $request->getParsedBody();
        $where = '';

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['search']) && $data['search'] != "") {
                $texto = $data['search'];
                $where .= 'AND user_name like "%' . $texto . '%" OR user_lastname like "%' . $texto . '%" OR user_dniciut like "%' . $texto . '%" OR user_email like "%' . $texto . '%" ';
            }
            if (isset($data['grupo']) && $data['grupo'] != "") {
                $group = $data['grupo'];
                $where .= 'AND ug.group_id="' . $group . '" ';
            }
        }
        //obtengo lista de usuarios
        $query = $this->db->prepare("SELECT u.user_id, user_dniciut, user_name, 
            user_lastname, user_email, user_phone, area_name, user_status, group_name
            FROM users u
            LEFT JOIN company_area c ON c.area_id = u.area_id
            LEFT JOIN user_group ug ON ug.user_id = u.user_id
            LEFT JOIN groups g ON g.group_id = ug.group_id
            WHERE u.user_status >= 0 " . $where . " ORDER BY user_name ASC");
        $query->execute();
        $users = $query->fetchAll(PDO::FETCH_ASSOC);

        //obtengo lista de grupos
        $query = $this->db->prepare("SELECT group_id, group_name
                                        FROM groups u
                                        WHERE 1=1 ORDER BY group_name ASC");
        $query->execute();
        $groups = $query->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se ingresó al listado de usuarios", "SELECT", "", 0);

        return $this->view->render($response, 'user_list.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'body' => array(),
                    'users' => $users,
                    'groups' => $groups
        ]);
    })->setName('user-list');

    $this->put('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        $id = $args["id"];

        $milestone = $request->getParsedBody();

        $add_query = '';
        if ($milestone["user_password"] != "") {
            $add_query = ', user_password=:uuser_password';
        }

        if ($milestone["user_birthday"] != "") {
            $add_query .= ', user_birthday=:uuser_birthday';
        }
        
        if ($milestone["user_datein"] != "") {
            $add_query .= ', user_datein=:uuser_datein';
        }
        
        $query = $this->db->prepare("UPDATE users "
                . "SET user_name=:uuser_name, user_lastname=:uuser_lastname, "
                . "user_statuscompany=:uuser_statuscompany, "
                . "user_dniciut=:uuser_dniciut, user_email=:uuser_email, "
                . "area_id=:uuser_areaid, user_status=:uuser_status, "
                . "user_personalemail=:uuser_personalemail, "
                . "user_phone=:uuser_phone, user_address=:uuser_address, "
                . "user_movilephone=:uuser_movilephone, "
                . "user_cp=:uuser_cp, user_photo=:uuser_photo " . $add_query
                . " WHERE user_id=:id ");

        $query->bindParam(':uuser_name', $milestone["user_name"]);
        $query->bindParam(':uuser_lastname', $milestone["user_lastname"]);
        $query->bindParam(':uuser_statuscompany', $milestone["cargo"]);
        $query->bindParam(':uuser_dniciut', $milestone["user_dniciut"]);
        $query->bindParam(':uuser_email', $milestone["user_email"]);

        $query->bindParam(':uuser_areaid', $milestone["area"]);
        $query->bindParam(':uuser_status', $milestone["user_status"]);
        $query->bindParam(':uuser_personalemail', $milestone["user_personalemail"]);
        $query->bindParam(':uuser_phone', $milestone["user_phone"]);
        $query->bindParam(':uuser_address', $milestone["user_address"]);

        $query->bindParam(':uuser_movilephone', $milestone["user_movilephone"]);
        $query->bindParam(':uuser_cp', $milestone["user_cp"]);
        $query->bindParam(':uuser_photo', $milestone["name_photo"]);

        if ($milestone["user_password"] != "") {
            $query->bindParam(':uuser_password', $milestone["user_password"]);
        }
        if ($milestone["user_birthday"] != "") {
            $date = DateTime::createFromFormat('d/m/Y', $milestone['user_birthday']);            
            $query->bindValue(':uuser_birthday', $date->format('Y/m/d'));
        }
        if ($milestone["user_datein"] != "") {
            $date = DateTime::createFromFormat('d/m/Y', $milestone['user_datein']);
            $query->bindParam(':uuser_datein', $milestone["user_datein"]);
        }
        $query->bindParam(':id', $id);
        $query->execute();

        //actualizo el grupo
        $query2 = $this->db->prepare("UPDATE user_group 
            SET group_id = :ugroup_id
            WHERE user_id = :id ");
        $query2->bindParam(':ugroup_id', $milestone["grupo"]);
        $query2->bindParam(':id', $id);
        $query2->execute();

        $this->log->info("Se modificó el usuario", "UPDATE", "user_id", $id);

        return $response->withJson("OK");
    });

    $this->post('/photo', function ($request, $response, $args) {
        $files = $request->getUploadedFiles();        
        
        if (empty($files['file-0'])) {
            throw new Exception('Expected a newfile');
        }

        $newfile = $files['file-0'];
        
        if ($newfile->getError() === UPLOAD_ERR_OK) 
        {
            $uploadFileName = $newfile->getClientFilename();
            $ext = explode('.', basename($uploadFileName));
            $name_file = md5(uniqid()) . "." . array_pop($ext);
            $newfile->moveTo("/var/www/html/plataforma/template/img/profile-pics/$name_file");
            
            $this->log->info("Se cargo una foto de usuario", "UPDATE", "", 0);
            return $response->withJson(["name" => $name_file]);
        }
    });

    $this->post('/crear/{id:[0-9]+}', function ($request, $response, $args) {
        
    });

    $this->post('/crearUsuario', function ($request, $response, $args) {
        $query = $this->db->prepare("INSERT INTO users (user_id, user_dniciut, "
                . "user_name, user_lastname, user_email, user_personalemail, "
                . "user_phone, user_movilephone, user_datein, user_address, "
                . "user_cp, user_password, area_id, position_id, user_birthday, "
                . "user_statuscompany, user_status) "
                . "VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "
                . "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-1')");
        $query->execute();
        $user = $this->db->lastInsertId();

        //creo el grupo
        $query2 = $this->db->prepare("INSERT INTO user_group 
            (group_id, user_id) VALUES (:ugroup_id, :id) ");
        $query2->bindValue(':ugroup_id', 1);
        $query2->bindParam(':id', $user);
        $query2->execute();
        
        $this->log->info("Se creó el usuario", "INSERT", "user_id", $user);
        
        return $response->withJson($user);
    });

    $this->post('/agregarMarcas', function ($request, $response, $args) {
        $data = $request->getParsedBody();

        $query = $this->db->prepare("INSERT INTO brand_user (user_id, brand_id) VALUES (:user,:brand)");
        $query->bindParam(':user', $data["user"]);
        $query->bindParam(':brand', $data["brand"]);
        $query->execute();

        $this->log->info("Se creó asociación marca - usuario", "INSERT", "user_id", $data["user"]);
        
        return $response->withJson("OK");
    });

    $this->delete('/eliminarMarcas', function ($request, $response, $args) {
        $data = $request->getParsedBody();

        $query = $this->db->prepare("DELETE FROM brand_user WHERE user_id=:uid AND brand_id=:bid ");
        $query->bindParam(':uid', $data["user"]);
        $query->bindParam(':bid', $data["brand"]);
        $query->execute();

        $this->log->info("Se eliminó asociación marca - usuario", "DELETE", "user_id", $data["user"]);
        
        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/eliminarUsuario/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("UPDATE users SET user_status='2' WHERE user_id=:uid ");
        $query->bindParam(':uid', $milestone["user_id"]);
        $query->execute();

        $this->log->info("Se eliminó usuario", "DELETE", "user_id", $milestone["user_id"]);

        return $response->withJson("OK");
    });
});
