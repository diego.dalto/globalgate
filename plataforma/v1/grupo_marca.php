<?php 

$app->group('/grupos/marca', function () {

    $this->get('', function ($request, $response, $args) {
        $params = [];

        /**
         * Lista de marcas
         */
        $tableBrand = $this->db->prepare("
            SELECT brand_id, brand_name
                FROM product_brand;
        ");
        $tableBrand->execute();
        $params['brands'] = $tableBrand->fetchAll(PDO::FETCH_ASSOC);
        
        /**
         * Lista de grupo de marcas
         */
        $tableBrandGroup = $this->db->prepare("
                SELECT group_brand_id, group_brand_name  
                    FROM group_brand;
        ");
        $tableBrandGroup->execute();
        $params['brand_groups'] = $tableBrandGroup->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se ingresó al listado de grupos de marca", "SELECT", "", 0);

        return $this->view->render($response, 'group_brand.twig', $params);
        
    })->setName('group-brand-list');

    $this->get('/brands/{id}', function ($request, $response, $args) {
        $id = $args["id"];

        $params = [];
        
        $tableGroupBrand = $this->db->prepare("
                SELECT brand_id
                    FROM product_brand_group
                WHERE group_brand_id = :group_brand_id;
        ");
        $tableGroupBrand->bindParam(':group_brand_id', $id);
        $tableGroupBrand->execute();
        $params['users'] = $tableGroupBrand->fetchAll(PDO::FETCH_ASSOC);
                
        return $response->withJSON($params);
    })->setName('group-brands');

    $this->delete('/delete', function ($request, $response, $args) {
        $group_brand_id = $request->getParsedBodyParam('group_brand_id');
        $params = [
            'type' => -1,
            'message' => "Falta ingresar el id"
        ];

        if (isset($group_brand_id) && !empty($group_brand_id)) {
            $tableBrandGroup = $this->db->prepare("
                DELETE
                    FROM product_brand_group
                WHERE group_brand_id = :id;
            ");
            $tableBrandGroup->bindValue(':id', $group_brand_id);
            $tableBrandGroup->execute();
            $tableGroupBrand = $this->db->prepare("
                DELETE
                    FROM group_brand
                WHERE group_brand_id = :id;
            ");
            $tableGroupBrand->bindValue(':id', $group_brand_id);
            $tableGroupBrand->execute();
            
            $params = [
                'type' => 2,
                'message' => "Se eliminó el grupo"
            ];
        }

        $this->log->info("Se borró el grupo", "DELETE", "group_brand_id", $group_brand_id);

        return $response->withJson($params);
    })->setName('group-brand-delete');

    $this->post('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $existGroup = false;
        $brand_type = '';
        $params = [
            'type' => -1,
            'message' => "No se pudo guardar"
        ];

        if (isset($post)) {
            if (isset($post['group_name']) && !empty($post['group_name'])) {
                $getTableBrand = $this->db->prepare("
                    SELECT group_brand_name 
                        FROM group_brand 
                    WHERE group_brand_name = :name;
                ");
                $getTableBrand->bindValue(':name', $post['group_name']);
                $getTableBrand->execute();

                $groupName = $getTableBrand->fetchAll(PDO::FETCH_ASSOC);

                if (count($groupName) > 0) {
                    $existGroup = true;
                    $params = [
                        'type' => 1,
                        'message' => "El grupo ya existe"
                    ];
                }
            }

            if (!$existGroup) {
                $tableBrandGroup = $this->db->prepare("
                    INSERT INTO group_brand 
                    (group_brand_name) VALUES (:name);
                ");
                $tableBrandGroup->bindValue(':name', $post['group_name']);
                $tableBrandGroup->execute();
                
                $params = [
                    'type' => 2,
                    'message' => "Guardado"
                ];
                $group_brand_id = $this->db->lastInsertId();

                $this->log->info("Se creó el grupo ".$post['group_name'], "INSERT", "group_brand_id", $group_brand_id);
                
                if (isset($post['brands']) && !empty($post['brands'])) {
                    $tableBrandGroup = $this->db->prepare("
                        INSERT 
                            INTO product_brand_group 
                        (brand_id, group_brand_id) VALUES (:brand_id, :group_brand_id);
                    ");
                    $tableBrandGroup->bindValue(':group_brand_id', $group_brand_id);
                    
                    foreach ($post['brands'] as $brand_id) {
                        $tableBrandGroup->bindValue(':brand_id', $brand_id);
                        $tableBrandGroup->execute();
                        
                        $this->log->info("Se creó la asociación de marca grupo ".$post['group_name'], "INSERT", "brand_id", $brand_id);
                    }
                }
            }
        }

        return $response->withJson($params);
    })->setName('group-brand-create');

    $this->put('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $group_brand_id = $post['group_brand_id'];
        $params = [
            'type' => -1,
            'message' => "No se pudo actualizar"
        ];

        if (isset($post) && (isset($group_brand_id) && !empty($group_brand_id))) {
            if (isset($post['group_name']) && !empty($post['group_name'])) {
                $tableBrand = $this->db->prepare("
                    UPDATE group_brand 
                        SET group_brand_name = :name
                    WHERE group_brand_id = :id;
                ");
                $tableBrand->bindValue(':name', $post['group_name']);
                $tableBrand->bindValue(':id', $group_brand_id);
                $tableBrand->execute();
                
                $this->log->info("Se modificó el grupo ".$post['group_name'], "UPDATE", "group_brand_id", $group_brand_id);

                $params = [
                    'type' => 2,
                    'message' => "Actualizado"
                ];
            }

            $tableBrandGroup = $this->db->prepare("
                DELETE 
                    FROM product_brand_group 
                WHERE group_brand_id = :group_brand_id;
            ");
            $tableBrandGroup->bindValue(':group_brand_id', $group_brand_id);
            $tableBrandGroup->execute();
            
            $this->log->info("Se borró la asociación de marca grupo", "DELETE", "group_brand_id", $group_brand_id);

            if (isset($post['brands']) && !empty($post['brands'])) {
                
                $tableBrandGroup = $this->db->prepare("
                    INSERT 
                        INTO product_brand_group 
                    (brand_id, group_brand_id) VALUES (:brand_id, :group_brand_id);
                ");
                $tableBrandGroup->bindValue(':group_brand_id', $group_brand_id);

                foreach ($post['brands'] as $brand_id) {
                    $tableBrandGroup->bindValue(':brand_id', $brand_id);
                    $tableBrandGroup->execute();
                    
                    $this->log->info("Se modificó la asociación de marca grupo", "INSERT", "brand_id", $brand_id);
                }
            }
        }

        return $response->withJson($params);
    })->setName('group-brand-update');
});