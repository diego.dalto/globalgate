<?php

$app->group('/oportunidad', function () {

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        $opportunity_id = $args["id"];

        $sql = "SELECT opportunity_id, opportunity_salenumber, opportunity_type, opportunity_stage, opportunity_status, opportunity_createdate,
											opportunity_reason, opportunity_subclient_id, opportunity_integrator_user_id, opportunity_heritage, opportunity_conversion_date,
											opportunity_dolar, opportunity_total, opportunity_iddealregistrado, opportunity_currentexpiration, opportunity_comments,
											a.brand_id, a.user_id, a.client_id,
											CONCAT(user_name, ' ', user_lastname) AS user_namefull, client_businessname, brand_name, client_type
									FROM opportunity a, users b, client c, product_brand e
									WHERE a.user_id=b.user_id AND a.client_id=c.client_id AND a.brand_id=e.brand_id AND opportunity_id='" . $opportunity_id . "'";

        $query = $this->db->prepare($sql);

        $query->execute();
        $oportunidad = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////

        $query = $this->db->prepare("SELECT a.*, (a.opportunity_detail_qty*a.opportunity_detail_price) AS total_detail_item
									 FROM opportunity_detail a LEFT JOIN opportunity_detail b ON a.opportunity_detail_father=b.opportunity_detail_id
									 WHERE  a.oportunity_id=" . $oportunidad[0]["opportunity_id"] . "
									 ORDER BY coalesce(b.opportunity_detail_id, a.opportunity_detail_id), a.opportunity_detail_father");

        $query->execute();
        $productos = $query->fetchAll(PDO::FETCH_ASSOC);

        $cantidad_productos = count($productos);

        $query = $this->db->prepare("SELECT opportunity_detail_id, opportunity_detail_description, opportunity_detail_productid, opportunity_detail_moneda, opportunity_detail_qty,
											opportunity_detail_price, opportunity_detail_stock, oportunity_id, opportunity_detail_createdate,opportunity_detail_serial,
											(opportunity_detail_qty*opportunity_detail_price) AS total_detail_item,opportunity_detail_detail,opportunity_detail_type,
											opportunity_detail_father,opportunity_detail_renovation
									FROM opportunity_detail
									WHERE oportunity_id='" . $oportunidad[0]["opportunity_id"] . "' AND opportunity_detail_type IN ('HARD','SOFT','HW','SF','HD','SW')");

        $query->execute();
        $productos_padre = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////

        $query = $this->db->prepare("SELECT a.contact_id, contat_createdate, contact_name, contact_lastname, contact_email, contact_phone, contact_celphone,
											contact_status, contact_type, contact_area, client_id, user_id, b.opportunity_contact_id
									 FROM contact_client a, opportunity_contacts b
									 WHERE a.contact_id=b.contact_id AND b.opportunity_id='" . $oportunidad[0]["opportunity_id"] . "'");

        $query->execute();
        $contactos = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////

        $query = $this->db->prepare("SELECT milestone_id, milestone_createdate, milestone_title, milestone_detail, milestone_date, milestone_start_time, milestone_end_time,
											milestone_type, milestone_notification, milestone_status, milestone_link, opportunity_id, a.user_id, client_id, b.user_name,
											b.user_lastname,milestone_parent,milestone_children
									FROM milestone a, users b
									WHERE a.user_id=b.user_id AND opportunity_id='" . $oportunidad[0]["opportunity_id"] . "' ORDER BY milestone_id DESC, milestone_date DESC");

        $query->execute();
        $milestone = $query->fetchAll(PDO::FETCH_ASSOC);

        $milestone_last_date = $milestone[0]["milestone_date"];

        //////////////////////

        $query = $this->db->prepare("SELECT milestone_id, milestone_createdate, milestone_title, milestone_detail, milestone_date, milestone_start_time, milestone_end_time,
											milestone_type, milestone_notification, milestone_status, milestone_link, opportunity_id, a.user_id, client_id, b.user_name, b.user_lastname
									FROM milestone a, users b
									WHERE a.user_id=b.user_id AND opportunity_id='" . $oportunidad[0]["opportunity_id"] . "' AND milestone_type IN ('task','calendar','call','follow') AND milestone_status='0' ORDER BY milestone_date DESC");

        $query->execute();
        $task = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////

        $integrador = "";

        if ($oportunidad[0]["opportunity_integrator_user_id"] != "" && $oportunidad[0]["opportunity_integrator_user_id"] != "0") {
            $query = $this->db->prepare("SELECT client_id, client_businessname, client_address, client_city, client_state, client_cuit, client_invoicetype, client_agent, client_email
										FROM client WHERE client_id='" . $oportunidad[0]["opportunity_integrator_user_id"] . "'");

            $query->execute();
            $integrador = $query->fetchAll(PDO::FETCH_ASSOC);
            $integrador = $integrador[0];
        }

        //////////////////////

        $subcliente = "";

        if ($oportunidad[0]["opportunity_subclient_id"] != "" && $oportunidad[0]["opportunity_subclient_id"] != "0") {
            $query = $this->db->prepare("SELECT client_id, client_businessname, client_address, client_city, client_state, client_cuit, client_invoicetype, client_agent, client_email
										FROM client WHERE client_id='" . $oportunidad[0]["opportunity_subclient_id"] . "'");

            $query->execute();
            $subcliente = $query->fetchAll(PDO::FETCH_ASSOC);
            $subcliente = $subcliente[0];
        }

        //////////////////////

        $query = $this->db->prepare("SELECT user_id, CONVERT(CAST(CONCAT(user_name, ' ', user_lastname) as BINARY) USING utf8) AS user_name_full
									 FROM users WHERE user_status=1 ORDER BY user_name_full ASC");
        $query->execute();
        $usuarios = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////

        $tableOpportunityUser = $this->db->prepare("
                SELECT opportunity_user.user_id, CONCAT(user_name, ' ', user_lastname) AS user_fullname
                    FROM opportunity_user
                    NATURAL JOIN users
                WHERE opportunity_id = :opportunity_id;
        ");
        $tableOpportunityUser->bindParam(':opportunity_id', $opportunity_id);
        $tableOpportunityUser->execute();
        $otherUser = $tableOpportunityUser->fetchAll(PDO::FETCH_ASSOC);

        //////////////////////

        $opportunityOriginTable = $this->db->prepare("SELECT opportunity_origin_name
                                        FROM opportunity_origin;");
        $opportunityOriginTable->execute();
        $opportunityOrigin = $opportunityOriginTable->fetchAll(PDO::FETCH_ASSOC);


        $this->log->info("Se ingresó a edición de oportunidad", "SELECT", "opportunity_id", $opportunity_id);

        return $this->view->render($response, 'opportunity_edit.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'action' => 'EditarOportunidades',
                    'opportunity' => $oportunidad[0],
                    'milestone_last_date' => $milestone_last_date,
                    'products' => $productos,
                    'cantidad_productos' => $cantidad_productos,
                    'opportunity_origin' => $opportunityOrigin,
                    'contacts' => $contactos,
                    'integrator' => $integrador,
                    'subclient' => $subcliente,
                    'milestone' => $milestone,
                    'users' => $usuarios,
                    'otherUser' => $otherUser,
                    'products_father' => $productos_padre,
                    'task' => $task
        ]);
    })->setName('edit-opportunity');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/activas', function ($request, $response, $args) {
        $cliente = $request->getParam('cliente');
        $presupuesto = $request->getParam('presupuesto');
        $aRealizar = $request->getParam('a_realizar');
        $atiende = $request->getParam('atiende');
        $search = [
            'cliente' => '', 'presupuesto' => '',
            'a_realizar' => '', 'atiende' => ''
        ];

        $where = "";

        if (!empty($cliente)) {
            $where .= " AND cliente LIKE '%$cliente%'";
            $search['cliente'] = $cliente;
        }
        if (!empty($presupuesto)) {
            $where .= " AND presupuesto LIKE '%$presupuesto%'";
            $search['presupuesto'] = $presupuesto;
        }
        if (!empty($aRealizar)) {
            $where .= " AND a_realizar LIKE '%$aRealizar%'";
            $search['a_realizar'] = $aRealizar;
        }
        if (!empty($atiende)) {
            $where .= " AND atiende LIKE '%$atiende%'";
            $search['atiende'] = $atiende;
        }

        $query = $this->db->prepare("
                SELECT oportunidad_activa, cliente, fecha_alta, origen, codigo_cliente, tipo, contacto, cargo, telefono, cel, mail,
				       producto, cantidad, presupuesto, deal_id, etapa, ultimo_contacto, Ultimo_realizado, Prox_contacto, a_realizar,
					   atiende, estado, motivo, opportunity_id
                FROM oportunidad_activa
                WHERE 1=1 $where;");
        $query->execute();
        $oportunidad = $query->fetchAll(PDO::FETCH_ASSOC);

        foreach ($oportunidad as $key => &$val) {
            $val['presupuesto'] = trim(str_ireplace(['u$s', '$', 'u$s ', '$ '], ['u$s ', '$ ', 'u$s ', '$ '], $val['presupuesto']));
        }

        $query = $this->db->prepare("SELECT brand_id,brand_name
            FROM product_brand c
            WHERE 1=1 ORDER BY brand_name ASC");
        $query->execute();
        $brands = $query->fetchAll(PDO::FETCH_ASSOC);

        $query = $this->db->prepare("SELECT user_id,
            CONCAT(user_name, ' ', user_lastname) as name
            FROM users
            WHERE 1=1 ORDER BY name ASC");
        $query->execute();
        $users = $query->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se cargó lista oportunidades activas", "SELECT", "", 0);

        return $this->view->render($response, 'opportunity_list_active.twig', [
                    'search' => $search,
                    'brands' => $brands,
                    'users' => $users,
                    'opportunity_activites' => $oportunidad
        ]);
    })->setName('opportunity-list-active');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/listado/{new}', function ($request, $response, $args) {

        $data = $request->getParsedBody();
        $new = $args["new"];
        $where = '';
        $where2 = '';
        $userId = $this->session->user_id;

		$limit_list = "LIMIT 100";

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['noportunidad']) && $data['noportunidad'] != "") {
                $texto = trim($data['noportunidad']);
                $where .= "AND opportunity_id like '%" . $texto . "%' ";
                //$where2 .= $where;
            }
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['clientes']) && $data['clientes'] != "") {
                $texto = trim($data['clientes']);
                $where .= "AND client_businessname like '%" . $texto . "%' ";
                //$where2 .= $where;
            }
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['marcas']) && $data['marcas'] != "") {
                $texto = trim($data['marcas']);
                $where .= "AND brand_id='" . $texto . "' ";
                //$where2 .= $where;
            }
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['etapas']) && $data['etapas'] != "") {
                $texto = trim($data['etapas']);
                $where .= "AND opportunity_stage='" . $texto . "' ";
               // $where2 .= $where;
            }
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['estado']) && $data['estado'] != "") {
                $texto = trim($data['estado']);
                $where .= "AND opportunity_status='" . $texto . "' ";
               // $where2 .= $where;

				$limit_list = "";
            }
        } else {
            $where .= "AND opportunity_status='A' ";
            //$where2 .= $where;
            $data['estado'] = "A";
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['fechadesde']) && $data['fechadesde'] != "") {
                $texto = trim($data['fechadesde']);
                $texto = date("Y-m-d", strtotime(str_replace("/", "-", $texto)));
                $where .= "AND opportunity_createdate>='" . $texto . " 00:00:00' ";
                //$where2 .= $where;

				$limit_list = "";
            }
        }

        /////////
		
		if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['iddeal']) && $data['iddeal'] != "") {
                $texto = trim($data['iddeal']);
				
                $where .= "AND opportunity_iddealregistrado LIKE '%" . $texto . " %' ";
                //$where2 .= $where;

				$limit_list = "";
            }
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['fechahasta']) && $data['fechahasta'] != "") {
                $texto = trim($data['fechahasta']);
                $texto = date("Y-m-d", strtotime(str_replace("/", "-", $texto)));
                $where .= "AND opportunity_createdate<='" . $texto . " 23:59:59' ";
                //$where2 .= $where;

				$limit_list = "";
            }
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['client_email']) && $data['client_email'] != "") {
                $texto = trim($data['client_email']);
				
				$where .= " AND opportunity_contacts LIKE '%".$texto."%'";
				
                /**
                  * El query se demora, no sirve
                  */
                //$where .= " AND (a.opportunity_id IN (SELECT oc.opportunity_id FROM opportunity_contacts oc, contact_client cc WHERE oc.contact_id=cc.contact_id AND contact_email = '$texto') OR client_email = '$texto') ";
                //$where2 .= $where;
            }
        }

        /////////
        /////////
		
		$user_name = "";
		
        if ($new == "me") {
            $tableUser = $this->db->prepare("
                SELECT CONCAT(user_name, ' ', user_lastname) as us
                    FROM users
                WHERE user_id = :user_id LIMIT 1;
            ");
            $tableUser->bindParam(':user_id', $userId);
            $tableUser->execute();
            $user_name = $tableUser->fetch(PDO::FETCH_ASSOC);
			$user_name = $user_name['us'];
			
            if (empty($data['usuarios'])) {
                $data['usuarios'] = $user_name;
            }
        }

		
        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['usuarios']) && $data['usuarios'] != "") {
                $texto = trim($data['usuarios']);
                $where .= " AND  (user_namefull LIKE '%" . $texto . "%' OR opportunity_user LIKE '%" . $texto . "%') ";
                //$where2 .= " AND ou.user_id = $userId ";
				//$where2 .= " AND ou.user_id IN (SELECT xx.user_id FROM opportunity_user xx, users zz WHERE xx.user_id=zz.user_id AND (CONCAT(zz.user_name, ' ', zz.user_lastname)) like '%" . $texto . "%') ";

				$limit_list = "";
            } elseif ($new == "me") {
                $where .= " AND (user_id = $userId  OR opportunity_user LIKE '%" . $user_name . "%') ";
                //$where2 .= " AND ou.user_id = $userId ";
            }
        }

        /////////
        /////////

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['client_type']) && $data['client_type'] != "") {
                $texto = trim($data['client_type']);
                $where .= " AND client_type LIKE '%" . $texto . "%' ";
                //$where2 .= $where;
            }
        }

        /////////

        if (isset($data)) {//válido que lleguen parámetros //SKU
            if (isset($data['titulo']) && $data['titulo'] != "") {
				
				$where .= " AND opportunity_detail LIKE '%".trim($data['titulo'])."%'";
				
				////$w .= "AND (xx.opportunity_detail_description LIKE '%{$data['titulo']}%' OR "
						//. "xx.opportunity_detail_detail LIKE '%{$data['titulo']}%' OR "
						//. "xx.opportunity_detail_productid LIKE '%{$data['titulo']}%')";

                ////$where .= " AND a.opportunity_id IN (SELECT xx.oportunity_id FROM opportunity_detail xx WHERE 1=1 $w ) ";
                //$where2 .= $where;
            }
        }

		/////////		
		if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['serial_number']) && $data['serial_number'] != "") {
				
				$where .= " AND opportunity_detail_serial LIKE '%".trim($data['serial_number'])."%'";
				
				//$w = "AND xx.opportunity_detail_serial LIKE '%{$data['serial_number']}%'";

               // $where .= " AND a.opportunity_id IN (SELECT xx.oportunity_id FROM opportunity_detail xx  WHERE 1=1 $w) ";
               // $where2 .= $where;

				//$where .= " AND a.opportunity_id IN (SELECT xx.oportunity_id "
                       // . "FROM serials_contract zz, opportunity_detail xx"
                       // . "WHERE zz.opportunity_detail_id=xxopportunity_detail_id AND xx.opportunity_detail_id = a.opportunity_id $w) ";
            }
        }


		/*$sql="(SELECT
                a.opportunity_id, opportunity_salenumber, opportunity_type, opportunity_stage, opportunity_status, opportunity_createdate,
                opportunity_reason, opportunity_subclient_id, opportunity_integrator_user_id, opportunity_heritage, opportunity_conversion_date,
                opportunity_dolar, opportunity_total, opportunity_iddealregistrado, opportunity_currentexpiration, opportunity_comments,
                a.brand_id, a.user_id, a.client_id,
                CONCAT(user_name, ' ', user_lastname) AS user_namefull, client_businessname, brand_name,
                (SELECT SUM(opportunity_detail_price) FROM opportunity_detail d WHERE d.oportunity_id=a.opportunity_id) AS importe,
                client_type, 'titular' AS permiso,
                (SELECT subc.client_businessname FROM client subc WHERE subc.client_id = a.opportunity_subclient_id) AS subclient,
                (SELECT subc.client_businessname FROM client subc WHERE subc.client_id = a.opportunity_integrator_user_id) AS integrador
            FROM opportunity a, opportunity_contacts oc, users b, client c, product_brand e, serials_contract, contact_client cc
            WHERE a.user_id=b.user_id AND a.client_id=c.client_id AND a.brand_id=e.brand_id $where $limit_list)
            UNION
            (SELECT
                a.opportunity_id, opportunity_salenumber, opportunity_type, opportunity_stage, opportunity_status, opportunity_createdate,
                opportunity_reason, opportunity_subclient_id, opportunity_integrator_user_id, opportunity_heritage, opportunity_conversion_date,
                opportunity_dolar, opportunity_total, opportunity_iddealregistrado, opportunity_currentexpiration, opportunity_comments,
                a.brand_id, a.user_id, a.client_id,
                CONCAT(user_name, ' ', user_lastname) AS user_namefull, client_businessname, brand_name,
                (SELECT SUM(opportunity_detail_price) FROM opportunity_detail d WHERE d.oportunity_id=a.opportunity_id) AS importe,
                client_type, 'adicional' AS permiso,
                (SELECT subc.client_businessname FROM client subc WHERE subc.client_id = a.opportunity_subclient_id) AS subclient,
                (SELECT subc.client_businessname FROM client subc WHERE subc.client_id = a.opportunity_integrator_user_id) AS integrador
            FROM opportunity a, opportunity_contacts oc, users b, client c, product_brand e, serials_contract, opportunity_user ou, contact_client cc
            WHERE a.user_id=b.user_id AND a.client_id=c.client_id AND a.brand_id=e.brand_id AND ou.opportunity_id=a.opportunity_id $where2 $limit_list);";*/
		$sql="SELECT 
		opportunity_id,opportunity_salenumber,opportunity_type,opportunity_stage,opportunity_status,opportunity_createdate,opportunity_reason,
		opportunity_subclient_id,opportunity_integrator_user_id,opportunity_heritage,opportunity_conversion_date,opportunity_dolar,opportunity_total,
		opportunity_iddealregistrado,opportunity_currentexpiration,opportunity_comments,brand_id,user_id,client_id,client_type,client_email,user_namefull,
		brand_name,client_businessname,permiso,opportunity_contacts,opportunity_user,opportunity_detail,opportunity_detail_serial,importe,subclient,integrador 
		FROM (
			SELECT
				a.opportunity_id, 
				a.opportunity_salenumber, 
				a.opportunity_type, 
				a.opportunity_stage, 
				a.opportunity_status, 
				a.opportunity_createdate, 
				a.opportunity_reason, 
				a.opportunity_subclient_id, 
				a.opportunity_integrator_user_id, 
				a.opportunity_heritage, 
				a.opportunity_conversion_date, 
				a.opportunity_dolar, 
				a.opportunity_total, 
				a.opportunity_iddealregistrado, 
				a.opportunity_currentexpiration, 
				a.opportunity_comments, 
				a.brand_id, 
				a.user_id, 
				a.client_id,
				c.client_type,
				c.client_email,
				CONCAT(b.user_name, ' ', b.user_lastname) AS user_namefull, 
				e.brand_name,
				c.client_businessname, 
				'titular' AS permiso,				
				detail_opportunity_contacts(a.opportunity_id) AS opportunity_contacts,
				detail_opportunity_user(a.opportunity_id) AS opportunity_user,
				detail_opportunity_detail(a.opportunity_id) AS opportunity_detail,
				detail_opportunity_detail_serial(a.opportunity_id) AS opportunity_detail_serial,
				 
				(SELECT SUM(opportunity_detail_price) FROM opportunity_detail d WHERE d.oportunity_id=a.opportunity_id) AS importe,
				(SELECT subc.client_businessname FROM client subc WHERE subc.client_id = a.opportunity_subclient_id) AS subclient,
				(SELECT subc.client_businessname FROM client subc WHERE subc.client_id = a.opportunity_integrator_user_id) AS integrador
			FROM 
				opportunity a,
				users b, 
				product_brand e,
				client c
			WHERE 
				a.user_id=b.user_id AND  
				a.brand_id=e.brand_id AND 
				a.client_id=c.client_id) op_list
			WHERE 1
				$where				
			$limit_list";

			$query = $this->db->prepare($sql);

        $query->execute();
        $oportunidades = $query->fetchAll(PDO::FETCH_ASSOC);

        #return $query->debugDumpParams();

        $this->log->info("Se cargó lista de oportunidad", "SELECT", "", 0);

        $query = $this->db->prepare("SELECT brand_id,brand_name
                                        FROM product_brand c
                                    WHERE 1=1 ORDER BY brand_name ASC");//brand_id NOT IN (99)
        $query->execute();
        $marcas = $query->fetchAll(PDO::FETCH_ASSOC);

        $query = $this->db->prepare("SELECT opportunity_origin_id,opportunity_origin_name
                                        FROM opportunity_origin
                                    WHERE 1=1 ORDER BY opportunity_origin_name ASC");
        $query->execute();
        $opportunity_origin = $query->fetchAll(PDO::FETCH_ASSOC);

        return $this->view->render($response, 'opportunity_list.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'action' => 'ListadoOportunidades',
                    'new' => $new,
                    'opportunity' => $oportunidades,
                    'opportunity_origin' => $opportunity_origin,
                    'brands' => $marcas,
                    'me' => $user_name,
                    'params' => $data
        ]);
    })->setName('list-opportunity');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/jsonUsuarios', function ($request, $response, $args) {
        $query = $this->db->prepare("SELECT user_id, CONVERT(CAST(CONCAT(user_name, ' ', user_lastname) as BINARY) USING utf8) AS user_name_full
									 FROM users WHERE user_status=1 ORDER BY user_name_full ASC");
        $query->execute();
        $usuarios = $query->fetchAll(PDO::FETCH_ASSOC);

        $users = array();
        for ($i = 0; $i < count($usuarios); $i++) {
            array_push($users, array("name" => (($usuarios[$i]["user_name_full"])), "id" => $usuarios[$i]["user_id"]));
        }

        return $response->withJson($users, 200, JSON_UNESCAPED_UNICODE);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/jsonClientes[/{type}]', function ($request, $response, $args) {
        $type = "";

        if (array_key_exists("type", $args)) {
            $type = $args['type'];
        }

        $where = "";
        if (!empty($type)) {
            $type = str_replace("_", " ", $type);

            if($type == "Tercera Empresa") {
              $where = " AND (client_type != 'Integrador' OR client_type IS NULL) ";
            } else {
              $where = " AND client_type = '$type' ";
            }
        }

        $query = $this->db->prepare("SELECT client_id, client_businessname AS client_name, client_type
                                        FROM client WHERE client_status='A' $where ORDER BY client_name ASC");
        $query->execute();
        $clientes = $query->fetchAll(PDO::FETCH_ASSOC);

        $clients = array();
        for ($i = 0; $i < count($clientes); $i++) {
            array_push($clients, array("name" => $clientes[$i]["client_name"], "id" => $clientes[$i]["client_id"], "type" => $clientes[$i]["client_type"]));
        }

        return $response->withJson($clients, 200);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/jsonProductos/{brand}/{sku}/{keyword}/{hardware}', function ($request, $response, $args) {

        $brand = $request->getAttribute('brand');
        $sku = $request->getAttribute('sku');
        $hardware = $request->getAttribute('hardware');
        $keyword = $request->getAttribute('keyword');

        $where = "";

        if ($hardware != "" && $hardware != "-") {
            $where .= " AND type LIKE '%" . $hardware . "%' ";
        }

        if ($sku != "" && $sku != "-") {
            $sku = str_replace(' ', '%', $sku);
            $where .= " AND sku LIKE '%" . $sku . "%' ";
        }
        if ($keyword != "" && $keyword != "-") {
            $where .= " AND (description1 LIKE '%" . $keyword . "%' OR item LIKE '%" . $keyword . "%')";
        }

        if ($brand != "-" && $brand != 0) {
            if ($brand == 98) {
                //Multimarca, muestra todo
                $where .= " ";
            } else {
                $where .= " AND a.brand_id IN (2,99," . $brand . ")";
            }
        }
        $sql = "SELECT product_id, family,item,type,sku,description1,price,category,brand_name
				FROM products a, product_brand b
				WHERE a.brand_id=b.brand_id AND product_status='A' " . $where . " ORDER BY family,product,type ASC LIMIT 0,200";

        $query = $this->db->prepare($sql);
        $query->execute();
        $products = $query->fetchAll(PDO::FETCH_ASSOC);

        $products_list = array();
        for ($i = 0; $i < count($products); $i++) {
            /* array_push($products_list, array("family" => $products[$i]["family"],
              "item" => $products[$i]["item"],
              "type" => $products[$i]["type"],
              "sku" => $products[$i]["sku"],
              "description1" => $products[$i]["description1"],
              "price" => $products[$i]["price"],
              "category" => $products[$i]["category"])); */

			$products[$i]["description1"] = strip_tags($products[$i]["description1"]);
			$products[$i]["description1"] = str_replace("\n","",$products[$i]["description1"]);
			$products[$i]["description1"] = str_replace("\r","",$products[$i]["description1"]);
			$products[$i]["description1"] = str_replace("\t","",$products[$i]["description1"]);


            array_push($products_list, array(
                "product_id" => $products[$i]["product_id"],
                "brand_name" => $products[$i]["brand_name"],
                "family" => ($products[$i]["family"]),
                "item" => ($products[$i]["item"]),
                "type" => $products[$i]["type"],
                "sku" => $products[$i]["sku"],
                "description1" => (mberegi_replace("[\n|\r|\n\r|\t||\x0B]", "", $products[$i]["description1"])),
                "price" => (($products[$i]["price"] == "") ? "0" : $products[$i]["price"]),
                "category" => (($products[$i]["category"] == "") ? "-" : $products[$i]["category"]))
            );
        }

        return $response->withJson($products_list, 200);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/jsonContactos/{idClient}', function ($request, $response, $args) {
        $idClient = $request->getAttribute('idClient');

        if ($idClient != "" && $idClient != "0") {
            $where = " AND client_id=" . $idClient . " ";
        }

        $query = $this->db->prepare("SELECT contact_id, CONVERT(CAST(CONCAT(contact_name, ' ', contact_lastname) as BINARY) USING utf8) AS contact_name
                                    FROM contact_client WHERE contact_status='1' " . $where . " ORDER BY CONCAT(contact_name, ' ', contact_lastname) ASC");
        $query->execute();
        $contactos = $query->fetchAll(PDO::FETCH_ASSOC);

        $contacts = array();
        for ($i = 0; $i < count($contactos); $i++) {
            array_push($contacts, array("name" => (($contactos[$i]["contact_name"])), "id" => $contactos[$i]["contact_id"]));
        }

        $this->log->info("Se cargó lista de contactos de cliente", "SELECT", "client_id", empty($idClient) ? 0 : $idClient);

        return $response->withJson($contacts, 200);
    })->setName('opportunity-client-contact');

    ///////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/json/opportunity/contact/email', function ($request, $response, $args) {
        $email = $args['email'];

        $query = $this->db->prepare("
        SELECT client_email
          FROM (
            SELECT client_email
              FROM client c, opportunity o
            WHERE o.client_id = c.client_id AND (client_email IS NOT NULL AND client_email != '')
            UNION
            SELECT contact_email AS client_email
              FROM contact_client cc, opportunity o, opportunity_contacts oc
            WHERE  cc.contact_id = oc.contact_id AND (contact_email IS NOT NULL AND contact_email != '')
          ) t
          GROUP BY client_email ASC
        ");
        $query->execute();
        $emails = $query->fetchAll(PDO::FETCH_ASSOC);

        //revisar porque se rearma el array
        $listEmails = [];
        foreach($emails as $email) {
          $push = [
            "name" => $email['client_email']
          ];
          array_push($listEmails, $push);
        }

        return $response->withJson($listEmails, 200);
    })->setName('opportunity-client-email');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/infoContacto/{id:[0-9]+}', function ($request, $response, $args) {
        $contact_id = $args["id"];

        $query = $this->db->prepare("SELECT contact_id, contat_createdate, contact_name, contact_lastname, contact_email, contact_phone, contact_celphone, contact_status, contact_type, client_id, user_id, contact_area
									FROM contact_client WHERE contact_id=:cid");
        $query->bindParam(':cid', $contact_id);
        $query->execute();
        $contactos = $query->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se cargó info de contacto", "SELECT", "contact_id", $contact_id);

        return $response->withJson($contactos[0], 200);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/jsonCliente/{idCliente}/type', function ($request, $response, $args) {
        $id = $request->getAttribute('idCliente');

        $query = $this->db->prepare("SELECT client_type
                                     FROM client
                                     WHERE client_id='" . $id . "'");
        $query->execute();
        $cliente = $query->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se cargó tipo de cliente", "SELECT", "client_id", $client_id);

        return $response->withJson($cliente[0], 200);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/jsonCliente/{idOportunidad}', function ($request, $response, $args) {
        $id = $request->getAttribute('idOportunidad');

        $query = $this->db->prepare("SELECT a.client_id, client_origin, client_code, client_businessname, client_address, client_city,
                                        client_state, client_contact, client_cuit, client_invoicetype, client_phone0, client_phone1,
                                        client_phone2, client_phone3, client_fax, client_invoicedate, client_newtinvoicedate,
                                        client_cp, client_createdate, client_notes, client_agent, client_email, client_www,
                                        client_status, client_leyen, client_entry, client_type, client_tc, client_fc, client_cuitpais,
                                        client_npais,client_activity
                                     FROM client a
                                     WHERE client_id='" . $id . "'");

        $query->execute();
        $cliente = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////

        $query = $this->db->prepare("SELECT milestone_title,milestone_detail,milestone_date,milestone_start_time,milestone_end_time,milestone_type,milestone_status,a.opportunity_id
                                        FROM milestone a, opportunity b
                                        WHERE a.opportunity_id=b.opportunity_id AND b.opportunity_status='A' AND a.client_id='" . $cliente[0]["client_id"] . "'
                                        ORDER BY milestone_date DESC LIMIT 0,15");

        $query->execute();
        $milestone = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////

        $query = $this->db->prepare("SELECT serials_contract_id, a.opportunity_detail_id, serials_contract_type, serials_contract_createdate, serials_contract_number, serials_contract_date, serials_contract_status,
										opportunity_detail_description,c.opportunity_id
									 FROM serials_contract a, opportunity_detail b, opportunity c, client d
									 WHERE a.opportunity_detail_id=b.opportunity_detail_id AND b.oportunity_id=c.opportunity_id AND c.client_id=d.client_id AND serials_contract_status=1 AND d.client_id='" . $cliente[0]["client_id"] . "'
									 ORDER BY serials_contract_date DESC ");

        $query->execute();
        $vencimientos = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////

        $query = $this->db->prepare("SELECT opportunity_id, opportunity_salenumber, opportunity_type, opportunity_stage, opportunity_status, opportunity_total, opportunity_currentexpiration, opportunity_createdate
		                             FROM opportunity WHERE client_id='" . $cliente[0]["client_id"] . "' AND opportunity_status IN ('A','V')
									 ORDER BY opportunity_currentexpiration DESC");

        $query->execute();
        $oportunidades = $query->fetchAll(PDO::FETCH_ASSOC);


        //////////

        $query = $this->db->prepare("SELECT contact_id, contat_createdate, contact_name, contact_lastname, contact_email, contact_phone, contact_celphone, contact_status, contact_type, contact_area, client_id, user_id
									 FROM contact_client
									 WHERE client_id='" . $cliente[0]["client_id"] . "'");

        $query->execute();
        $contactos = $query->fetchAll(PDO::FETCH_ASSOC);

        //////////

        $tablas = array();
        array_push($tablas, $cliente);
        array_push($tablas, $milestone);
        array_push($tablas, $vencimientos);
        array_push($tablas, $contactos);
        array_push($tablas, $oportunidades);

        return $response->withJson($tablas, 200, JSON_PARTIAL_OUTPUT_ON_ERROR);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////
    $this->get('/usuarios/opportunity_id/{id}', function ($request, $response, $args) {
        $id = $args["id"];

        $params = [];

        $tableOpportunityUser = $this->db->prepare("
                SELECT user_id
                    FROM opportunity_user
                WHERE opportunity_id = :opportunity_id;
        ");
        $tableOpportunityUser->bindParam(':opportunity_id', $id);
        $tableOpportunityUser->execute();
        $params['users'] = $tableOpportunityUser->fetchAll(PDO::FETCH_ASSOC);

        return $response->withJSON($params);
    })->setName('opportunity-users');
    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/editarOpportunity/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $milestone = $request->getParsedBody();

        if ($milestone["opportunity_currentexpiration"] != "") {
            $fecha = explode("/", $milestone["opportunity_currentexpiration"]);
            $milestone["opportunity_currentexpiration"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];
        } else {
            $milestone["opportunity_currentexpiration"] = NULL;
        }

        if ($milestone["opportunity_dolar"] != "") {
            $milestone["opportunity_dolar"] = str_replace(",", ".", $milestone["opportunity_dolar"]);
        } else {
            $milestone["opportunity_dolar"] = 0;
        }

        $userId = $this->session->user_id;

        $query = $this->db->prepare("UPDATE opportunity SET
										opportunity_currentexpiration=:opportunity_currentexpiration, opportunity_iddealregistrado=:opportunity_iddealregistrado, opportunity_dolar=:opportunity_dolar,
										opportunity_comments=:opportunity_comments,opportunity_reason=:opportunity_reason
								     WHERE opportunity_id=:id ");

        $query->bindParam(':opportunity_currentexpiration', $milestone["opportunity_currentexpiration"]);
        $query->bindParam(':opportunity_iddealregistrado', $milestone["opportunity_iddealregistrado"]);
        $query->bindParam(':opportunity_dolar', $milestone["opportunity_dolar"]);
        //$query->bindParam(':opportunity_total', $milestone["opportunity_total"]);
        $query->bindParam(':opportunity_comments', $milestone["opportunity_comments"]);
        $query->bindParam(':opportunity_reason', $milestone["opportunity_reason"]);
        $query->bindParam(':id', $oportunity_id);

        $query->execute();

        ////////////////////////////////

        $this->log->info("Se editó la Oportunidad", "UPDATE", "opportunity_id", $oportunity_id);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/cerrarOpportunity/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_status=:milestone_status WHERE opportunity_id=:id ");

        $query->bindParam(':milestone_status', $milestone["milestone_status"]);
        $query->bindParam(':id', $oportunity_id);

        $query->execute();

        ///////////////////////////////

        $milestone_status = "0";
        $milestone_type = "notice";
        $milestone_date = explode("/", $milestone["milestone_date"]);
        $milestone_date = $milestone_date[2] . "-" . $milestone_date[1] . "-" . $milestone_date[0];

        $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date, milestone_start_time,
		 													milestone_end_time, milestone_type, milestone_notification, milestone_status,
															milestone_link, opportunity_id, user_id, client_id)
									 VALUES (:mtitle, :mdetail, :mdate, NULL, NULL, :mtype, 'N', :mstatus, NULL, :opid, :uid, :cid);");

        $query->bindParam(':mtitle', $milestone["milestone_title"]);
        $query->bindParam(':mdetail', $milestone["milestone_detail"]);
        $query->bindParam(':mdate', $milestone_date);
        $query->bindParam(':mtype', $milestone_type);
        $query->bindParam(':mstatus', $milestone_status);

        $query->bindParam(':cid', $milestone["mclient_id"]);
        $query->bindParam(':uid', $userId);
        $query->bindParam(':opid', $oportunity_id);

        $query->execute();

        ////////////////////////////////

        $this->log->info("Se cerró la oportunidad", "UPDATE", "opportunity_id", $oportunity_id);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/ganarOpportunity/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $query = $this->db->prepare("SELECT MAX(opportunity_salenumber) AS opportunity_salenumber FROM opportunity");
        $query->execute();
        $max = $query->fetchAll(PDO::FETCH_ASSOC);

        $maximo = $max[0]["opportunity_salenumber"];
        $maximo = (date("y") * 10000) + (($maximo - (substr($maximo, 0, 2) * 10000)) + 1);

        ///////////////////////////////

        $query = $this->db->prepare("UPDATE opportunity
		                             SET opportunity_salenumber='" . $maximo . "'
									 WHERE opportunity_id=:id AND (opportunity_salenumber='' OR opportunity_salenumber IS NULL) ");

        $query->bindParam(':id', $oportunity_id);
        $query->execute();

        $query = $this->db->prepare("UPDATE opportunity
		                             SET opportunity_status=:milestone_status,opportunity_conversion_date='" . date("Y-m-d H:i:s") . "'
									 WHERE opportunity_id=:id ");

        $query->bindParam(':milestone_status', $milestone["milestone_status"]);
        $query->bindParam(':id', $oportunity_id);
        $query->execute();

        ///////////////////////////////

        $query = $this->db->prepare("SELECT opportunity_detail_id, opportunity_detail_serial,opportunity_detail_qty,opportunity_detail_type,opportunity_detail_father,opportunity_detail_enddate,opportunity_detail_productid
									 FROM opportunity_detail WHERE opportunity_detail_father=0 AND oportunity_id='" . $oportunity_id . "'");
        $query->execute();
        $detalles = $query->fetchAll(PDO::FETCH_ASSOC);

        for ($i = 0; $i < count($detalles); $i++) {
            $cant_items = 1;
            $cant_contratos = 0;
            $tipo_sc = "Serial";

            if ($milestone["mbrand_id"] == 1) {
                /* if($detalles[$i]["opportunity_detail_father"] == "0"){
                  if ($detalles[$i]["opportunity_detail_type"] == "HARD" || $detalles[$i]["opportunity_detail_type"] == "SOFT" || $detalles[$i]["opportunity_detail_type"] == "Service" || $detalles[$i]["opportunity_detail_type"] == "SaaS") {
                  $cant_items = $detalles[$i]["opportunity_detail_qty"];
                  }
                  }else{
                  $query = $this->db->prepare("SELECT opportunity_detail_qty FROM opportunity_detail WHERE oportunity_id='" . $oportunity_id . "' AND opportunity_detail_id='".$detalles[$i]["opportunity_detail_father"]."'");
                  $query->execute();
                  $detalles_padre = $query->fetchAll(PDO::FETCH_ASSOC);

                  $cant_items = round($detalles[$i]["opportunity_detail_qty"]/$detalles_padre[0]["opportunity_detail_qty"]);
                  } */


                if ($detalles[$i]["opportunity_detail_type"] == "HARD" || $detalles[$i]["opportunity_detail_type"] == "HD" || $detalles[$i]["opportunity_detail_type"] == "HW" || $detalles[$i]["opportunity_detail_type"] == "SOFT" || $detalles[$i]["opportunity_detail_type"] == "SF" || $detalles[$i]["opportunity_detail_type"] == "SW") {
                    $cant_items = $detalles[$i]["opportunity_detail_qty"];
                } elseif ($detalles[$i]["opportunity_detail_type"] == "Service" || $detalles[$i]["opportunity_detail_type"] == "SaaS") {
                   	if ($detalles[$i]["opportunity_detail_productid"] == "FTNT-COT") {
                        $cant_items = $detalles[$i]["opportunity_detail_qty"];
                    } else {
                        $sc = explode(",", $detalles[$i]["opportunity_detail_serial"]);

                        /*if (count($sc) == $detalles[$i]["opportunity_detail_qty"]) {
                            $cant_items = $detalles[$i]["opportunity_detail_qty"];
                        }elseif (count($sc) < $detalles[$i]["opportunity_detail_qty"]) {
                            $cant_items = ceil($detalles[$i]["opportunity_detail_qty"]/count($sc));
                        }else {
                            $cant_items = ceil(count($sc)/$detalles[$i]["opportunity_detail_qty"]);
                        }*/
						/*if (count($sc) >= $detalles[$i]["opportunity_detail_qty"]) {
                            $cant_items = count($sc);
                        } else {
                            $cant_items = $detalles[$i]["opportunity_detail_qty"];
                        }*/
						$cant_items = count($sc);

                        $cant_contratos = ceil($detalles[$i]["opportunity_detail_qty"]/count($sc));
                    }
                }
            } else {
                if ($detalles[$i]["opportunity_detail_type"] == "HARD" || $detalles[$i]["opportunity_detail_type"] == "HD" || $detalles[$i]["opportunity_detail_type"] == "HW") {
                    $cant_items = $detalles[$i]["opportunity_detail_qty"];
                }
            }



            $serials = array();
            if ($detalles[$i]["opportunity_detail_serial"] != "") {
                $sc = explode(",", $detalles[$i]["opportunity_detail_serial"]);
                for ($y = 0; $y < count($sc); $y++) {
                    array_push($serials, trim($sc[$y]));
                }
            }

			/////////////////////////////////////////////////////////////////////////////////////////////////////

            for ($j = 0; $j < $cant_items; $j++) {
                $serial_final = "";

                if (array_key_exists($j, $serials)) {
                    $serial_final = $serials[$j];
                }

				/////Adiciona fecvha vencimiento solo para software y hardware

				if($detalles[$i]["opportunity_detail_type"] == "HARD" || $detalles[$i]["opportunity_detail_type"] == "HD" || $detalles[$i]["opportunity_detail_type"] == "HW" || $detalles[$i]["opportunity_detail_type"] == "SOFT" || $detalles[$i]["opportunity_detail_type"] == "SF" || $detalles[$i]["opportunity_detail_type"] == "SW"){
					$query = $this->db->prepare("INSERT IGNORE INTO serials_contract (opportunity_detail_id,serials_contract_type,serials_contract_createdate,serials_contract_number,serials_contract_status,serials_contract_enddate)
												 VALUE('" . $detalles[$i]["opportunity_detail_id"] . "','" . $tipo_sc . "','" . date("Y-m-d H:i:s") . "','" . $serial_final . "','0','" . $detalles[$i]["opportunity_detail_enddate"] . "')");
					$query->execute();
					$sc_id_father = $this->db->lastInsertId();
				}else{
					$query = $this->db->prepare("INSERT IGNORE INTO serials_contract (opportunity_detail_id,serials_contract_type,serials_contract_createdate,serials_contract_number,serials_contract_status)
												 VALUE('" . $detalles[$i]["opportunity_detail_id"] . "','" . $tipo_sc . "','" . date("Y-m-d H:i:s") . "','" . $serial_final . "','0')");
					$query->execute();
					$sc_id_father = $this->db->lastInsertId();
				}

				/////

                if($tipo_sc == "Serial") {
                    $query = $this->db->prepare("INSERT IGNORE INTO serial_history
                        (
                            serial_history_number, serials_contract_id
                        ) VALUE (
                            '$serial_final',  '$sc_id_father'
                        )");
                    $query->execute();
                }

                /////////////////////////////////////////

                $query = $this->db->prepare("SELECT opportunity_detail_id, opportunity_detail_serial,opportunity_detail_qty,opportunity_detail_type,opportunity_detail_father,opportunity_detail_enddate
											 FROM opportunity_detail
											 WHERE opportunity_detail_father=" . $detalles[$i]["opportunity_detail_id"] . " AND oportunity_id='" . $oportunity_id . "'");
                $query->execute();
                $detalles_hijos = $query->fetchAll(PDO::FETCH_ASSOC);

                for ($k = 0; $k < count($detalles_hijos); $k++) {
                    $query = $this->db->prepare("INSERT IGNORE INTO serials_contract (opportunity_detail_id,serials_contract_type,serials_contract_createdate,serials_contract_number,serials_contract_status,serials_contract_parent,serials_contract_enddate)
												 VALUE('" . $detalles_hijos[$k]["opportunity_detail_id"] . "','Serial','" . date("Y-m-d H:i:s") . "','" . $serial_final . "','0','" . $sc_id_father . "','" . $detalles_hijos[$k]["opportunity_detail_enddate"] . "')");
                    $query->execute();
                    $sc_id = $this->db->lastInsertId();

                    if($tipo_sc == "Serial") {
                        $query = $this->db->prepare("
                            INSERT IGNORE INTO serial_history
                            (
                                serial_history_number, serials_contract_id
                            ) VALUE (
                                '$serial_final',  '$sc_id_father'
                            )");
                        $query->execute();
                    }
                    //////////////////////
                    $cant_contratos_hijos = 1;

                    if ($milestone["mbrand_id"] == 1) {
                        $cant_contratos_hijos = ceil($detalles_hijos[$k]["opportunity_detail_qty"] / $cant_items);
                    }

                    for ($h = 0; $h < $cant_contratos_hijos; $h++) {
                        #Revisar key $k != $h
                        $query = $this->db->prepare("INSERT IGNORE INTO serials_contract (opportunity_detail_id,serials_contract_type,serials_contract_createdate,serials_contract_number,serials_contract_status,serials_contract_parent)
                                                     VALUE('" . $detalles_hijos[$k]["opportunity_detail_id"] . "','Contrato','" . date("Y-m-d H:i:s") . "','','0','" . $sc_id . "')");
                        $query->execute();
                    }
                }

                /////////////////////////////////////////
				//solo para contratos de Services o SaaS
				for ($g = 0; $g < $cant_contratos; $g++) {
					$query = $this->db->prepare("INSERT IGNORE INTO serials_contract (opportunity_detail_id,serials_contract_type,serials_contract_createdate,serials_contract_number,serials_contract_status,serials_contract_parent)
												 VALUE('" . $detalles[$i]["opportunity_detail_id"] . "','Contrato','" . date("Y-m-d H:i:s") . "','','0','". $sc_id_father ."')");
					$query->execute();
				}
            }
        }

        ///////////////////////////////

        $milestone_status = "0";
        $milestone_type = "notice";
        $milestone_date = explode("/", $milestone["milestone_date"]);
        $milestone_date = $milestone_date[2] . "-" . $milestone_date[1] . "-" . $milestone_date[0];

        $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date, milestone_start_time,
		 													milestone_end_time, milestone_type, milestone_notification, milestone_status,
															milestone_link, opportunity_id, user_id, client_id)
									 VALUES (:mtitle, :mdetail, :mdate, NULL, NULL, :mtype, 'N', :mstatus, NULL, :opid, :uid, :cid);");

        $query->bindParam(':mtitle', $milestone["milestone_title"]);
        $query->bindParam(':mdetail', $milestone["milestone_detail"]);
        $query->bindParam(':mdate', $milestone_date);
        $query->bindParam(':mtype', $milestone_type);
        $query->bindParam(':mstatus', $milestone_status);

        $query->bindParam(':cid', $milestone["mclient_id"]);
        $query->bindParam(':uid', $userId);
        $query->bindParam(':opid', $oportunity_id);

        $query->execute();

        ////////////////////////////////

        $this->log->info("Se creó la Oportunidad", "INSERT", "opportunity_id", $oportunity_id);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/activarOpportunity/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;
        $opportunity_status = "A";

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_status=:milestone_status WHERE opportunity_id=:id ");

        $query->bindParam(':milestone_status', $opportunity_status);
        $query->bindParam(':id', $oportunity_id);

        $query->execute();

        ///////////////////////////////

        $milestone_status = "0";
        $milestone_type = "notice";
        $milestone_title = "Activar";
        $milestone_detail = "Activar Oportunidad";
        $milestone_date = date("Y-m-d");

        $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date, milestone_start_time,
		 													milestone_end_time, milestone_type, milestone_notification, milestone_status,
															milestone_link, opportunity_id, user_id, client_id)
									 VALUES (:mtitle, :mdetail, :mdate, NULL, NULL, :mtype, 'N', :mstatus, NULL, :opid, :uid, :cid);");

        $query->bindParam(':mtitle', $milestone_title);
        $query->bindParam(':mdetail', $milestone_detail);
        $query->bindParam(':mdate', $milestone_date);
        $query->bindParam(':mtype', $milestone_type);
        $query->bindParam(':mstatus', $milestone_status);

        $query->bindParam(':cid', $milestone["mclientid"]);
        $query->bindParam(':uid', $userId);
        $query->bindParam(':opid', $oportunity_id);

        $query->execute();

        ////////////////////////////////

        $this->log->info("Se activó la oportunidad", "INSERT", "opportunity_id", $oportunity_id);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->post('/crearOportunidad', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;
        $status = "A";

        $w1 = "";
        $w1v = "";
        if ($milestone["opportunity_integrator_user_id_value"] != "") {
            $w1 = ",opportunity_integrator_user_id";
            $w1v = ",:opintegrator";
        }

        $w2 = "";
        $w2v = "";
        if ($milestone["opportunity_subclient_id_value"] != "") {
            $w2 = ",opportunity_subclient_id";
            $w2v = ",:opsubclient";
        }

        $query = $this->db->prepare("INSERT IGNORE INTO opportunity (client_id,opportunity_reason,opportunity_stage,opportunity_type,brand_id,user_id,opportunity_status" . $w1 . $w2 . ")
		                             VALUES (:clid,:opreason,:opstage,:optype,:bid,:uid,:opsta" . $w1v . $w2v . "); ");

        $query->bindParam(':clid', $milestone["client_id_value"]);
        $query->bindParam(':opreason', $milestone["opportunity_reason"]);
        $query->bindParam(':opstage', $milestone["opportunity_stage"]);
        $query->bindParam(':optype', $milestone["opportunity_type"]);
        $query->bindParam(':bid', $milestone["brand_id"]);
        $query->bindParam(':uid', $userId);
        $query->bindParam(':opsta', $status);

        if ($milestone["opportunity_integrator_user_id_value"] != "") {
            $query->bindParam(':opintegrator', $milestone["opportunity_integrator_user_id_value"]);
        }
        if ($milestone["opportunity_subclient_id_value"] != "") {
            $query->bindParam(':opsubclient', $milestone["opportunity_subclient_id_value"]);
        }

        $query->execute();
        $oportunity_id = $this->db->lastInsertId();

        $this->log->info("Se creó la Oportunidad", "INSERT", "opportunity_id", $oportunity_id);

        ////////////////

        $query = $this->db->prepare("INSERT IGNORE INTO opportunity_contacts (contact_id,opportunity_id)
		                             VALUES (:cid,:opid); ");

        $query->bindParam(':opid', $oportunity_id);
        $query->bindParam(':cid', $milestone["contact_id_value"]);

        $query->execute();


        $this->log->info("Se adiciono un contacto a la oportunidad", "INSERT", "contact_id", $milestone["contact_id_value"]);
        ////////////////////////////////


        return $response->withJson($oportunity_id);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/oportunidadEliminar/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_status='I' WHERE opportunity_id=:oid ");
        $query->bindParam(':oid', $milestone["opportunity_id"]);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se eliminó la oportunidad", "DELETE", "opportunity_id", $milestone["opportunity_id"]);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/oportunidadCambiarUsuario/', function ($request, $response, $args) {
        $post = $request->getParsedBody();

        $opportunity_id = $post['cuo_op_id'];
        $query = $this->db->prepare("UPDATE opportunity SET user_id=:uid WHERE opportunity_id=:oid ");
        $query->bindParam(':oid', $opportunity_id);
        $query->bindParam(':uid', $post["cuo_us_id"]);
        $query->execute();

        $tableOpportunityUser = $this->db->prepare("
            DELETE
                FROM opportunity_user
            WHERE opportunity_id = :opportunity_id;
        ");
        $tableOpportunityUser->bindValue(':opportunity_id', $opportunity_id);
        $tableOpportunityUser->execute();

        if (isset($post['add_user']) && !empty($post['add_user'])) {

            $tableBrandGroup = $this->db->prepare("
                INSERT
                    INTO opportunity_user
                (opportunity_id, user_id) VALUES (:opportunity_id, :user_id);
            ");
            $tableBrandGroup->bindValue(':opportunity_id', $opportunity_id);

            foreach ($post['add_user'] as $user_id) {
                $tableBrandGroup->bindValue(':user_id', $user_id);
                $tableBrandGroup->execute();

                $this->log->info("Se modificó la asociación de oportunidad - usuario", "INSERT", "user_id", $user_id);
            }
        }

        $tableUser = $this->db->prepare("
            SELECT user_id, CONCAT(user_name, ' ', user_lastname) AS user_fullname
                FROM users
            WHERE user_id = :user_id;
        ");
        $tableUser->bindValue(':user_id', $post["cuo_us_id"]);
        $tableUser->execute();

        $user = $tableUser->fetch(PDO::FETCH_ASSOC);

        $this->log->info(sprintf("Se actualizó el usuario principal %s a la oportunidad", $user['user_fullname']), "UPDATE", "opportunity_id", $post["cuo_op_id"]);

        return $response->withJson(['status' => 201, 'user' => $user]);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/oportunidadCambiarCliente', function ($request, $response, $args) {
        $post = $request->getParsedBody();

        $opportunity_id = $post['op_id'];
        $contact_id = $post['contact_id_value'];
        $client_id = $post['client_id_value'];
        $client_last_id = $post['client_last_id'];
        $client_last_businessname = $post['client_last_businessname'];

        $query = $this->db->prepare("UPDATE opportunity
          SET client_id = :cid
          WHERE opportunity_id = :oid ");
        $query->bindParam(':cid', $client_id);
        $query->bindParam(':oid', $opportunity_id);
        $query->execute();

        $tableOpportunityContacts = $this->db->prepare("
            DELETE
                FROM opportunity_contacts
            WHERE opportunity_id = :opportunity_id;
        ");
        $tableOpportunityContacts->bindValue(':opportunity_id', $opportunity_id);
        $tableOpportunityContacts->execute();

        $tableOpportunityContacts = $this->db->prepare("
            INSERT INTO opportunity_contacts
            (contact_id, opportunity_id) VALUES (:contact_id, :opportunity_id);
        ");
        $tableOpportunityContacts->bindValue(':contact_id', $contact_id);
        $tableOpportunityContacts->bindValue(':opportunity_id', $opportunity_id);
        $tableOpportunityContacts->execute();

        $tableClient = $this->db->prepare("
            SELECT client_id, client_businessname
                FROM client
            WHERE client_id = :client_id;
        ");
        $tableClient->bindValue(':client_id', $client_id);
        $tableClient->execute();

        $client = $tableClient->fetch(PDO::FETCH_ASSOC);

        $tableOpportunityContacts = $this->db->prepare("
            SELECT cc.contact_id, contact_email, contact_phone, contact_celphone,
              contact_type, opportunity_contact_id,
              CONCAT(contact_name, ' ', contact_lastname) AS contact_fullname
                FROM contact_client cc
                INNER JOIN opportunity_contacts oc ON oc.contact_id = cc.contact_id
            WHERE cc.contact_id = :contact_id AND opportunity_id = :opportunity_id;
        ");
        $tableOpportunityContacts->bindValue(':contact_id', $contact_id);
        $tableOpportunityContacts->bindValue(':opportunity_id', $opportunity_id);
        $tableOpportunityContacts->execute();

        $contact = $tableOpportunityContacts->fetch(PDO::FETCH_ASSOC);

        $milestone_title = "Nuevo cliente en oportunidad";
        $milestone_detail = "Se cambio el cliente de la oportunidad de $client_last_businessname a {$client['client_businessname']}.";
        $milestone_status = "0";
        $milestone_type = "notice";
        $milestone_date = date('Y-m-d');
        $userId = $this->session->user_id;

        $query = $this->db->prepare("INSERT IGNORE INTO milestone (
            milestone_title, milestone_detail, milestone_date, milestone_start_time,
  					milestone_end_time, milestone_type, milestone_notification, milestone_status,
  					milestone_link, opportunity_id, user_id, client_id
          ) VALUES (
            :mtitle, :mdetail, :mdate, NULL, NULL,
            :mtype, 'N', :mstatus, NULL, :opid, :uid, :cid
          );");
        $query->bindParam(':mtitle', $milestone_title);
        $query->bindParam(':mdetail', $milestone_detail);
        $query->bindParam(':mdate', $milestone_date);
        $query->bindParam(':mtype', $milestone_type);
        $query->bindParam(':mstatus', $milestone_status);
        $query->bindParam(':cid', $client_id);
        $query->bindParam(':uid', $userId);
        $query->bindParam(':opid', $opportunity_id);
        $query->execute();

        $milestone_id = $this->db->lastInsertId();

        $this->log->info(sprintf("Se actualizó al cliente %s", $client['client_businessname']), "UPDATE", "opportunity_id", $opportunity_id);

        return $response->withJson(['status' => 201, 'client' => $client, 'contact' => $contact, 'milestone_id' => $milestone_id]);
    })->setName('opportunity-change-client');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/oportunidadEliminarContacto/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("DELETE FROM opportunity_contacts WHERE opportunity_contact_id=:ocid");
        $query->bindParam(':ocid', $milestone["idcontactooportunidad"]);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se eliminó el contacto de la oportunidad", "DELETE", "opportunity_contact_id", $milestone["idcontactooportunidad"]);
        }
        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->post('/crearContacto/{id:[0-9]+}/{client:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];
        $contact_cliente_id = $args["client"];

        $milestone = $request->getParsedBody();
        $userId = $this->session->user_id;
        $contact_status = "1";

        if ($milestone["contact_id_value"] == "") {
            $query = $this->db->prepare("INSERT IGNORE INTO contact_client (contact_name, contact_lastname, contact_email,
																			contact_phone, contact_celphone, contact_status, contact_type,
																			client_id, user_id,contact_area)
										 VALUES (:cname,:clast,:cemail,:cphone,:ccphone,:cstatus,:ctype,:ccid,:userid,:carea); ");

            $query->bindParam(':cname', $milestone["contact_name"]);
            $query->bindParam(':clast', $milestone["contact_lastname"]);
            $query->bindParam(':cemail', $milestone["contact_email"]);
            $query->bindParam(':cphone', $milestone["contact_phone"]);
            $query->bindParam(':ccphone', $milestone["contact_celphone"]);
            $query->bindParam(':cstatus', $contact_status);
            $query->bindParam(':ctype', $milestone["contact_type"]);
            $query->bindParam(':carea', $milestone["contact_area"]);
            $query->bindParam(':ccid', $contact_cliente_id);
            $query->bindParam(':userid', $userId);

            $query->execute();
            $contact_cliente_id = $this->db->lastInsertId();
        } else {
            $contact_cliente_id = $milestone["contact_id_value"];
        }

        $query = $this->db->prepare("INSERT IGNORE INTO opportunity_contacts (contact_id,opportunity_id)
									 VALUES (:ccid,:copid); ");

        $query->bindParam(':copid', $oportunity_id);
        $query->bindParam(':ccid', $contact_cliente_id);
        $query->execute();

        $this->log->info("Se creó el contacto a la oportunidad", "INSERT", "contact_id", $contact_cliente_id);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->post('/crearDetalle/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];
        $milestone = $request->getParsedBody();
        $opportunity_detail_stock = "S";

        if (isset($milestone["opportunity_detail_type"]) &&  $milestone["opportunity_detail_type"]=="") {
            $milestone["opportunity_detail_type"] = 0;
        }

		if($milestone["opportunity_detail_type_hidden"]!=""){
			$milestone["opportunity_detail_type"]=$milestone["opportunity_detail_type_hidden"];
		}

        $opportunity_detail_renovation = "Nuevo";

        if ($milestone["opportunity_detail_enddate"] != "") {
            $fecha = explode("/", $milestone["opportunity_detail_enddate"]);
            $milestone["opportunity_detail_enddate"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];
        } else {
            $milestone["opportunity_detail_enddate"] = NULL;
        }

        $query = $this->db->prepare("INSERT IGNORE INTO opportunity_detail (opportunity_detail_description, opportunity_detail_productid, opportunity_detail_moneda, opportunity_detail_qty,
																		opportunity_detail_price, opportunity_detail_stock, opportunity_detail_serial, oportunity_id,opportunity_detail_detail,
																		opportunity_detail_type,opportunity_detail_father,opportunity_detail_renovation,opportunity_detail_enddate)
		                             VALUES (:opddescripcion,:opdproductid,:opdmoneda,:opdqty,:opdprice,:opdstock,:opdserial,:opid,:opdetail,:optype,:opfather,:oprenovation,:openddate); ");

        $query->bindParam(':opddescripcion', $milestone["opportunity_detail_description"]);
        $query->bindParam(':opdproductid', $milestone["opportunity_detail_productid"]);
        $query->bindParam(':opdmoneda', $milestone["opportunity_detail_moneda"]);
        $query->bindParam(':opdqty', $milestone["opportunity_detail_qty"]);
        $query->bindParam(':opdprice', $milestone["opportunity_detail_price"]);
        $query->bindParam(':opdstock', $opportunity_detail_stock);
        $query->bindParam(':opdserial', $milestone["opportunity_detail_serial"]);
        $query->bindParam(':opdetail', $milestone["opportunity_detail_detail"]);
        $query->bindParam(':optype', $milestone["opportunity_detail_type"]);
        $query->bindParam(':opfather', $milestone["opportunity_detail_father"]);
        $query->bindParam(':openddate', $milestone["opportunity_detail_enddate"]);
        $query->bindParam(':oprenovation', $opportunity_detail_renovation);
        $query->bindParam(':opid', $oportunity_id);
        $query->execute();

        $last_id = $this->db->lastInsertId();

        $this->log->info("Se creó el detalle", "INSERT", "opportunity_id", $last_id);

        ////////////////

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_total=(SELECT SUM(IF(opportunity_detail_productid='FTNT-COT',1,opportunity_detail_qty)*opportunity_detail_price) AS total FROM opportunity_detail WHERE oportunity_id=:opmid)
		                             WHERE opportunity_id=:opid");
        $query->bindParam(':opid', $oportunity_id);
        $query->bindParam(':opmid', $oportunity_id);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se actualizó el total", "UPDATE", "opportunity_id", $oportunity_id);
        }
        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/oportunidadEliminarDetalle/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("DELETE FROM opportunity_detail WHERE opportunity_detail_id=:opdid");
        $query->bindParam(':opdid', $milestone["opdid"]);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se eliminó el detalle", "DELETE", "opportunity_detail_id", $milestone["opdid"]);
        }
        ////////////////

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_total=(SELECT SUM(IF(opportunity_detail_productid='FTNT-COT',1,opportunity_detail_qty)*opportunity_detail_price) AS total FROM opportunity_detail WHERE oportunity_id=:opmid)
		                             WHERE opportunity_id=:opid");
        $query->bindParam(':opid', $milestone["opid"]);
        $query->bindParam(':opmid', $milestone["opid"]);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se actualizó el total", "UPDATE", "opportunity_id", $milestone["opid"]);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/editarDetalle/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $opportunity_detail_stock = "S";
        $milestone = $request->getParsedBody();

        if (isset($milestone["opportunity_detail_type"]) &&  $milestone["opportunity_detail_type"]=="") {
            $milestone["opportunity_detail_type"] = 0;
        }

		if($milestone["opportunity_detail_type_hidden"]!=""){
			$milestone["opportunity_detail_type"]=$milestone["opportunity_detail_type_hidden"];
		}

        if ($milestone["opportunity_detail_enddate"] != "") {
            $fecha = explode("/", $milestone["opportunity_detail_enddate"]);
            $milestone["opportunity_detail_enddate"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];
        } else {
            $milestone["opportunity_detail_enddate"] = NULL;
        }

		//opportunity_detail_type=:optype, SE elimino, porque se desactivo en la pantalla para la edicion
        $query = $this->db->prepare("UPDATE opportunity_detail SET
									     opportunity_detail_description=:opddescripcion, opportunity_detail_productid=:opdproductid,
										 opportunity_detail_moneda=:opdmoneda, opportunity_detail_qty=:opdqty,
										 opportunity_detail_price=:opdprice, opportunity_detail_stock=:opdstock,
										 opportunity_detail_serial=:opdserial,opportunity_detail_detail=:opdetail,
										 opportunity_detail_father=:opfather,
										 opportunity_detail_enddate=:openddate
								     WHERE opportunity_detail_id=:opportunity_detail_id ");

        $query->bindParam(':opddescripcion', $milestone["opportunity_detail_description"]);
        $query->bindParam(':opdproductid', $milestone["opportunity_detail_productid"]);
        $query->bindParam(':opdmoneda', $milestone["opportunity_detail_moneda"]);
        $query->bindParam(':opdqty', $milestone["opportunity_detail_qty"]);
        $query->bindParam(':opdprice', $milestone["opportunity_detail_price"]);
        $query->bindParam(':opdstock', $opportunity_detail_stock);
        $query->bindParam(':opdserial', $milestone["opportunity_detail_serial"]);
        $query->bindParam(':opdetail', $milestone["opportunity_detail_detail"]);
        //$query->bindParam(':optype', $milestone["opportunity_detail_type"]);
        $query->bindParam(':opfather', $milestone["opportunity_detail_father"]);
        $query->bindParam(':openddate', $milestone["opportunity_detail_enddate"]);

        $query->bindParam(':opportunity_detail_id', $milestone["opportunity_detail_id"]);

        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se actualizó el detalle", "UPDATE", "opportunity_detail_id", $milestone["opportunity_detail_id"]);
        }

        ////////////////

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_total=(SELECT SUM(IF(opportunity_detail_productid='FTNT-COT',1,opportunity_detail_qty)*opportunity_detail_price) AS total FROM opportunity_detail WHERE oportunity_id=:opmid)
		                             WHERE opportunity_id=:opid");
        $query->bindParam(':opid', $oportunity_id);
        $query->bindParam(':opmid', $oportunity_id);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se actualizó el total", "UPDATE", "opportunity_id", $oportunity_id);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->post('/crearMilestone/{id:[0-9]+}/{client:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];
        $client_id = $args["client"];
        $milestone = $request->getParsedBody();

        $milestone_notification = "N";
        $fecha = explode("/", $milestone["milestone_date"]);
        $milestone["milestone_date"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

        $userId = $this->session->user_id;

        //igualo los valores del formulario a la etapa de la oportinidad
        $stage_op = "";
        if ($milestone["milestone_stage"] == "start") {
            $stage_op = "Contacto inicial";
        }
        if ($milestone["milestone_stage"] == "evaluation") {
            $stage_op = "En evaluación";
        }
        if ($milestone["milestone_stage"] == "budget") {
            $stage_op = "Presupuestado";
        }
        if ($milestone["milestone_stage"] == "close") {
            $stage_op = "Cerrada";
        }

        if ($stage_op != $milestone["milestone_newstage"]) {

            $query = $this->db->prepare("UPDATE opportunity SET opportunity_stage=:opstage WHERE opportunity_id=:opid");
            $query->bindParam(':opstage', $stage_op);
            $query->bindParam(':opid', $oportunity_id);
            $query->execute();

            //Se crea un nuevo milestone notificando el cambio de etapa del usuario
            $new_title = "Nueva Etapa Oportunidad";
            $new_detail = "Se ha cambiado la etapa de la oportunidad: " . $stage_op . ".";
            $new_date = date("Y-m-d");
            $new_type = "notice";
            $new_status = "1";
            $new_link = "";

            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date,milestone_type,
											  milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id)
		                             VALUES (:opmtitle,:opmdetail,:opmdate,:opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid); ");

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $new_detail);
            $query->bindParam(':opmdate', $new_date);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);

            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se ha cambiado la etapa de la oportunidad: " . $stage_op . ".", "INSERT", "milestone_id", $last_id);
            }
        }

        /////////////////////////////////////////////////////////////////////

        $milestone_id_close_f = 0;

        if ($milestone["milestone_id_close_f"] != "") {
            $milestone_id_close_f = $milestone["milestone_id_close_f"];
        }

        /////////////////////////////////////////////////////////////////////

        $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date,milestone_type,
											  milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id, milestone_parent)
		                             VALUES (:opmtitle,:opmdetail,:opmdate,:opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid,:opmcloseid); ");

        $query->bindParam(':opmtitle', $milestone["milestone_title"]);
        $query->bindParam(':opmdetail', $milestone["milestone_detail"]);
        $query->bindParam(':opmdate', $milestone["milestone_date"]);
        $query->bindParam(':opmtype', $milestone["milestone_type"]);
        $query->bindParam(':opmnotification', $milestone_notification);
        $query->bindParam(':opmstatus', $milestone["milestone_status"]);
        $query->bindParam(':opmlink', $milestone["milestone_link"]);

        $query->bindParam(':opid', $oportunity_id);
        $query->bindParam(':uid', $userId);
        $query->bindParam(':cid', $client_id);

        $query->bindParam(':opmcloseid', $milestone_id_close_f);

        $query->execute();
        $last_id = $this->db->lastInsertId();

        if ($query->rowCount() > 0) {
            $this->log->info("Se creó un nuevo hito notificando el cambio de etapa del usuario", "INSERT", "milestone_id", $last_id);
        }

        if ($milestone_id_close_f != "0") {
            $query = $this->db->prepare("UPDATE milestone SET milestone_status=1,milestone_children=:last_id WHERE milestone_id=:opmcloseid");
            $query->bindParam(':last_id', $last_id);
            $query->bindParam(':opmcloseid', $milestone_id_close_f);
            $query->execute();
        }

        /////////////////////////////////////////////////////////////////////
        //siguiente paso calendario reunion
        if ($milestone["milestone_calendar_date_next"] != "" && $milestone["milestone_calendar_detail_next"] != "") {
            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date,milestone_type,
											  	milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id,
												milestone_start_time,milestone_end_time)
		                            	 VALUES (:opmtitle,:opmdetail,:opmdate,:opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid,:opmstarttime,:opmendtime); ");

            $new_title = "Reunión agendada";
            $new_type = "calendar";
            $new_status = "0";
            $new_link = "";

            $fecha = explode("/", $milestone["milestone_calendar_date_next"]);
            $milestone["milestone_calendar_date_next"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

            if ($milestone["milestone_calendar_time_start_next"] == "") {
                $milestone["milestone_calendar_time_start_next"] = NULL;
            }
            if ($milestone["milestone_calendar_time_end_next"] == "") {
                $milestone["milestone_calendar_time_end_next"] = NULL;
            }

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $milestone["milestone_calendar_detail_next"]);
            $query->bindParam(':opmdate', $milestone["milestone_calendar_date_next"]);
            $query->bindParam(':opmstarttime', $milestone["milestone_calendar_time_start_next"]);
            $query->bindParam(':opmendtime', $milestone["milestone_calendar_time_end_next"]);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);
            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se creó calendario de reunión", "INSERT", "milestone_id", $last_id);
            }
        }

        //siguiente paso llamada
        if ($milestone["milestone_call_date_next"] != "" && $milestone["milestone_call_detail_next"] != "") {
            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date,milestone_type,
											  	milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id,
												milestone_start_time,milestone_end_time)
		                            	 VALUES (:opmtitle,:opmdetail,:opmdate,:opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid,:opmstarttime,:opmendtime); ");

            $new_title = "Llamada agendada";
            $new_type = "call";
            $new_status = "0";
            $new_link = "";

            $fecha = explode("/", $milestone["milestone_call_date_next"]);
            $milestone["milestone_call_date_next"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

            if ($milestone["milestone_call_time_start_next"] == "") {
                $milestone["milestone_call_time_start_next"] = NULL;
            }
            if ($milestone["milestone_call_time_end_next"] == "") {
                $milestone["milestone_call_time_end_next"] = NULL;
            }

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $milestone["milestone_call_detail_next"]);
            $query->bindParam(':opmdate', $milestone["milestone_call_date_next"]);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmstarttime', $milestone["milestone_call_time_start_next"]);
            $query->bindParam(':opmendtime', $milestone["milestone_call_time_end_next"]);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);
            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se creó llamada", "INSERT", "milestone_id", $last_id);
            }
        }

        //siguiente paso seguimieto
        if ($milestone["milestone_follow_date_next"] != "" && $milestone["milestone_follow_detail_next"] != "") {
            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date,milestone_type,
											  	milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id)
		                            	 VALUES (:opmtitle,:opmdetail,:opmdate,:opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid); ");

            $new_title = "Seguimiento agendado";
            $new_type = "follow";
            $new_status = "0";
            $new_link = "";

            $fecha = explode("/", $milestone["milestone_follow_date_next"]);
            $milestone["milestone_follow_date_next"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $milestone["milestone_follow_detail_next"]);
            $query->bindParam(':opmdate', $milestone["milestone_follow_date_next"]);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);
            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se creó seguimiento", "INSERT", "milestone_id", $last_id);
            }
        }

        /////////////////////////////////////////////////////////////////////

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/oportunidadEliminarMilestone/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("DELETE FROM milestone WHERE milestone_id=:mid");
        $query->bindParam(':mid', $milestone["mid"]);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se eliminó hito", "DELETE", "milestone_id", $milestone["mid"]);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/oportunidadEstadoMilestone/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("UPDATE milestone SET milestone_status='1' WHERE milestone_id=:mid");
        $query->bindParam(':mid', $milestone["mid"]);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se modificó hito", "UPDATE", "milestone_id", $milestone["mid"]);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/editarMilestone/{id:[0-9]+}/{client:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];
        $client_id = $args["client"];
        $opportunity_detail_stock = "S";
        $milestone = $request->getParsedBody();

        $fecha = explode("/", $milestone["milestone_date"]);
        $milestone["milestone_date"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

        $userId = $this->session->user_id;
        $q = "";

        if ($milestone["milestone_link"] != "") {
            $q = "milestone_link=:opmlink,";
        }

        ////////////////////////////////////////////////////////////////////////////////
        //igualo los valores del formulario a la etapa de la oportinidad
        $stage_op = "";
        if ($milestone["milestone_stage"] == "start") {
            $stage_op = "Contacto inicial";
        }
        if ($milestone["milestone_stage"] == "evaluation") {
            $stage_op = "En evaluación";
        }
        if ($milestone["milestone_stage"] == "budget") {
            $stage_op = "Presupuestado";
        }
        if ($milestone["milestone_stage"] == "close") {
            $stage_op = "Cerrada";
        }

        if ($stage_op != $milestone["milestone_newstage"]) {

            $query = $this->db->prepare("UPDATE opportunity SET opportunity_stage=:opstage WHERE opportunity_id=:opid");
            $query->bindParam(':opstage', $stage_op);
            $query->bindParam(':opid', $oportunity_id);
            $query->execute();

            //Se crea un nuevo milestone notificando el cambio de etapa del usuario
            $new_title = "Nueva Etapa Oportunidad";
            $new_detail = "Se ha cambiado la etapa de la oportunidad: " . $stage_op . ".";
            $new_date = date("Y-m-d");
            $new_type = "notice";
            $new_status = "1";
            $new_link = "";

            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date,milestone_type,
											  milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id)
		                             VALUES (:opmtitle,:opmdetail,:opmdate,:opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid); ");

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $new_detail);
            $query->bindParam(':opmdate', $new_date);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);

            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se modificó la etapa de la oportunidad: $stage_op.", "INSERT", "milestone_id", $last_id);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////

        $query = $this->db->prepare("UPDATE milestone SET
                                        milestone_title=:opmtitle, milestone_detail=:opmdetail,
                                        milestone_date=:opmdate, milestone_start_time=:opmtimestart,
                                        milestone_end_time=:opmtimeend,
                                        milestone_type=:opmtype,
                                        milestone_status=:opmstatus, " . $q . "
                                        user_id=:uid
                                    WHERE milestone_id=:mid ");

        $query->bindParam(':opmtitle', $milestone["milestone_title"]);
        $query->bindParam(':opmdetail', $milestone["milestone_detail"]);
        $query->bindParam(':opmdate', $milestone["milestone_date"]);
        $query->bindParam(':opmtimestart', $milestone["milestone_time_start"]);
        $query->bindParam(':opmtimeend', $milestone["milestone_time_end"]);
        $query->bindParam(':opmtype', $milestone["milestone_type"]);
        $query->bindParam(':opmstatus', $milestone["milestone_status"]);

        if ($milestone["milestone_link"] != "") {
            $query->bindParam(':opmlink', $milestone["milestone_link"]);
        }

        $query->bindParam(':uid', $userId);
        $query->bindParam(':mid', $milestone["milestone_id"]);

        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se modificó hito", "UPDATE", "milestone_id", $milestone["milestone_id"]);
        }

        /////////////////////////////////////////////////////////////////////
        //siguiente paso calendario reunion
        if ($milestone["milestone_calendar_date_next"] != "" && $milestone["milestone_calendar_detail_next"] != "") {
            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date, milestone_start_time, milestone_end_time, milestone_type,
                                        milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id)
                                         VALUES (:opmtitle,:opmdetail,:opmdate, :opmstarttime, :opmendtime, :opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid); ");

            $new_title = "Reunión pendiente";
            $new_type = "calendar";
            $new_status = "0";
            $new_link = "";

            $fecha = explode("/", $milestone["milestone_calendar_date_next"]);
            $milestone["milestone_calendar_date_next"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $milestone["milestone_calendar_detail_next"]);
            $query->bindParam(':opmdate', $milestone["milestone_calendar_date_next"]);
            $query->bindParam(':opmstarttime', $milestone["milestone_calendar_time_start_next"]);
            $query->bindParam(':opmendtime', $milestone["milestone_calendar_time_end_next"]);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);
            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se creó calendario de reunión", "INSERT", "milestone_id", $last_id);
            }
        }

        //siguiente paso llamada
        if ($milestone["milestone_call_date_next"] != "" && $milestone["milestone_call_detail_next"] != "") {
            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date, milestone_start_time, milestone_end_time, milestone_type,
                                        milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id)
                                         VALUES (:opmtitle,:opmdetail,:opmdate, :opmstarttime, :opmendtime, :opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid); ");

            $new_title = "Llamada pendiente";
            $new_type = "call";
            $new_status = "0";
            $new_link = "";

            $fecha = explode("/", $milestone["milestone_call_date_next"]);
            $milestone["milestone_call_date_next"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $milestone["milestone_call_detail_next"]);
            $query->bindParam(':opmdate', $milestone["milestone_call_date_next"]);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmstarttime', $milestone["milestone_call_time_start_next"]);
            $query->bindParam(':opmendtime', $milestone["milestone_call_time_end_next"]);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);
            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se creó llamada", "INSERT", "milestone_id", $last_id);
            }
        }

        //siguiente paso seguimieto
        if ($milestone["milestone_follow_date_next"] != "" && $milestone["milestone_follow_detail_next"] != "") {
            $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date, milestone_start_time, milestone_end_time, milestone_type,
                                        milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id)
                                         VALUES (:opmtitle,:opmdetail,:opmdate, :opmstarttime, :opmendtime, :opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid); ");

            $new_title = "Seguimiento pendiente";
            $new_type = "follow";
            $new_status = "0";
            $new_link = "";

            $fecha = explode("/", $milestone["milestone_follow_date_next"]);
            $milestone["milestone_follow_date_next"] = $fecha[2] . "-" . $fecha[1] . "-" . $fecha[0];

            $query->bindParam(':opmtitle', $new_title);
            $query->bindParam(':opmdetail', $milestone["milestone_follow_detail_next"]);
            $query->bindParam(':opmdate', $milestone["milestone_follow_date_next"]);
            $query->bindParam(':opmstarttime', $milestone["milestone_call_time_start_next"]);
            $query->bindParam(':opmendtime', $milestone["milestone_call_time_end_next"]);
            $query->bindParam(':opmtype', $new_type);
            $query->bindParam(':opmnotification', $milestone_notification);
            $query->bindParam(':opmstatus', $new_status);
            $query->bindParam(':opmlink', $new_link);
            $query->bindParam(':opid', $oportunity_id);
            $query->bindParam(':uid', $userId);
            $query->bindParam(':cid', $client_id);

            $query->execute();

            if ($query->rowCount() > 0) {
                $last_id = $this->db->lastInsertId();
                $this->log->info("Se creó seguimiento", "INSERT", "milestone_id", $last_id);
            }
        }

        /////////////////////////////////////////////////////////////////////

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/eliminarIntegrador/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_integrator_user_id=NULL WHERE opportunity_id=:opid");
        $query->bindParam(':opid', $oportunity_id);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se modficó integrador", "UPDATE", "opportunity_id", $oportunity_id);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/crearIntegrador/{id:[0-9]+}/{client:[0-9]+}', function ($request, $response, $args) {
        $opid = $args["id"];
        $cid = $args["client"];

        $milestone = $request->getParsedBody();
        $userId = $this->session->user_id;
        $query = $this->db->prepare("UPDATE opportunity SET opportunity_integrator_user_id=:cid WHERE opportunity_id=:opid");
        $query->bindParam(':opid', $opid);
        $query->bindParam(':cid', $cid);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se modficó integrador", "UPDATE", "opportunity_id", $cid);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/eliminarSubcliente/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_subclient_id=NULL WHERE opportunity_id=:opid");
        $query->bindParam(':opid', $oportunity_id);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se modficó subcliente", "UPDATE", "opportunity_id", $oportunity_id);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/crearSubcliente/{id:[0-9]+}/{client:[0-9]+}', function ($request, $response, $args) {
        $opid = $args["id"];
        $cid = $args["client"];

        $milestone = $request->getParsedBody();
        $userId = $this->session->user_id;
        $query = $this->db->prepare("UPDATE opportunity SET opportunity_subclient_id=:cid WHERE opportunity_id=:opid");
        $query->bindParam(':opid', $opid);
        $query->bindParam(':cid', $cid);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se modficó subcliente", "UPDATE", "opportunity_id", $cid);
        }

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/migrarOportunidad', function ($request, $response, $args) {

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;
        $status = "A";

        $query = $this->db->prepare("SELECT oportunidad_activa, cliente, fecha_alta, origen, codigo_cliente, tipo, contacto, cargo, telefono, cel, mail,
		                                    producto, cantidad, presupuesto, deal_id, etapa, ultimo_contacto, Ultimo_realizado, Prox_contacto, a_realizar,
											atiende, estado, motivo
									 FROM oportunidad_activa WHERE oportunidad_activa='" . $milestone["oportunidad_activa"] . "'");
        $query->execute();

        $this->log->info("Inicia proceso de migrar oportunidad", "SELECT", "oportunidad_activa", $milestone["oportunidad_activa"]);

        $migracion = $query->fetchAll(PDO::FETCH_ASSOC);

        $migracion[0]["presupuesto"] = str_replace("$", "", $migracion[0]["presupuesto"]);
        $migracion[0]["presupuesto"] = str_replace(".", "", $migracion[0]["presupuesto"]);
        $migracion[0]["presupuesto"] = str_replace(",", ".", $migracion[0]["presupuesto"]);

        $comments = "Migración " . $milestone["oportunidad_activa"] . " // " . date("d-m-Y") . " // Último contacto " . $migracion[0]["ultimo_contacto"] . " // Próximo contacto " . $migracion[0]["Prox_contacto"] . " // A realizar " . $migracion[0]["a_realizar"];

        //////////////////////////////

        $opportunity_status = "A";

        //se crea la oportunidad
        $query = $this->db->prepare("INSERT IGNORE INTO opportunity (client_id,opportunity_reason,opportunity_stage,opportunity_type,brand_id,user_id,opportunity_status,opportunity_createdate,opportunity_comments)
		                             VALUES (:clid,:opreason,:opstage,:optype,:bid,:uid,:opsta,:falta,:opscomm); ");

        $query->bindParam(':clid', $milestone["client_id"]); //
        $query->bindParam(':opreason', $milestone["opportunity_reason"]);
        $query->bindParam(':opstage', $milestone["opportunity_stage"]);
        $query->bindParam(':optype', $milestone["opportunity_type"]);
        $query->bindParam(':bid', $milestone["brand_id"]); //
        $query->bindParam(':uid', $milestone["user_id"]); //
        $query->bindParam(':opsta', $opportunity_status);
        $query->bindParam(':falta', $milestone["fecha_alta"]); //
        $query->bindParam(':opscomm', $comments);
        $query->execute();

        $oportunity_id = $this->db->lastInsertId();

        if ($query->rowCount() > 0) {
            $this->log->info("Se creá oportunidad", "INSERT", "opportunity_id", $oportunity_id);
        }

        $query = $this->db->prepare("UPDATE oportunidad_activa SET opportunity_id='" . $oportunity_id . "' WHERE oportunidad_activa='" . $milestone["oportunidad_activa"] . "'");
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se modificó oportunidad activa", "UPDATE", "oportunidad_activa", $milestone["oportunidad_activa"]);
        }

        ////////////////////////////////////////////////////////////
        //buscamos si existe el conctado por email, sino se registra y se asigna a la solicitud
        $query = $this->db->prepare("SELECT contact_id FROM contact_client WHERE contact_email	='" . $migracion[0]["mail"] . "'");
        $query->execute();
        $contacto = $query->fetchAll(PDO::FETCH_ASSOC);

        $contact_id = "";

        if (count($contacto) > 0) {
            $contact_id = $contacto[0]["contact_id"];
            $this->log->info("Se encuentra contacto por email", "SELECT", "contact_id", $contact_id);
        } else {
            $query = $this->db->prepare("INSERT INTO contact_client(contact_name, contact_lastname, contact_email, contact_phone, contact_celphone, contact_status, contact_type, contact_area, client_id, user_id)
			                             VALUES ('" . $migracion[0]["contacto"] . "','','" . $migracion[0]["mail"] . "','" . $migracion[0]["telefono"] . "','" . $migracion[0]["cel"] . "',1,'','','" . $milestone["client_id"] . "','" . $userId . "')");
            $query->execute();

            $contact_id = $this->db->lastInsertId();

            $this->log->info("Se creó contacto", "INSERT", "contact_id", $contact_id);
        }

        $query = $this->db->prepare("INSERT INTO opportunity_contacts(contact_id, opportunity_id) VALUES ('" . $contact_id . "','" . $oportunity_id . "')");
        $query->execute();

        ////////////////////////
        //Se busca el producto seleccionado para registrar la informacion
        $query = $this->db->prepare("SELECT product_id, brand_id, product_status, comments, identifier, family, product, type, item, sku, description1, description2, price, category, translate_price, upc_code, fed, gsa, coo
		                             FROM products WHERE product_id='" . $milestone["product_id"] . "'");
        $query->execute();
        $producto = $query->fetchAll(PDO::FETCH_ASSOC);

        $this->log->info("Se busca el producto seleccionado para registrar la información", "SELECT", "product_id", $milestone["product_id"]);

        $query = $this->db->prepare("INSERT IGNORE INTO opportunity_detail(opportunity_detail_description, opportunity_detail_detail, opportunity_detail_productid, opportunity_detail_moneda,
		                                                            opportunity_detail_qty, opportunity_detail_price, opportunity_detail_stock, opportunity_detail_serial, opportunity_detail_type,
																	opportunity_detail_father, oportunity_id, opportunity_detail_renovation)
		                             VALUES ('" . $producto[0]["item"] . "','" . $producto[0]["description1"] . "','" . $producto[0]["sku"] . "','usd$','" . $migracion[0]["cantidad"] . "','" . $migracion[0]["presupuesto"] . "',
									         'S','','" . $producto[0]["type"] . "',0,'" . $oportunity_id . "','Nuevo')");
        $query->execute();

        if ($query->rowCount() > 0) {
            $last_id = $this->db->lastInsertId();
            $this->log->info("Se creó el detalle de oportunidad", "INSERT", "opportunity_detail", $last_id);
        }
        ///////////////////////////
        //Se crea un nuevo milestone
        $new_title = "Histórico migración";
        $new_detail = $migracion[0]["Ultimo_realizado"];
        $new_date = date("Y-m-d");
        $new_type = "follow";
        $new_status = "1";
        $new_link = "";
        $milestone_notification = "N";

        $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date,milestone_type,
										  milestone_notification, milestone_status, milestone_link, opportunity_id, user_id, client_id)
								 VALUES (:opmtitle,:opmdetail,:opmdate,:opmtype,:opmnotification,:opmstatus,:opmlink,:opid,:uid,:cid); ");

        $query->bindParam(':opmtitle', $new_title);
        $query->bindParam(':opmdetail', $new_detail);
        $query->bindParam(':opmdate', $new_date);
        $query->bindParam(':opmtype', $new_type);
        $query->bindParam(':opmnotification', $milestone_notification);
        $query->bindParam(':opmstatus', $new_status);
        $query->bindParam(':opmlink', $new_link);

        $query->bindParam(':opid', $oportunity_id);
        $query->bindParam(':uid', $milestone["user_id"]);
        $query->bindParam(':cid', $milestone["client_id"]);

        $query->execute();

        if ($query->rowCount() > 0) {
            $last_id = $this->db->lastInsertId();
            $this->log->info("Se creó nuevo hito", "INSERT", "milestone_id", $last_id);
        }


        return $response->withJson($oportunity_id);
    });
});
