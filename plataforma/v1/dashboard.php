<?php

$app->get('/dashboard', function ($request, $response, $args) {

    $me = $this->session->get('user_id', 0);
    $dateTimeTomorrow = new DateTime('tomorrow');
    $dateTime = new DateTime('now');
    $date = $dateTime->format('Y-m-d');
    $milestone = new \Models\PDO\Milestone($this->db);

    //Obtengo tareas
    $task = $milestone->getTask($me, $dateTimeTomorrow->format('Y-m-d'), "LAST");
    $taskLast = $task;
    
    if (count($task) < 3) {
        $task += $milestone->getTask($me, $dateTimeTomorrow->format('Y-m-d'));
    }
    if (count($task) < 4) {
        $task += $milestone->getTask($me, $dateTimeTomorrow->format('Y-m-d'), "TOMORROW");
    }
    //Obtengo cantidad de tareas last
    $last = $milestone->getTaskCount($me, $date, "LAST");
    //Obtengo cantidad de tareas now
    $nows = $milestone->getTaskCount($me, $date);
    
    
    /*
     * Start task expired and current counts
     */
    $subtitleLast = 0;    
    
    if (count($last) > 0) {
        $dateLast = new DateTime(end($last)['milestone_date']);

        $diffLast = $dateTime->diff($dateLast)->format("%a");
        $subtitleLast = "$diffLast día" . (($diffLast > 1) ? "s" : "") . " de atraso";
    }
    
    $taskExpired = [
        "color" => "red", 
        "title" => "Tareas Vencidas", 
        "number" => $last[0]["number"], 
        "subtitle" => $subtitleLast, 
        "href" => $this->router->pathFor("task-list"), 
        "namelink" => "Ver tareas"
    ];
    
    $taskCurrent = [
        "color" => "indigo", 
        "title" => "Tareas del día", 
        "number" => $nows[0]["number"], 
        "subtitle" => ($nows[0]["number"] > 0) ? "Manos a la obra" : "Excelente", 
        "href" => $this->router->pathFor("task-list"), 
        "namelink" => "Ver tareas"
    ];
    /*
     * End expired and current counts
     */
    
    /*
     * Start Opportunity and renovations counts
     */
    $opportunity = new \Models\PDO\Opportunity($this->db, "A");
    $customerCount = $opportunity->getCountCustomer();
    $container_opportunity = [
        "color" => "green",
        "title" => "Oportunidades del mes",
        "number" => $opportunity->getCountAvailable(), 
        "subtitle" => sprintf("%s cliente%s", $customerCount, ($customerCount > 1) ? "s diferentes" : ""), 
        "href" => $this->router->pathFor("list-opportunity", ["new" => "ok"]), 
        "namelink" => "Ver oportunidades"
    ];
    $opportunity->setType("R");
    $customerCount = $opportunity->getCountCustomer();
    
    $container_renovation_course = [
        "color" => "orange", 
        "title" => "Renovaciones en curso", 
        "number" => $opportunity->getCountAvailable(), 
        "subtitle" => sprintf("%s cliente%s", $customerCount, ($customerCount > 1) ? "s diferentes" : ""), 
        "href" => $this->router->pathFor("renewal-list"), 
        "namelink" => "Ver renovaciones"
    ];
    /*
     * End Opportunity and renovations counts
     */

    $tableSerials = $this->db->prepare("
        SELECT 
            serials_contract_type, serials_contract_number, 
            opportunity_detail_description, opportunity_detail_price, 
            opportunity_detail_productid, opportunity_salenumber, opportunity_id, 
            client.client_id, client.client_businessname,serials_contract_enddate,opportunity_detail_moneda
        FROM `serials_contract`
         INNER JOIN opportunity_detail ON opportunity_detail.opportunity_detail_id = serials_contract.opportunity_detail_id
         INNER JOIN opportunity ON opportunity.opportunity_id = opportunity_detail.oportunity_id 
         INNER JOIN client ON client.client_id = opportunity.client_id 
        WHERE opportunity_status = 'V' AND serials_contract_enddate IS NOT NULL
        ORDER BY serials_contract_enddate DESC LIMIT 5;
    ");
    $tableSerials->execute();
    $expiration_date = $tableSerials->fetchAll(PDO::FETCH_ASSOC);
    
    //////////////////////
    $query = $this->db->prepare("
            SELECT log_timestamp, UPPER(LEFT(log_detail , 1)) AS inicial, 
                log_detail, log_value 
            FROM log 
            WHERE log_action != 'SELECT'
                ORDER BY log_timestamp DESC LIMIT 0,7
    ");
    $query->execute();
    $last_update = $query->fetchAll(PDO::FETCH_ASSOC);

    //////////////////////


    //////////////////////

    $query = $this->db->prepare("
            SELECT log_timestamp, UPPER(LEFT(b.client_businessname , 1)) AS inicial, 
              b.client_businessname, b.client_id, 
              CONCAT(c.user_name,' ',c.user_lastname) AS nombre 
            FROM log a, client b, users c  
                WHERE a.log_key = 'client_id' AND log_value=b.client_id AND a.user_id = c.user_id 
                AND a.log_action != 'SELECT' 
            ORDER BY log_timestamp DESC LIMIT 0,7;
    ");
    $query->execute();
    $last_clients = $query->fetchAll(PDO::FETCH_ASSOC);

    
    //////////////////////
    //Calendario
    $calendar = new \Models\PDO\Calendar($this->db);
    $calendarJSON = $calendar->get($me);
    $calendarParse = $calendar->parseJsonCalendar($calendarJSON);
    
    $this->log->info("Se ingresó al dashboard", "SELECT", "", 0);

    return $this->view->render($response, 'dashboard.twig', [
                'counts' => array(
                    $taskExpired,
                    $taskCurrent,
                    $container_opportunity,
                    $container_renovation_course
                ),
                'task' => $task,
                'calendar' => $calendarParse,
                'last_update' => $last_update,
                'last_clients' => $last_clients,
                 'expiration_date' => $expiration_date
    ]);
})->setName('dashboard');
