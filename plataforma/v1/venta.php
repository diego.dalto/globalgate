<?php

$app->group('/venta', function () {

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        $opportunity_id = $args["id"];

        $sql = "SELECT opportunity_id, opportunity_salenumber, opportunity_type, opportunity_stage, opportunity_status, opportunity_createdate,
						opportunity_reason, opportunity_subclient_id, opportunity_integrator_user_id, opportunity_heritage, opportunity_conversion_date,
						opportunity_dolar, opportunity_total, opportunity_iddealregistrado, opportunity_currentexpiration, opportunity_comments,
						a.brand_id, a.user_id, a.client_id,
						CONCAT(user_name, ' ', user_lastname) AS user_namefull, client_businessname, brand_name
				FROM opportunity a, users b, client c, product_brand e
				WHERE a.user_id=b.user_id AND a.client_id=c.client_id AND a.brand_id=e.brand_id AND opportunity_id='" . $opportunity_id . "'";

        $query = $this->db->prepare($sql);

        $query->execute();
        $oportunidad = $query->fetchAll(PDO::FETCH_ASSOC);

        ////////////

        $query = $this->db->prepare("SELECT a.contact_id, contat_createdate, contact_name, contact_lastname, contact_email, contact_phone, contact_celphone,
											contact_status, contact_type, contact_area, client_id, user_id, b.opportunity_contact_id
									 FROM contact_client a, opportunity_contacts b
									 WHERE a.contact_id=b.contact_id AND b.opportunity_id='" . $opportunity_id . "'");

        $query->execute();
        $contactos = $query->fetchAll(PDO::FETCH_ASSOC);

        ////////////

        $sql = "SELECT
				c.client_businessname,o.opportunity_createdate, o.opportunity_total, u.user_name, u.user_lastname,
				od.opportunity_detail_description, od.opportunity_detail_qty,
				od.opportunity_detail_moneda, od.opportunity_detail_price, od.opportunity_detail_createdate,
        od.opportunity_detail_productid,
				s.serials_contract_type, (od.opportunity_detail_qty*od.opportunity_detail_price) AS total_price, s.serials_contract_number,
				s.serials_contract_date, s.serials_contract_status, s.serials_contract_enddate, s.serials_contract_limitdate,
				CONCAT(u.user_name, ' ', u.user_lastname) AS user_namefull,o.brand_id,
				o.opportunity_id, od.opportunity_detail_id, od.opportunity_detail_father, s.serials_contract_id, s.serials_contract_parent
			FROM
				opportunity o
				INNER JOIN client c ON c.client_id = o.client_id
				INNER JOIN users u ON u.user_id = o.user_id
				INNER JOIN opportunity_detail od ON od.oportunity_id = o.opportunity_id
				LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id
			WHERE
				o.opportunity_status='V' AND
				o.opportunity_id='" . $opportunity_id . "'
			ORDER BY
				CAST(
					CONCAT(	(CASE WHEN (s.serials_contract_parent='0') THEN s.serials_contract_id ELSE s.serials_contract_parent END),
							s.serials_contract_id
					)
				AS UNSIGNED )
			ASC";

        $query = $this->db->prepare($sql);
        $query->execute();
        $sales = $query->fetchAll(PDO::FETCH_ASSOC);

        $products = array();
        $rules = array();
        $seriales = array();
        $childrens = array();
        $contracts = array();
        $contracts_father = array();

        $od = "";

        foreach ($sales as $s => $v) {
            $id_o = $v['opportunity_id'];
            $id_d = $v['opportunity_detail_id'];
            $id_s = $v['serials_contract_id'];

            if ($v['opportunity_detail_father'] == 0) {
                if ($od != $v['opportunity_detail_id']) {
                    $od = $v['opportunity_detail_id'];
                    $seriales = array();
                }

                $products[$id_d] = array(
                    'id_detail' => $v['opportunity_detail_id'],
                    'currency' => $v['opportunity_detail_moneda'],
                    'product' => $v['opportunity_detail_description'],
                    'qty' => $v['opportunity_detail_qty'],
                    'price' => $v['opportunity_detail_price'],
                    'sku' => $v['opportunity_detail_productid'],
                    'total_price' => $v['total_price'],
                    'scid' => $v['serials_contract_id'],
                    'date' => substr($v['opportunity_detail_createdate'], 0, 10)
                );

                ////////////////////////////////////

                $sql = "SELECT `product_rules_id`, `rules_key`, `sku` FROM `product_rules` WHERE sku=:skuproduct";
                $query = $this->db->prepare($sql);
                $query->bindParam(':skuproduct', $v['opportunity_detail_productid']);
                $query->execute();
                $rules_sku = $query->fetchAll(PDO::FETCH_ASSOC);

                $rules[$v['opportunity_detail_productid']]=array();

                foreach ($rules_sku as $row => $value) {
                    array_push($rules[$v['opportunity_detail_productid']], $value["rules_key"]);
                }

                ////////////////////////////////////

                if ($v['serials_contract_type'] == "Serial") {
                    if ($v['serials_contract_date'] != "") {
                        $date = explode("-", $v['serials_contract_date']);
                        $v['serials_contract_date'] = $date[2] . "/" . $date[1] . "/" . $date[0];
                    }

                    if ($v['serials_contract_enddate'] != "") {
                        $date = explode("-", $v['serials_contract_enddate']);
                        $v['serials_contract_enddate'] = $date[2] . "/" . $date[1] . "/" . $date[0];
                    }

                    if ($v['serials_contract_limitdate'] != "") {
                        $date = explode("-", $v['serials_contract_limitdate']);
                        $v['serials_contract_limitdate'] = $date[2] . "/" . $date[1] . "/" . $date[0];
                    }

                    /////////////////
                    $serials_history = [];

                    if (!empty($v['serials_contract_id'])) {
                        $query = $this->db->prepare("SELECT *
                                 FROM serial_history
                                 WHERE serials_contract_id = :id AND serial_history_number != ''");
                        $query->bindValue(':id', $v['serials_contract_id']);
                        $query->execute();
                        $serials_history = $query->fetchAll(PDO::FETCH_ASSOC);
                    }

                    $serial = array(
                        'type' => $v['serials_contract_type'],
                        'number' => $v['serials_contract_number'],
                        'date' => $v['serials_contract_date'],
                        'enddate' => $v['serials_contract_enddate'],
                        'limitdate' => $v['serials_contract_limitdate'],
                        'status' => $v['serials_contract_status'],
                        'parent' => $v['serials_contract_parent'],
                        'scid' => $v['serials_contract_id'],
                        'history' => $serials_history
                    );

                    array_push($seriales, $serial);
                } else {
                    $contracts_f = array(
                        'type' => $v['serials_contract_type'],
                        'number' => $v['serials_contract_number'],
                        'date' => $v['serials_contract_date'],
                        'enddate' => $v['serials_contract_enddate'],
                        'limitdate' => $v['serials_contract_limitdate'],
                        'status' => $v['serials_contract_status'],
                        'odf' => $v['opportunity_detail_father'],
                        'scfid' => $v['serials_contract_parent'],
                        'scid' => $v['serials_contract_id']
                    );

                    array_push($contracts_father, $contracts_f);
                }

                $products[$id_d]["serials"] = $seriales;
                $products[$id_d]["contracts_father"] = $contracts_father;
            } else {
                if ($v['serials_contract_type'] == "Serial") {
                    $children = array(
                        'id_detail' => $v['opportunity_detail_id'],
                        'currency' => $v['opportunity_detail_moneda'],
                        'product' => $v['opportunity_detail_description'],
                        'qty' => $v['opportunity_detail_qty'],
                        'price' => $v['opportunity_detail_price'],
                        'total_price' => $v['total_price'],
                        'father' => $v['opportunity_detail_father'],
                        'number' => $v['serials_contract_number'],
                        'scid' => $v['serials_contract_id'],
                        'scfid' => $v['serials_contract_parent'],
                        'sku' => $v['opportunity_detail_productid'],
                        'brand_id' => $v['brand_id'],
                        'date' => substr($v['opportunity_detail_createdate'], 0, 10)
                    );

                    array_push($childrens, $children);
                } else {
                    $contract = array(
                        'type' => $v['serials_contract_type'],
                        'number' => $v['serials_contract_number'],
                        'date' => $v['serials_contract_date'],
                        'enddate' => $v['serials_contract_enddate'],
                        'limitdate' => $v['serials_contract_limitdate'],
                        'status' => $v['serials_contract_status'],
                        'odf' => $v['opportunity_detail_father'],
                        'scfid' => $v['serials_contract_parent'],
                        'scid' => $v['serials_contract_id']
                    );

                    array_push($contracts, $contract);
                }
            }
        }

        ////////////
        $this->log->info("Ver editar ventas", "SELECT", "opportunity_id", $opportunity_id);

        return $this->view->render($response, 'sale_edit.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'action' => 'EditarOportunidades',
                    'opportunity' => $oportunidad[0],
                    'contacts' => $contactos,
                    'rules' => $rules,
                    'products' => $products,
                    'childrens' => $childrens,
                    'contracts' => $contracts,
                    'user_id' => $this->session->user_id
        ]);
    })->setName('sale-edit');

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->get('/listado', function ($request, $response, $args) {
        $data = $request->getParsedBody();
        $where = '';
        $join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';

        if (isset($data)) {
            if (isset($data['pagina']) && $data['pagina'] != "") {
            } else {
                $data['pagina']=1;
            }

            ////////////////

            if (isset($data['countResults']) && $data['countResults'] != "") {
            } else {
                $data['countResults']=10;
            }
        } else {
            $data['countResults']=10;
            $data['pagina']=1;
        }

        if (isset($data)) {//válido que lleguen parámetros
            if (isset($data['clientes']) && $data['clientes'] != "") {
                $cliente = trim($data['clientes']);
                $where .= 'AND c.client_businessname like "%' . $cliente . '%" ';
            }
            if (isset($data['serialcontrato']) && $data['serialcontrato'] != "") {
                $serial = trim($data['serialcontrato']);
                $where .= 'AND s.serials_contract_number like "%' . $serial . '%" ';
            }
            /* if (isset($data['estado']) && $data['estado'] != "") {
              $status = $data['estado'];
              $where .= 'AND o.opportunity_status = "' . $status . '" ';
              } */
            if (isset($data['marcas']) && $data['marcas'] != "") {
                $marca = trim($data['marcas']);
                $where .= 'AND o.brand_id = "' . $marca . '" ';
            }

            if (isset($data['numeroventa']) && $data['numeroventa'] != "") {
                $numeroventa = trim($data['numeroventa']);
                if (isset($numeroventa) && $numeroventa != "") {
                    $marca = $numeroventa;
                    $where .= "AND o.opportunity_salenumber LIKE '%" . $marca . "%'";
                }
            }

            if (isset($data['fechadesde']) && $data['fechadesde'] != "") {
                if (isset($data['fechahasta']) && $data['fechahasta'] != "") {
                    $fecha = explode('/', trim($data['fechadesde']));
                    $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                    $fecha2 = explode('/', trim($data['fechahasta']));
                    $date2 = $fecha2[2] . '-' . $fecha2[1] . '-' . $fecha2[0];
                    $where .= 'AND s.serials_contract_date BETWEEN "' . $date . '" AND "' . $date2 . '" ';
                } else {
                    $fecha = explode('/', trim($data['fechadesde']));
                    $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                    $where .= 'AND s.serials_contract_date >= "' . $date . '" ';
                }
            }
            if (isset($data['fechahasta']) && $data['fechahasta'] != "") {
                if (isset($data['fechadesde']) && $data['fechadesde'] != "") {
                    $fecha = explode('/', trim($data['fechadesde']));
                    $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                    $fecha2 = explode('/', trim($data['fechahasta']));
                    $date2 = $fecha2[2] . '-' . $fecha2[1] . '-' . $fecha2[0];
                    $where .= 'AND s.serials_contract_date BETWEEN "' . $date . '" AND "' . $date2 . '" ';
                } else {
                    $fecha = explode('/', trim($data['fechahasta']));
                    $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                    $where .= 'AND s.serials_contract_date <= "' . $date . '" ';
                }
            }

            //////


            if (isset($data['serialescontratos']) && $data['serialescontratos'] != "") {
                /*$sc = trim($data['serialescontratos']);
                switch ($sc) {
                    case 'cs':
                        $join = 'RIGHT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
                        $where .= 'AND s.serials_contract_type = "Serial" ';
                        break;
                    case 'cc':
                        $join = 'RIGHT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
                        $where .= 'AND s.serials_contract_type = "Contrato" ';
                        break;
                    case 'csc':
                        $join = 'RIGHT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
                        $where .= 'AND (s.serials_contract_type = "Contrato" OR s.serials_contract_type = "Serial") ';
                        break;
                    case 'ss':
                        $join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
                        $where .= 'AND s.serials_contract_type = "Contrato" ';
                        break;
                    case 'sc':
                        $join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
                        $where .= 'AND s.serials_contract_type = "Serial" ';
                        break;
                    case 'ssc':
                        $join = 'LEFT JOIN serials_contract s ON od.opportunity_detail_id = s.opportunity_detail_id';
                        $where .= 'AND s.serials_contract_type <> "Contrato" AND s.serials_contract_type <> "Serial" ';
                        break;
                }*/

                $status = trim($data['serialescontratos']);
                //$where .= "AND s.serials_contract_status = $status ";

				if($status=="0"){
					$where .= " AND (SELECT COUNT(zz.opportunity_detail_id) FROM serials_contract zz WHERE zz.opportunity_detail_id=od.opportunity_detail_id AND zz.serials_contract_status=0)>0  ";
				}else{
					$where .= " AND (SELECT COUNT(zz.opportunity_detail_id) FROM serials_contract zz WHERE zz.opportunity_detail_id IN (SELECT yy.opportunity_detail_id FROM opportunity_detail yy WHERE yy.oportunity_id=o.opportunity_id) AND  zz.serials_contract_status=1)=(SELECT COUNT(zz.opportunity_detail_id) FROM serials_contract zz WHERE zz.opportunity_detail_id IN (SELECT yy.opportunity_detail_id FROM opportunity_detail yy WHERE yy.oportunity_id=o.opportunity_id)) ";
				}
            }

            //////

            if (isset($data['usuarios']) && $data['usuarios'] != "") {
                $usuario = trim($data['usuarios']);
                $where .= " AND CONCAT(u.user_name, ' ', u.user_lastname) LIKE '%" . $usuario . "%' ";
            }
        }

        //////

        $sql="SELECT DISTINCT o.opportunity_id
            FROM opportunity o
            INNER JOIN client c ON c.client_id = o.client_id
		        INNER JOIN product_brand br ON o.brand_id = br.brand_id
            INNER JOIN users u ON u.user_id = o.user_id
            INNER JOIN opportunity_detail od ON od.oportunity_id = o.opportunity_id
			      $join
			WHERE o.opportunity_status='V' $where
            ORDER BY CAST(o.opportunity_salenumber AS SIGNED INTEGER)  DESC LIMIT ".($data["countResults"]*($data["pagina"]-1)).", ".($data["countResults"])."";

	   $query = $this->db->prepare($sql);
        $query->execute();
        $sales = $query->fetchAll(PDO::FETCH_ASSOC);

        $all = array();
        foreach ($sales as $s => $v) {
            array_push($all, $v['opportunity_id']);
        }

        $data["totalItems"]=count($all);

        if (count($all)>0) {
            $sql="SELECT c.client_businessname,o.opportunity_id, o.opportunity_createdate, o.opportunity_total,
				u.user_name, u.user_lastname, od.opportunity_detail_id, od.opportunity_detail_description,
				od.opportunity_detail_qty, od.opportunity_detail_moneda, od.opportunity_detail_price, s.serials_contract_id,
				od.opportunity_detail_createdate, s.serials_contract_type,od.opportunity_detail_productid,
				s.serials_contract_number, s.serials_contract_date, s.serials_contract_status, o.opportunity_id, s.serials_contract_enddate,
				s.serials_contract_limitdate, CONCAT(u.user_name, ' ', u.user_lastname) AS user_namefull,
				o.opportunity_salenumber, br.brand_name, od.opportunity_detail_type, od.opportunity_detail_father, o.brand_id
				FROM opportunity o
				INNER JOIN client c ON c.client_id = o.client_id
				INNER JOIN product_brand br ON o.brand_id = br.brand_id
				INNER JOIN users u ON u.user_id = o.user_id
				INNER JOIN opportunity_detail od ON od.oportunity_id = o.opportunity_id
				" . $join . "
				WHERE o.opportunity_id IN (".implode(",", $all).")
				ORDER BY CAST(o.opportunity_salenumber AS SIGNED INTEGER)  DESC ;";
            $query = $this->db->prepare($sql);
            $query->execute();
            $sales = $query->fetchAll(PDO::FETCH_ASSOC);
			
        }
        ////////
        //Saco listado de marcas
        $query = $this->db->prepare("SELECT brand_id, brand_name
									 FROM product_brand");
        $query->execute();
        $brands = $query->fetchAll(PDO::FETCH_ASSOC);

        ////////

        $order_sales = array();
        //$rules = array();
        $j = 1;
        $d = 0;

        foreach ($sales as $s => $v) {
            $id_o = $v['opportunity_id'];
            $id_d = $v['opportunity_detail_id'];
            $id_s = $v['serials_contract_id'];

            $order_sales[$id_o]['opportunity_id'] = $v['opportunity_id'];
            $order_sales[$id_o]['opportunity_client'] = $v['client_businessname'];
            $order_sales[$id_o]['opportunity_user'] = $v['user_name'] . ' ' . $v['user_lastname'];
            $order_sales[$id_o]['opportunity_date'] = substr($v['opportunity_createdate'], 0, 10);
            $order_sales[$id_o]['opportunity_total'] = $v['opportunity_total'];
            $order_sales[$id_o]['brand_name'] = $v['brand_name'];
            $order_sales[$id_o]['brand_id'] = $v['brand_id'];
            $order_sales[$id_o]['opportunity_salenumber'] = $v['opportunity_salenumber'];


            ////////////////////////////////////


            /*$sql = "SELECT `product_rules_id`, `rules_key`, `sku` FROM `product_rules` WHERE sku=:skuproduct";
            $query = $this->db->prepare($sql);
            $query->bindParam(':skuproduct', $v['opportunity_detail_productid']);
            $query->execute();
            $rules_sku = $query->fetchAll(PDO::FETCH_ASSOC);

            $rules[$v['opportunity_detail_productid']]=array();

            $estadoFinal = "PENDIENTE";

            $estadoSerial = true;
            $estadoContrato = true;
            $estadoAdmin = true;

            $infoReglas = "";*/

            /*foreach ($rules_sku as $row => $value) {
                if($value["rules_key"]=="REQUIRE_SERIAL_CONTRACT"){
                    if($v['opportunity_detail_father']=="" || $v['opportunity_detail_father']=="0"){
                        if($v['serials_contract_type']=="Serial" && $v['serials_contract_number']==""){
                            $estadoSerial = false;
                        }
                    }else{
                        if($v['serials_contract_type']=="Contrato" && $v['serials_contract_number']==""){
                            $estadoSerial = false;
                        }
                    }
                    $infoReglas .= "/RSC/";
                }

                if($value["rules_key"]=="REQUIRE_DEAD_LINE"){
                    if($v['opportunity_detail_father']=="" || $v['opportunity_detail_father']=="0"){
                        if($v['serials_contract_type']=="Serial" && $v['serials_contract_enddate']==""){
                            $estadoSerial = false;
                        }
                    }else{
                        if($v['serials_contract_type']=="Contrato" && $v['opportunity_detail_father']==""){
                            $estadoSerial = false;
                        }
                    }
                    $infoReglas .= "/RFV/";
                }

                if($value["rules_key"]=="REQUIRE_REGISTER_LIMIT"){
                    if($v['opportunity_detail_father']=="" || $v['opportunity_detail_father']=="0"){
                        if($v['serials_contract_type']=="Serial" && $v['serials_contract_limitdate']==""){
                            $estadoSerial = false;
                        }
                    }else{
                        if($v['serials_contract_type']=="Contrato" && $v['serials_contract_limitdate']==""){
                            $estadoSerial = false;
                        }
                    }
                    $infoReglas .= "/RFL/";
                }

                if($value["rules_key"]=="REQUIRE_CONTTRACT_ADITIONAL"){
                    if($v['serials_contract_type']=="Serial"){
                        $estadoContrato = false;

                        $query = $this->db->prepare("SELECT COUNT(*) AS total FROM serials_contract WHERE serials_contract_type='Contrato' AND serials_contract_number!='' AND serials_contract_parent=".$v['serials_contract_id']."");
                        $query->execute();
                        $serials_contract_count = $query->fetchAll(PDO::FETCH_ASSOC);

                        $totalContratos = $serials_contract_count[0]["total"];

                        if($totalContratos>0){
                            $estadoContrato = true;
                        }
                    }

                    $infoReglas .= "/RCAD/";
                }

                if($value["rules_key"]=="REQUIRE_ADMIN_CLOSE"){
                    if($v['serials_contract_status']=="0"){
                        $estadoAdmin = false;
                    }

                    $infoReglas .= "/RCADM/";
                }
            }*/

            /*if($estadoSerial && $estadoContrato && $estadoAdmin){
                $estadoFinal = "COMPLETO";
            }*/

            $Milestone = new \Models\PDO\Milestone($this->db);
            $statusSerialSale = $Milestone->statusSerialSale($v['serials_contract_id'], $v['opportunity_detail_productid']);
            $statusSerialSale = explode("|", $statusSerialSale);

            $estadoFinal = $statusSerialSale[0];
            $infoReglas = $statusSerialSale[1];

            ////////////////////////////////////

            $order_sales[$id_o]['opportunity_details'][$id_d] = array(
                'id_detail' => $v['opportunity_detail_id'],
                'currency' => $v['opportunity_detail_moneda'],
                'product' => $v['opportunity_detail_description'],
                'qty' => $v['opportunity_detail_qty'],
                'price' => $v['opportunity_detail_price'],
                'type' => $v['opportunity_detail_type'],
                'father' => $v['opportunity_detail_father'],
                'sku' => $v['opportunity_detail_productid'],
                'date' => substr($v['opportunity_detail_createdate'], 0, 10)
            );


            // if ($v['serials_contract_number'] != '') {
            $order_sales[$id_o]['details_serials'][$id_d][$id_s] = array(
                'type' => $v['serials_contract_type'],
                'number' => $v['serials_contract_number'],
                'date' => $v['serials_contract_date'],
                'enddate' => $v['serials_contract_enddate'],
                'limitdate' => $v['serials_contract_limitdate'],
                'statusComplete' => $estadoFinal,
                'infoRules' => $infoReglas,
                'status' => $v['serials_contract_status']
            );
            $j++;
            //}
        }

        $this->log->info("Ver listado ventas", "SELECT", "", 0);

        /*$ordenesfinales = array();
        $ini=$data["countResults"]*($data["pagina"]-1);
        $fin=$data["countResults"]*($data["pagina"]);
        $countlist=0;

        foreach($order_sales as $k=>$v){

            if($countlist>=$ini && $countlist<$fin){
                $ordenesfinales[$k]=$v;
            }

            if($countlist>=$fin){
                break;
            }

            $countlist++;
        }

        $data["totalItems"]=count($ordenesfinales);*/

        ///////////////////

        return $this->view->render($response, 'sales.twig', [
                    'head' => array(),
                    'path' => '../template/',
                    'body' => array(),
                    //'sales' => $ordenesfinales,
                    'sales' => $order_sales,
                    //'rules' => $rules,
                    'brands' => $brands,
                    'params' => $data
        ]);
    })->setName('sale-list');


    $this->get('/serials_history/{id:[0-9]+}', function ($request, $response, $args) {
        $query = $this->db->prepare("SELECT *
               FROM serial_history
               WHERE serials_contract_id = :id AND serials_history_number != ''");
        $query->bindValue(':id', $args['id']);
        $query->execute();
        return $response->withJson($query->fetchAll(PDO::FETCH_ASSOC));
    })->setName('sale-serials-history');
    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/confirmarEstado/{id:[0-9]+}', function ($request, $response, $args) {
        $serials_contract_id = $args["id"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $query = $this->db->prepare("UPDATE serials_contract SET
                                        serials_contract_status=:serials_contract_status
                                    WHERE serials_contract_id=:id ");

        $query->bindParam(':serials_contract_status', $milestone["serials_contract_status"]);
        $query->bindParam(':id', $serials_contract_id);
        $query->execute();


        ////////////////////////////////

        $this->log->info("Se Confirmo el estado del Serial/Contrato " . $milestone["serials_contract_status"], "UPDATE", "serials_contract_id", $serials_contract_id);

        return $response->withJson("OK");
    });


    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/editarVenta/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $query = $this->db->prepare("UPDATE opportunity SET
                                        opportunity_salenumber=:opportunity_salenumber
                                    WHERE opportunity_id=:id ");

        $query->bindParam(':opportunity_salenumber', $milestone["opportunity_salenumber"]);
        $query->bindParam(':id', $oportunity_id);

        $query->execute();

        ////////////////////////////////

        $this->log->info("Se Modificó la venta #$oportunity_id. Número " . $milestone["opportunity_salenumber"], "UPDATE", "opportunity_id", $oportunity_id);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/reversarVenta/{id:[0-9]+}', function ($request, $response, $args) {
        $oportunity_id = $args["id"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $query = $this->db->prepare("DELETE FROM serials_contract WHERE opportunity_detail_id IN (SELECT a.opportunity_detail_id FROM opportunity_detail a WHERE a.oportunity_id=:id) ");
        $query->bindParam(':id', $oportunity_id);

        $query->execute();

        ///////////////////////////////

        $query = $this->db->prepare("UPDATE opportunity SET opportunity_status=:milestone_status WHERE opportunity_id=:id ");
        $query->bindParam(':milestone_status', $milestone["milestone_status"]);
        $query->bindParam(':id', $oportunity_id);

        $query->execute();

        ///////////////////////////////

        $milestone_status = "0";
        $milestone_type = "notice";
        $milestone_date = explode("/", $milestone["milestone_date"]);
        $milestone_date = $milestone_date[2] . "-" . $milestone_date[1] . "-" . $milestone_date[0];

        $query = $this->db->prepare("INSERT IGNORE INTO milestone (milestone_title, milestone_detail, milestone_date, milestone_start_time,
		 													milestone_end_time, milestone_type, milestone_notification, milestone_status,
															milestone_link, opportunity_id, user_id, client_id)
									 VALUES (:mtitle, :mdetail, :mdate, NULL, NULL, :mtype, 'N', :mstatus, NULL, :opid, :uid, :cid);");

        $query->bindParam(':mtitle', $milestone["milestone_title"]);
        $query->bindParam(':mdetail', $milestone["milestone_detail"]);
        $query->bindParam(':mdate', $milestone_date);
        $query->bindParam(':mtype', $milestone_type);
        $query->bindParam(':mstatus', $milestone_status);

        $query->bindParam(':cid', $milestone["mclient_id"]);
        $query->bindParam(':uid', $userId);
        $query->bindParam(':opid', $oportunity_id);

        $query->execute();

        ////////////////////////////////

        $this->log->info("Se reverso la venta", "INSERT", "opportunity_id", $oportunity_id);

        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->put('/registrarSerial/{idsc:[0-9]+}', function ($request, $response, $args) {
        $serials_contract_id = $args["idsc"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $query = $this->db->prepare("SELECT
										serials_contract_number
		                             FROM
									 	serials_contract a, opportunity_detail b
                                     WHERE
                                    	a.opportunity_detail_id=b.opportunity_detail_id AND
										b.opportunity_detail_type IN ('HARD','HW') AND
										a.serials_contract_number='" . $milestone["serials_contract_number"] . "' AND
										a.serials_contract_id!='" . $serials_contract_id . "' AND
										a.serials_contract_parent=0");
        $query->execute();
        $seriales_contrato = $query->fetchAll(PDO::FETCH_ASSOC);

        if (count($seriales_contrato) <= 0) {
            if ($milestone["serials_contract_enddate"] != "") {
                $serials_contract_enddate = explode("/", $milestone["serials_contract_enddate"]);
                $serials_contract_enddate = $serials_contract_enddate[2] . "-" . $serials_contract_enddate[1] . "-" . $serials_contract_enddate[0];
            } else {
                $serials_contract_enddate = null;
            }

            if ($milestone["serials_contract_limitdate"] != "") {
                $serials_contract_limitdate = explode("/", $milestone["serials_contract_limitdate"]);
                $serials_contract_limitdate = $serials_contract_limitdate[2] . "-" . $serials_contract_limitdate[1] . "-" . $serials_contract_limitdate[0];
            } else {
                $serials_contract_limitdate = null;
            }

            //////////////////////////////////
            //
            $query = $this->db->prepare("
                INSERT IGNORE INTO serial_history
                (
                    serial_history_number, serials_contract_id
                ) VALUE (
                    :serial_history_number, :serial_contract_id
                )
            ");
            $query->bindParam(":serial_history_number", $milestone["serials_contract_number"]);
            $query->bindParam(":serial_contract_id", $serials_contract_id);
            $query->execute();
            //
            //////////////////////////////////

            //////////////////////////
            $query = $this->db->prepare("UPDATE serials_contract
                                            SET serials_contract_number=:serials_contract_number,
                                            serials_contract_enddate=:serials_contract_enddate,
                                            serials_contract_limitdate=:serials_contract_limitdate
                                         WHERE serials_contract_id=:idsc ");
            $query->bindParam(':serials_contract_number', $milestone["serials_contract_number"]);
            $query->bindParam(':serials_contract_enddate', $serials_contract_enddate);
            $query->bindParam(':serials_contract_limitdate', $serials_contract_limitdate);
            $query->bindParam(':idsc', $serials_contract_id);
            $query->execute();

            $this->log->info("Registrar serial", "UPDATE", "serials_contract_id", $serials_contract_id);

            //////////////////////////Actualizo los seriales de los hijos

            $query = $this->db->prepare("UPDATE serials_contract
                                            SET serials_contract_number=:serials_contract_number
                                         WHERE serials_contract_parent	=:idsc AND serials_contract_type='Serial'");
            $query->bindParam(':serials_contract_number', $milestone["serials_contract_number"]);
            $query->bindParam(':idsc', $serials_contract_id);
            $query->execute();

            //si es un coterm, acutalido la fecha para todos los seriales.
            if ($milestone['sku'] == "FTNT-COT") {
                $query = $this->db->prepare("UPDATE serials_contract
												SET serials_contract_enddate=:serials_contract_enddate
											 WHERE opportunity_detail_id =:opportunity_detail_id AND
											       serials_contract_type='Serial'");

                $query->bindParam(':serials_contract_enddate', $serials_contract_enddate);
                $query->bindParam(':opportunity_detail_id', $milestone["id_detail"]);
                $query->execute();
            }

            $this->log->info("Registrar serial hijo", "UPDATE", "serials_contract_parent", $serials_contract_id);

            //////////////////////////

            $Milestone = new \Models\PDO\Milestone($this->db);
            $statusSerialSale = $Milestone->statusSerialSale($serials_contract_id, "");

            //////////////////////////

            return $response->withJson("OK");
        } else {
            return $response->withJson("VALIDACION");
        }
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->post('/registrarContrato/{deid:[0-9]+}', function ($request, $response, $args) {
        $opportunity_detail_id = $args["deid"];

        $milestone = $request->getParsedBody();

        $userId = $this->session->user_id;

        $query = $this->db->prepare("SELECT serials_contract_number FROM serials_contract
		                             WHERE serials_contract_number='" . $milestone["serials_contract_number"] . "' ");
        $query->execute();
        $seriales_contrato = $query->fetchAll(PDO::FETCH_ASSOC);

        if (count($seriales_contrato) <= 0) {
            if ($milestone["serials_contract_limitdate"] != "") {
                $serials_contract_limitdate = explode("/", $milestone["serials_contract_limitdate"]);
                $serials_contract_limitdate = $serials_contract_limitdate[2] . "-" . $serials_contract_limitdate[1] . "-" . $serials_contract_limitdate[0];
            } else {
                $serials_contract_limitdate = null;
            }

            $query = $this->db->prepare("INSERT INTO serials_contract
                (
                    opportunity_detail_id,serials_contract_type,serials_contract_createdate,
                    serials_contract_number,serials_contract_status,
                    serials_contract_limitdate,serials_contract_parent
                ) VALUE(
                    '" . $opportunity_detail_id . "', 'Contrato', '" . date("Y-m-d H:i:s") . "',
                    '" . $milestone["serials_contract_number"] . "', '0',
                :serials_contract_limitdate, '" . $milestone["serials_contract_parent"] . "')
            ");
            $query->bindParam(':serials_contract_limitdate', $serials_contract_limitdate);
            $query->execute();

            $last_id = $this->db->lastInsertId();

            $this->log->info("Registrar contrato", "INSERT", "serials_contract_id", $last_id);

            if ($milestone['sku'] == "FTNT-COT") {
                $query = $this->db->prepare("INSERT INTO serials_contract (opportunity_detail_id,serials_contract_type,serials_contract_createdate,
			                                                           serials_contract_number,serials_contract_status,serials_contract_limitdate,
																	   serials_contract_parent )
					SELECT  opportunity_detail_id,'Contrato' AS serials_contract_type,
					        '".date("Y-m-d H:i:s")."' AS serials_contract_createdate,
						    '" . $milestone["serials_contract_number"] . "' AS serials_contract_number,
							'0' AS serials_contract_status,
						    ".(($serials_contract_limitdate == null)?"NULL":"'".$serials_contract_limitdate."'")." AS serials_contract_limitdate,
							serials_contract_id
					FROM serials_contract
					WHERE opportunity_detail_id=".$opportunity_detail_id." AND
					      serials_contract_type='Serial' AND
						  serials_contract_id!='".$milestone["serials_contract_parent"]."'");
                $query->execute();
            }

            //////////////////////////

            $Milestone = new \Models\PDO\Milestone($this->db);
            $statusSerialSale = $Milestone->statusSerialSale($last_id, "");

            //////////////////////////

            return $response->withJson("OK");
        } else {
            return $response->withJson("VALIDACION");
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->post('/editarContrato/{deid:[0-9]+}', function ($request, $response, $args) {
        $opportunity_detail_id = $args["deid"];

        $milestone = $request->getParsedBody();

        if ($milestone["serials_contract_limitdate"] != "") {
            $serials_contract_limitdate = explode("/", $milestone["serials_contract_limitdate"]);
            $serials_contract_limitdate = $serials_contract_limitdate[2] . "-" . $serials_contract_limitdate[1] . "-" . $serials_contract_limitdate[0];
        } else {
            $serials_contract_limitdate = null;
        }

        $query = $this->db->prepare("
            UPDATE serials_contract
                SET
                    serials_contract_number = :number, serials_contract_status = '0',
                    serials_contract_limitdate = :limit
                WHERE  serials_contract_id = :id
        ");
        $query->bindParam(':number', $milestone["serials_contract_number"]);
        $query->bindParam(':limit', $serials_contract_limitdate);
        $query->bindParam(':id', $opportunity_detail_id);
        $query->execute();

        //////////////////////////

        $Milestone = new \Models\PDO\Milestone($this->db);
        $statusSerialSale = $Milestone->statusSerialSale($opportunity_detail_id, "");

        //////////////////////////

        if ($query->rowCount() > 0) {
            $this->log->info("Se modificó el contrato", "UPDATE", "serials_contract_id", $opportunity_detail_id);
            return $response->withJson("OK");
        } else {
            return $response->withJson("FAIL");
        }
    });

    ///////////////////////////////////////////////////////////////////////////////////////////

    $this->delete('/eliminarContrato/', function ($request, $response, $args) {
        $milestone = $request->getParsedBody();

        $query = $this->db->prepare("DELETE FROM serials_contract WHERE serials_contract_id=:scid ");
        $query->bindParam(':scid', $milestone["serials_contract_id"]);
        $query->execute();

        if ($query->rowCount() > 0) {
            $this->log->info("Se eliminó el contrato", "DELETE", "serials_contract_id", $milestone["serials_contract_id"]);
        }
        return $response->withJson("OK");
    });

    ///////////////////////////////////////////////////////////////////////////////////////////
});
