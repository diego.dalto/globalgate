<?php

$app->group('/producto', function () {

    $this->get('/{id}', function ($request, $response, $args) {
        $tableProduct = $this->db->prepare("
            SELECT `product_id`, `products`.`brand_id`, `second_brand_id`,
                `product_status`, `comments`, `identifier`, `family`,
                `product`, `type`, `item`, `sku`, `description1`,
                `description2`, `price`, `category`, `translate_price`,
                `upc_code`, `fed`, `gsa`, `coo`, `ppb`.`brand_name`,
                `ppb`.`brand_type`, `spb`.`brand_name` AS second_brand_name
            FROM `products`
            INNER JOIN product_brand ppb ON ppb.brand_id = products.brand_id
            LEFT JOIN product_brand spb ON spb.brand_id = products.second_brand_id
            WHERE product_id = :product_id LIMIT 1;
        ");
        $tableProduct->bindValue(':product_id', $args['id']);
        $tableProduct->execute();
        $params['product'] = $tableProduct->fetch(PDO::FETCH_ASSOC);

        $tableProductRules = $this->db->prepare("
                SELECT rules_key, sku
                    FROM product_rules
                WHERE sku = :sku;
        ");
        $tableProductRules->bindValue(':sku', $params['product']['sku']);
        $tableProductRules->execute();
        $params['rules'] = $tableProductRules->fetchAll(PDO::FETCH_ASSOC);

        return $response->withJSON($params);
    })->setName('product');

    $this->get('s', function ($request, $response, $args) {
        $where = "";
        $params = [];

        $brand = $request->getParam('brand');
        $sku = trim($request->getParam('sku'));
        $hardware = $request->getParam('hardware');
        $keyword = trim($request->getParam('keyword'));
        $status = $request->getParam('status');

        if (!empty($hardware)) {
            $params['selected']['hardware'] = $hardware;
            $where .= " AND type LIKE '%" . $hardware . "%' ";
        }

        if (!empty($sku)) {
            $params['selected']['sku'] = $sku;
            $where .= " AND sku LIKE '%" . $sku . "%' ";
        }

        if (!empty($keyword)) {
            $params['selected']['keyword'] = $keyword;
            $where .= " AND (ppb.brand_name LIKE '%" . $keyword . "%' "
                    . "OR description1 LIKE '%" . $keyword . "%' "
                    . "OR item LIKE '%" . $keyword . "%' "
                    . "OR family LIKE '%" . $keyword . "%')";
        }

        if (!empty($brand)) {
            $params['selected']['brand_id'] = $brand;
            $where .= " AND products.brand_id = $brand ";
        }

        if (!empty($status)) {
            $params['selected']['status'] = $status;
            $where .= " AND product_status = $status ";
        }

        /**
         * Lista de marcas
         */
        $tableBrand = $this->db->prepare("
                SELECT brand_id, brand_name
                    FROM product_brand
                WHERE 1=1 GROUP BY brand_name ASC;
        ");
        $tableBrand->execute();
        $params['brands'] = $tableBrand->fetchAll(PDO::FETCH_ASSOC);

        $tableProduct = $this->db->prepare("
            SELECT `product_id`, `products`.`brand_id`, `second_brand_id`,
                `product_status`, `comments`, `identifier`, `family`,
                `product`, `type`, `item`, `sku`, `description1`,
                `description2`, `price`, `category`, `translate_price`,
                `upc_code`, `fed`, `gsa`, `coo`, `ppb`.`brand_name`,
                `ppb`.`brand_type`, `spb`.`brand_name` AS second_brand_name
            FROM `products`
            INNER JOIN product_brand ppb ON ppb.brand_id = products.brand_id
            LEFT JOIN product_brand spb ON spb.brand_id = products.second_brand_id
            WHERE 1=1 $where LIMIT 500;
        ");
        $tableProduct->execute();
        $params['products'] = $tableProduct->fetchAll(PDO::FETCH_ASSOC);

        /**
         * Lista de reglas de productos
         */
        $tableRules = $this->db->prepare("
            SELECT rules_key, rules_value, rules_description
              FROM rules
            WHERE 1=1 ORDER BY rules_order ASC;
        ");
        $tableRules->execute();
        $params['rules'] = $tableRules->fetchAll(PDO::FETCH_ASSOC);

        /**
         * Lista de familia
         */
        $tableFamily = $this->db->prepare("
            SELECT family
              FROM products
            WHERE family != ''
            GROUP BY family ASC, brand_id ASC;
        ");
        $tableFamily->execute();
        $params['family'] = $tableFamily->fetchAll(PDO::FETCH_ASSOC);

        //$this->log->info("Se ingresó al listado de productos", "SELECT", "", 0);
        //return $response->withJSON($params);
        return $this->view->render($response, 'products.twig', $params);
    })->setName('product-list');

    $this->delete('/delete', function ($request, $response, $args) {
        $product_id = $request->getParsedBodyParam('product_id');
        $params = [
            'type' => -1,
            'message' => "Falta ingresar el id"
        ];

        if (isset($product_id) && !empty($product_id)) {
            $tableProduct = $this->db->prepare("
                DELETE
                    FROM products
                WHERE product_id = :id;
            ");
            $tableProduct->bindValue(':id', $product_id);
            $tableProduct->execute();

            $params = [
                'type' => 2,
                'message' => "Se eliminó el producto"
            ];

            $this->log->info("Se borró el producto", "DELETE", "product_id", $product_id);
        }

        return $response->withJson($params);
    })->setName('product-delete');

    $this->post('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $sku = trim($post['sku']);

        $params = [
            'type' => -1,
            'message' => "No se pudo guardar"
        ];

        if (isset($post)) {
            $tableProducts = $this->db->prepare("
                SELECT sku
                    FROM products
                WHERE sku = :sku;
            ");
            $tableProducts->bindValue(':sku', $sku);
            $tableProducts->execute();

            if ($tableProducts->rowCount() > 0) {
                $params['message'] = "El serial ya existe";
            } else {
                $tableProducts = $this->db->prepare("
                    INSERT INTO `products`
                    (
                        `brand_id`, `product_status`, `identifier`, `family`,
                        `product`,  `type`, `item`, `sku`, `description1`,
                        `description2`, `price`, `category`,
                        `upc_code`, `fed`, `gsa`, `coo`
                    ) VALUES (
                        :brand_id, :product_status, :identifier, :family,
                        :product, :type, :item, :sku, :description1,
                        :description2, :price, :category,
                        :upc_code, :fed, :gsa, :coo
                    )
                ");
                $tableProducts->bindValue(':brand_id', $post['brand_id']);
                $tableProducts->bindValue(':product_status', 'A');
                //$tableProducts->bindValue(':identifier', $post['identifier']);
                $tableProducts->bindValue(':identifier', $post['product']);
                $tableProducts->bindValue(':family', $post['family']);
                $tableProducts->bindValue(':product', $post['product']);
                $tableProducts->bindValue(':type', $post['type']);
                //$tableProducts->bindValue(':item', $post['item']);
                $tableProducts->bindValue(':item', $post['product']);
                $tableProducts->bindValue(':sku', $sku);
                $tableProducts->bindValue(':description1', $post['description1']);
                $tableProducts->bindValue(':description2', $post['description2']);
                $tableProducts->bindValue(':price', $post['price']);
                $tableProducts->bindValue(':category', $post['category']);
                $tableProducts->bindValue(':upc_code', $post['upc_code']);
                $tableProducts->bindValue(':fed', $post['fed']);
                $tableProducts->bindValue(':gsa', $post['gsa']);
                $tableProducts->bindValue(':coo', $post['coo']);
                $tableProducts->execute();

                $params = [
                    'type' => 2,
                    'message' => "Guardado"
                ];
                $product_id = $this->db->lastInsertId();

                if(isset($post['require']) && is_array($post['require'])) {
                  $tableProductRules = $this->db->prepare("
                      INSERT INTO `product_rules`
                      (
                          `rules_key`, `sku`
                      ) VALUES (
                          :rules_key, :sku
                      )
                  ");
                  $tableProductRules->bindValue(':sku', $sku);

                  foreach($post['require'] as $key => $value) {
                    $tableProductRules->bindValue(':rules_key', $key);
                    $tableProductRules->execute();
                  }
                }
            }
            //$this->log->info("Se creó el producto ".$post['product'], "INSERT", "product_id", $product_id);
        }

        return $response->withJson($params);
    })->setName('product-create');

    $this->get('/marca/{id}/familia', function ($request, $response, $args) {
      /**
       * Lista de familia
       */
      $tableFamily = $this->db->prepare("
          SELECT family
            FROM products
          WHERE brand_id = :id AND family != ''
          GROUP BY TRIM(family) ASC;
      ");
      $tableFamily->bindValue(':id', $args['id']);
      $tableFamily->execute();
      $params['family'] = $tableFamily->fetchAll(PDO::FETCH_ASSOC);

      return $response->withJson($params);
    })->setName('product-family');

    $this->put('', function ($request, $response, $args) {
        $post = $request->getParsedBody();
        $sku = trim($post['sku']);

        $params = [
            'type' => -1,
            'message' => "No se pudo actualizar"
        ];
        if (isset($post)) {
            $tableProducts = $this->db->prepare("
              SELECT sku
                  FROM products
              WHERE sku = :sku AND product_id != :id;

            ");
            $tableProducts->bindValue(':sku', $sku);
            $tableProducts->bindValue(':id', $post['product_id']);
            $tableProducts->execute();

            if ($tableProducts->rowCount() > 0) {
                $params['message'] = "El serial ya existe";
            } else {
                $tableProducts = $this->db->prepare("
                UPDATE products
                    SET `brand_id` = :brand_id,
                    `identifier` = :identifier, `family` = :family,
                    `product` = :product, `type` = :type, `item` = :item,
                    `sku` = :sku, `description1` = :description1,
                    `description2` = :description2, `price` = :price,
                    `category` = :category, `upc_code` = :upc_code,
                    `fed` = :fed, `gsa` = :gsa, `coo` = :coo
                WHERE product_id = :id;
              ");
                $tableProducts->bindValue(':brand_id', $post['brand_id']);
                //$tableProducts->bindValue(':product_status', $post['product_status']);
                //$tableProducts->bindValue(':identifier', $post['identifier']);
                $tableProducts->bindValue(':identifier', $post['product']);
                $tableProducts->bindValue(':family', $post['family']);
                $tableProducts->bindValue(':product', $post['product']);
                $tableProducts->bindValue(':type', $post['type']);
                //$tableProducts->bindValue(':item', $post['item']);
                $tableProducts->bindValue(':item', $post['product']);
                $tableProducts->bindValue(':sku', $sku);
                $tableProducts->bindValue(':description1', $post['description1']);
                $tableProducts->bindValue(':description2', $post['description2']);
                $tableProducts->bindValue(':price', $post['price']);
                $tableProducts->bindValue(':category', $post['category']);
                $tableProducts->bindValue(':upc_code', $post['upc_code']);
                $tableProducts->bindValue(':fed', $post['fed']);
                $tableProducts->bindValue(':gsa', $post['gsa']);
                $tableProducts->bindValue(':coo', $post['coo']);
                $tableProducts->bindValue(':id', $post['product_id']);
                $tableProducts->execute();

                //$this->log->info("Se modificó el producto " . $post['brand_name'], "INSERT", "brand_id", $brand_id);

                $tableProductRules = $this->db->prepare("
                    DELETE FROM `product_rules`
                      WHERE sku = :sku;
                    )
                ");
                $tableProductRules->bindValue(':sku', $sku);
                $tableProductRules->execute();

                if(isset($post['require']) && is_array($post['require'])) {
                  $tableProductRules = $this->db->prepare("
                      INSERT INTO `product_rules`
                      (
                          `rules_key`, `sku`
                      ) VALUES (
                          :rules_key, :sku
                      )
                  ");
                  $tableProductRules->bindValue(':sku', $sku);

                  foreach($post['require'] as $key => $value) {
                    $tableProductRules->bindValue(':rules_key', $key);
                    $tableProductRules->execute();
                  }
                }

                $params = [
                'type' => 2,
                'message' => "Actualizado"
              ];
            }
        }

        return $response->withJson($params);
    })->setName('product-update');
});
