<?php 

$app->group('/log', function () {

    $this->get('', function ($request, $response, $args) {
        $logAction = $request->getParam('log_action');
        $user_id = $request->getParam('user_id');
        $dateFrom = $request->getParam('fechadesde');
        $dateTo = $request->getParam('fechahasta');

        $where = '';

        $params = [
            'head' => array(),
            'path' => '../template/',
            'action' => 'Listado',
            'selected' => [
                'log_action' => $logAction,
                'user_id' => $user_id,
                'fechadesde' => $dateFrom,
                'fechahasta' => $dateTo
            ]
        ];

        if (isset($logAction) && $logAction != "") {
            $where .= "AND log_action = '$logAction' ";
        }
        
        if (isset($user_id) && $user_id > 0) {
            $where .= "AND user_id = '$user_id' ";
        }

        if (isset($dateFrom) && $dateFrom != "") {
            if (isset($dateTo) && $dateTo != "") {
                $fecha = explode('/', $dateFrom);
                $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                $fecha2 = explode('/', $dateTo);
                $date2 = $fecha2[2] . '-' . $fecha2[1] . '-' . $fecha2[0];
                $where .= 'AND DATE(log_timestamp) BETWEEN "' . $date . '" AND "' . $date2 . '" ';
            } else {
                $fecha = explode('/', $dateFrom);
                $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                $where .= 'AND DATE(log_timestamp) >= "' . $date . '" ';
            }
        }

        if (isset($dateTo) && $dateTo != "") {
            if (isset($dateFrom) && $dateFrom != "") {
                $fecha = explode('/', $dateFrom);
                $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                $fecha2 = explode('/', $dateTo);
                $date2 = $fecha2[2] . '-' . $fecha2[1] . '-' . $fecha2[0];
                $where .= 'AND DATE(log_timestamp) BETWEEN "' . $date . '" AND "' . $date2 . '" ';
            } else {
                $fecha = explode('/', $dateTo);
                $date = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                $where .= 'AND DATE(log_timestamp) <= "' . $date . '" ';
            }
        }

        /**
         * Lista de log
         */
        $tableBrand = $this->db->prepare("
            SELECT 
                `log_id`, `log_timestamp`, `log_type`, 
                `log_detail`, `log_action`, `log_key`, 
                `log_value`, CONCAT(`user_name`, ' ', `user_lastname`) AS fullname 
                FROM log
                NATURAL JOIN users 
            WHERE 1=1 $where LIMIT 1000;
        ");
        $tableBrand->execute();
        $params['logs'] = $tableBrand->fetchAll(PDO::FETCH_ASSOC);

        /**
         * Lista de usuarios
         */
        $tableUsers = $this->db->prepare("
                SELECT CONCAT(user_name, ' ', user_lastname) AS fullname,
                    user_id
                FROM users ORDER BY fullname ASC;
        ");
        $tableUsers->execute();
        $params['users'] = $tableUsers->fetchAll(PDO::FETCH_ASSOC);

        return $this->view->render($response, 'log.twig', $params);
    })->setName('log-list');
});
