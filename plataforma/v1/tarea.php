<?php

$app->group('/tarea', function () {

    $this->get('/', function ($request, $response, $args) {
        
        $me = $this->session->user_id;
        $dateTime = new DateTime('NOW');
        $date = $dateTime->format('Y-m-d');
        $milestone = new \Models\PDO\Milestone($this->db);
                
        //Obtengo calendario last
        $last = $milestone->getTask($me, $date, "LAST");
        
        //Obtengo calendario now
        $nows = $milestone->getTask($me, $date);
        
        //Obtengo calendario tomorrow
        $tomorrow = $milestone->getTask($me, $date, "TOMORROW");
        
        $user = $this->user->get("1=1");
        
        $this->log->info("Se ingresó al listado de tareas", "SELECT", "", 0);

        return $this->view->render($response, 'task_list.twig', [
            'head' => array(),
            'path' => '../template/',
            'action' => 'Listado',
            'me' => $me,
            'users' => $user,
            'latest' => $last,
            'nows' => $nows,
            'tomorrows' => $tomorrow
        ]);
    })->setName('task-list');
    
    $this->get('/{id:[0-9]+}', function ($request, $response, $args) {
        $me = $args['id'];
        
        $dateTime = new DateTime('NOW');
        $date = $dateTime->format('Y-m-d');
        $milestone = new \Models\PDO\Milestone($this->db);
                
        //Obtengo calendario last
        $last = $milestone->getTask($me, $date, "LAST");
        
        //Obtengo calendario now
        $nows = $milestone->getTask($me, $date);
        
        //Obtengo calendario tomorrow
        $tomorrow = $milestone->getTask($me, $date, "TOMORROW");
        
        $this->log->info("Se ingresó al listado de tareas", "SELECT", "user_id", $me);

        return $this->view->render($response, 'template/partial/container/card_task.twig', [
            'column' => true,
            'latest' => $last,
            'nows' => $nows,
            'tomorrows' => $tomorrow
        ]);
    });

    $this->get('/{id:[0-9]+}/{limitLast}/{limitNow}/{limitTomorrow}', function ($request, $response, $args) {
        $me = $args['id'];
        
        $dateTime = new DateTime('NOW');
        $date = $dateTime->format('Y-m-d');
        $milestone = new \Models\PDO\Milestone($this->db);
                
        //Obtengo calendario last
        $last = $milestone->getTask($me, $date, "LAST", $args['limitLast']);

        //Obtengo calendario now
        $nows = $milestone->getTask($me, $date, "NOW", $args['limitNow']);
        
        //Obtengo calendario tomorrow
        $tomorrow = $milestone->getTask($me, $date, "TOMORROW", $args['limitTomorrow']);
        
        $this->log->info("Se ingresó al listado de tareas", "SELECT", "user_id", $me);

        return $this->view->render($response, 'template/partial/container/card_task.twig', [
            'column' => true,
            'latest' => $last,
            'nows' => $nows,
            'tomorrows' => $tomorrow,
            'limit' => $args
        ]);
    })->setName('task-last-limit');
    
    $this->get('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        
    });

    $this->get('/listado', function ($request, $response, $args) {
        
    });
    
    $this->put('/editar/{id:[0-9]+}', function ($request, $response, $args) {
        
    });

    $this->post('/crear/{id:[0-9]+}', function ($request, $response, $args) {
        
    });
});